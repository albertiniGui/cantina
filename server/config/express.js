const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const load = require('express-load');
const fs = require('fs');

module.exports = function () {
    const app = express();
    app.use(cors());

    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin","*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
        next();
    })

    app.use(bodyParser.json({ limit: '2mb' }));
    app.use('/', express.static(__dirname.split('server')[0] + '/dist'));
    load('rotas', { cwd: 'app', verbose: false })
        .then('infra')
        .then('utilities')
        .into(app);

    return app;
}
