function ControleDAO(conexao, polo) {
    this.conexao = conexao.conexao;
    this.polo = polo;
}

ControleDAO.prototype.selectControleProduto = function (callback) {
    this.conexao.query(`SELECT *, date_format(dataValidade, '%Y-%m-%d') as dataValidade, 
    date_format(dataValidade, '%d/%m/%Y') as dataValidadeFormatada 
    FROM produto_final join producao on produto_final.id_produtoFinal = producao.fk_idProdutoFinal 
    where producao.status_producao = 'Ativo' 
    and produto_final.polo = ? order by desc_produtoFinal asc`, this.polo, callback)
}

ControleDAO.prototype.selectProdReceitas = function (callback) {
    this.conexao.query(`select * from produto_final join receita on produto_final.desc_produtoFinal = receita.desc_receita
    where produto_final.polo = ?`, this.polo, callback)
}

ControleDAO.prototype.selectControleFreezer = function (callback) {
    this.conexao.query(`SELECT *, date_format(data_freezer, '%d/%m/%Y às %H:%i:%s') as data_freezerFormatada 
    FROM freezer join freezer_aux on freezer.id_freezer = freezer_aux.fk_idFreezer 
    where polo = ? order by id_registro`, this.polo, callback)
}

ControleDAO.prototype.selectIDReceita = function (data, callback) {
    this.conexao.query(`select id_receita, rendimento from receita where receita.desc_receita = ?`, data.nomeProduto, callback)
}

ControleDAO.prototype.insertProducao = function (data, id_receita, callback) {
    this.conexao.query(`INSERT INTO producao
    (qtdeProduzida, dataValidade, fk_idReceita, fk_idProdutoFinal, status_producao, polo, dataProducao) 
    values (?,?,?,?,'Ativo', ?, sysdate())`,
        [data.qtde, data.validade, id_receita, data.id_produto, this.polo], callback)
}

ControleDAO.prototype.updateProdutoFinal = function (data, rendimento, callback) {
    this.conexao.query(`UPDATE produto_final
    set qtde_produtoFinal = qtde_produtoFinal + (${rendimento} * ${data.qtde})
    where desc_produtoFinal like ? and polo = ?`, [data.nomeProduto, this.polo], callback)
}

ControleDAO.prototype.updateProducao = function (data, callback) {
    this.conexao.query(`update producao
    set qtdeProduzida = ?,
    dataValidade = ?
    where id_producao = ?
    and polo = ?`, [data.qtde, data.validade, data.id, this.polo], callback)
}

ControleDAO.prototype.deleteProducao = function (data, callback) {
    this.conexao.query(`update producao
    set status_producao = 'Inativo'
    where producao.id_producao = ?
    and polo = ?`, [data.producao, this.polo], callback)
}

ControleDAO.prototype.entradaEstoqueInsumo = function (data, callback) {
    this.conexao.query(`update insumo
    set qtdeEstoque_insumo = qtdeEstoque_insumo + ${data.qtde}
    where id_insumo = ?
    and polo = ?`, [data.insumo, this.polo], callback)
}

ControleDAO.prototype.updateEntrada = function (data, callback) {
    this.conexao.query(`update insumo
    set qtdeEstoque_insumo = ?
    where id_insumo = ?
    and polo = ?`, [data.qtde, data.id, this.polo], callback)
}

ControleDAO.prototype.selectInsumoReceita = function (callback) {
    this.conexao.query(`select ingredientes 
    from receita
    where id_receita >= 0`, callback)
}

ControleDAO.prototype.deleteInsumo = function (data, callback) {
    this.conexao.query(`delete from insumo where trim(desc_insumo) like trim(?)`, data.insumo, callback)
}

ControleDAO.prototype.selectInsumos = function (callback) {
    this.conexao.query(`SELECT * FROM insumo where polo = ?`, this.polo, callback)
}

ControleDAO.prototype.cadastrarRegistro = function (data, callback) {
    this.conexao.query(`insert into freezer_aux(fk_idFreezer, temp_freezer, data_freezer)
    values (?,?, sysdate())`, [data.freezer, data.temperatura], callback)
}

ControleDAO.prototype.editarRegistro = function (data, callback) {
    this.conexao.query(`update freezer_aux 
    set temp_freezer = ?,
    data_freezer = sysdate()
    where id_registro = ?
    and data_freezer = ?`, [data.temperatura, data.id, data.dataRegistro], callback)
}

module.exports = function () {
    return ControleDAO
}