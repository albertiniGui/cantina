function CadastroDAO(conexao, polo) {
    this.conexao = conexao.conexao;
    this.polo = polo;
}

CadastroDAO.prototype.checarExistencia = function (id, tabela, where, data, callback) {
    this.conexao.query(`SELECT ${id} 
    FROM ${tabela}
    WHERE ${where} LIKE ?`, data, callback)
}

CadastroDAO.prototype.cadastrarProduto = function (data, polo, callback) {
    this.conexao.query(`INSERT INTO produto_final 
    (codigo_barras,qtde_produtoFinal, valor_produtoFinal, valorDesc_produtoFinal, 
    desc_produtoFinal, unidadeVenda_produtoFinal,tipo_produto ,qtdeMax_produtoFinal, 
    qtdeMin_produtoFinal, status_produtoFinal, img_produtoFinal, polo) 
    VALUES (?,0,?,?,?,?,?,?,?,?,?,?)`, [data.codigo_barras, data.valor_produtoFinal,
        data.valorDesc_produtoFinal, data.desc_produtoFinal, data.unidadeVenda_produtoFinal,
        data.tipo_produto, data.qtdeMax_produtoFinal, data.qtdeMin_produtoFinal,
        data.status_produtoFinal, data.img_produtoFinal, polo], callback)
}

CadastroDAO.prototype.editarProduto = function (data, callback) {
    this.conexao.query(`UPDATE produto_final
    SET codigo_barras = ?,
    valor_produtoFinal = ?,
    valorDesc_produtoFinal = ?,
    desc_produtoFinal = ?,
    unidadeVenda_produtoFinal = ?,
    tipo_produto = ?,
    qtdeMax_produtoFinal = ?,
    qtdeMin_produtoFinal = ?,
    status_produtoFinal = ?,
    img_produtoFinal = ?
    WHERE id_produtoFinal = ? AND polo = ?`,
        [data.codigo_barras, data.valor_produtoFinal, data.valorDesc_produtoFinal,
        data.desc_produtoFinal, data.unidadeVenda_produtoFinal, data.tipo_produto,
        data.qtdeMax_produtoFinal, data.qtdeMin_produtoFinal, data.status_produtoFinal,
        data.img_produtoFinal, data.id_produtoFinal, this.polo], callback)
}

CadastroDAO.prototype.editarProdutoReceita = function (data, callback) {
    this.conexao.query(`UPDATE receita 
    SET desc_receita = ?
    WHERE desc_receita like ?`, [data.desc_produtoFinal, data.nomeAntigo], callback)
}


CadastroDAO.prototype.cadastrarInsumo = function (data, polo, callback) {
    this.conexao.query(`INSERT INTO insumo
    (desc_insumo,unidade_insumo,qtdeMax_insumo,qtdeMin_insumo,qtdeEstoque_insumo, polo) 
    VALUES (?,?,?,?,0,?)`, [data.desc_insumo, data.unidade_insumo, data.qtdeMax_insumo, data.qtdeMin_insumo, polo], callback)
}

CadastroDAO.prototype.editarInsumo = function (data, callback) {
    this.conexao.query(`UPDATE insumo 
    SET unidade_insumo = ?,
    qtdeMin_insumo = ?,
    qtdeMax_insumo = ?
    WHERE id_insumo = ?
    AND polo = ?`, [data.unidade_insumo, data.qtdeMin_insumo, data.qtdeMax_insumo, data.id_insumo, this.polo], callback)
}

CadastroDAO.prototype.cadastrarReceita = function (data, callback) {
    this.conexao.query(`INSERT INTO receita
    (criador,aprovado,dataIni,ativo,desc_receita,
    rendimento, custo, tempoPreparo_receita, ingredientes, modoPreparo_receita) 
    VALUES (?,?,?,?,?,?,?,?,?,?)`,
        [data.criador, data.aprovado, data.dataIni, data.ativo, data.desc_receita,
        data.rendimento, data.custo, data.tempoPreparo_receita, data.ingredientes, data.modopreparo_receita], callback)
}

CadastroDAO.prototype.editarReceita = function (data, callback) {
    this.conexao.query(`UPDATE receita 
    SET ?
    WHERE id_receita = ?`, [data, data.id_receita], callback)
}

CadastroDAO.prototype.cadastrarFreezer = function (data, callback) {
    this.conexao.query(`INSERT INTO freezer
    (tempMin_freezer,tempMax_freezer, polo) 
    VALUES (?,?,?)`, [data.tempMin_freezer, data.tempMax_freezer, this.polo], callback)
}

CadastroDAO.prototype.editarFreezer = function (data, callback) {
    this.conexao.query(`UPDATE freezer 
    SET tempMin_freezer = ?,
    tempMax_freezer = ?
    WHERE id_freezer = ?`, [data.tempMin_freezer, data.tempMax_freezer, data.id_freezer], callback)
}

module.exports = function () {
    return CadastroDAO
}