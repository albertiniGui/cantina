function ComandaDAO(conexao, polo) {
    this.conexao = conexao.conexao;
    this.conexaoAcesso = conexao.conexaoAcesso;
    this.polo = polo;
}

// CONEXOES NO BANCO DA CANTINA
ComandaDAO.prototype.selectProdutos = function (callback) {
    this.conexao.query(`SELECT * 
    FROM produto_final 
    WHERE qtde_produtoFinal > 0 
    AND polo = ?
    OR (tipo_produto = "Credit" OR desc_produtoFinal = "Crédito")
    AND polo = ? ORDER BY CASE 
    WHEN desc_produtoFinal = "Crédito"
    THEN 1 ELSE 0 END ASC, desc_produtoFinal ASC`, [polo, polo], callback);
}

ComandaDAO.prototype.selectCartoesConvidados = function (callback) {
    this.conexaoAcesso.query(`SELECT car_tag 
    FROM cartoes JOIN logins ON cartoes.car_login_vinculado = logins.log_id 
	JOIN usuarios ON logins.log_usuario_vinculado = usuarios.usu_id 
	JOIN tipos ON usuarios.usu_tipo_vinculado = tipos.tip_id 
	WHERE tip_descricao = 'Convidado'`, callback);
}

ComandaDAO.prototype.verificarComandaAberta = function (cartao, callback) {
    this.conexao.query(`SELECT * 
    FROM venda JOIN venda_aux on venda.id_venda = venda_aux.fk_idVenda
    WHERE fk_venda_cartao = ?
    AND pag_dinheiro = 0 
    AND pag_debito = 0 
    AND pag_credito = 0 
    AND pag_cardFiec = 0
    GROUP BY id_venda
    ORDER BY data_venda desc
    LIMIT 1`, [cartao], callback)
}

ComandaDAO.prototype.selectDadosComandaAberta = function (result, callback) {
    this.conexao.query(`SELECT * FROM venda JOIN venda_aux on venda.id_venda = venda_aux.fk_idVenda
    JOIN produto_final on venda_aux.fk_idProdutoFinal = produto_final.id_produtoFinal
    WHERE venda.id_venda = ?`, [result[0].id_venda], callback)
}

ComandaDAO.prototype.selectPorBarras = function (codigo_barras, callback) {
    this.conexao.query(`SELECT * FROM produto_final 
    WHERE codigo_barras like ? 
    AND polo = ?`, [codigo_barras, polo], callback)
}

ComandaDAO.prototype.senhaSupervisor = function (senha, callback) {
    this.conexao.query(`SELECT count(tipo) AS count 
    FROM login WHERE tipo = 1 AND log_senha = sha2(?,512)`, [senha], callback)
}

ComandaDAO.prototype.selectComandaAberta = function (tag, callback) {
    this.conexao.query(`SELECT id_venda FROM venda 
    WHERE fk_venda_cartao = ? AND pag_dinheiro = 0 
    AND pag_debito = 0 AND pag_credito = 0 
    AND pag_cardFiec = 0`, [tag], callback)
}
ComandaDAO.prototype.insertVenda = function (data, callback) {
    this.conexao.query(`INSERT INTO venda
    (fk_usu_comanda, fk_venda_cartao, total_venda, pag_dinheiro, pag_debito, pag_credito, pag_cardFiec, polo)
    values(?,?,?, 0, 0, 0, 0,?) `, [data.id_func, data.id_cartao, data.total, polo], callback)
}

ComandaDAO.prototype.insertVendaAux = function (lista, id, callback) {
    this.conexao.query(`INSERT INTO venda_aux
    (qtde_vendaAux, val_vendaAux, unidade_vendaAux, fk_idVenda, fk_idProdutoFinal, polo)
    values(?,?,?,?,?,?) `, [lista.quantidade, lista.valor_produtoFinal,
        lista.unidadeVenda_produtoFinal, id, lista.id_produtoFinal, polo], callback)
}

ComandaDAO.prototype.updateTotalVenda = function (data, id_venda, callback) {
    this.conexao.query(`UPDATE venda set total_venda = ? WHERE id_venda = ?`, [data.total, id_venda], callback)
}

ComandaDAO.prototype.deleteVendaAux = function (id_venda, callback) {
    this.conexao.query(`DELETE FROM venda_aux WHERE fk_idVenda = ?`, [id_venda], callback)
}


ComandaDAO.prototype.deleteVenda = function (id_venda, callback) {
    this.conexao.query(`DELETE FROM venda WHERE id_venda = ?`, [id_venda], callback)
}

// CONEXOES NO BANCO DO ACESSO
ComandaDAO.prototype.verificarExistenciaCartao = function (tag, callback) {
    this.conexaoAcesso.query(`SELECT car_tag FROM cartoes WHERE car_tag = ? `, [tag], callback)
}

ComandaDAO.prototype.verificarTipoCartao = function (tag, callback) {
    this.conexaoAcesso.query(`SELECT tip_descricao, car_tag
    FROM cartoes JOIN logins ON cartoes.car_login_vinculado = logins.log_id
    JOIN usuarios ON logins.log_usuario_vinculado = usuarios.usu_id
    JOIN tipos ON usuarios.usu_tipo_vinculado = tipos.tip_id
    WHERE car_tag = ? `, [tag], callback)
}

ComandaDAO.prototype.bloquearCartao = function (tag, callback) {
    this.conexaoAcesso.query(`UPDATE cartoes 
    set car_comanda_aberta = 1 WHERE car_tag = ? `, [tag], callback)
}

ComandaDAO.prototype.liberarCartao = function (tag, callback) {
    this.conexaoAcesso.query(`UPDATE teste_acesso_bd.cartoes 
    set car_comanda_aberta = 0 WHERE car_tag = ?`, [tag], callback)
}

module.exports = function () {
    return ComandaDAO
}