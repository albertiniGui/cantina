function RelatorioDAO(conexao) {
    this.conexao = conexao.conexao;
    this.polo = polo;
}

RelatorioDAO.prototype.somarProdutos = function (sqlQuery, callback) {
    this.conexao.query(`select desc_produtoFinal,sum(qtde_vendaAux) as qtde_vendida,
    val_vendaAux, (sum(qtde_vendaAux)*val_vendaAux) as total
    from produto_final join venda_aux on produto_final.id_produtoFinal = venda_aux.fk_idProdutoFinal
    join venda on venda_aux.fk_idVenda = venda.id_venda
    where id_venda < 0 ${sqlQuery}
    and (venda.polo = ?)
    group by desc_produtoFinal, val_vendaAux`, this.polo, callback)
}

RelatorioDAO.prototype.relatorioCompras = function (sqlQuery, polo, callback) {
    this.conexao.query(`SELECT *, date_format(data_compra, '%d/%m/%Y') as data_compraFormatada 
    FROM compra left join fornecedor on fornecedor.id_fornecedor = compra.fk_idFornecedor 
    where (polo = ${polo}) ${sqlQuery}`, callback)
}

RelatorioDAO.prototype.relatorioInsumos = function (sqlQuery, polo, callback) {
    this.conexao.query(`SELECT * FROM insumo where (polo = ${polo}) ${sqlQuery}`, callback)
}

RelatorioDAO.prototype.relatorioProducoes = function (sqlQuery, polo, callback) {
    this.conexao.query(`SELECT *, date_format(dataValidade, '%d/%m/%Y') as dataValidadeFormatada 
    FROM produto_final join producao on produto_final.id_produtoFinal = producao.fk_idProdutoFinal
    where (producao.polo = ${polo}) ${sqlQuery}`, callback)
}

RelatorioDAO.prototype.relatorioProdutos = function (sqlQuery, polo, callback) {
    this.conexao.query(`SELECT * FROM produto_final where (polo = ${polo}) ${sqlQuery}`, callback)
}

RelatorioDAO.prototype.relatorioReceitas = function (sqlQuery, callback) {
    this.conexao.query(`SELECT * FROM receita ${sqlQuery}`, callback)
}

RelatorioDAO.prototype.relatorioVendas = function (sqlQuery, callback) {
    this.conexao.query(`SELECT *, date_format(data_venda, '%d/%m/%Y às %H:%i:%s') as data_vendaFormatada 
    FROM venda ${sqlQuery}`, callback)
}

RelatorioDAO.prototype.relatorioVendasAux = function (id_venda, polo, callback) {
    this.conexao.query(`select venda_aux.*, desc_produtoFinal 
    from venda_aux  join produto_final on fk_idProdutoFinal = id_produtoFinal
    where fk_idVenda = ?
    and venda_aux.polo = ?`, [id_venda, polo], callback)
}

RelatorioDAO.prototype.relatorioRegistros = function (sqlQuery, polo, callback) {
    this.conexao.query(`SELECT *, date_format(data_freezer, '%d/%m/%Y às %H:%i:%s') as data_freezerFormatada
    FROM freezer  join freezer_aux on freezer.id_freezer = freezer_aux.fk_idFreezer
    where (freezer.polo = ${polo}) ${sqlQuery}`, callback)
}

RelatorioDAO.prototype.relatorioUsuarios = function (sqlQuery, callback) {
    this.conexao.query(`${sqlQuery} group by usu_id`, callback)
}

module.exports = function () {
    return RelatorioDAO
}