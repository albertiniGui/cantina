function LoginDAO(conexao) {
  this.conexao = conexao.conexao;
  this.conexaoAcesso = conexao.conexaoAcesso;
}

// BANCO DA CANTINA
LoginDAO.prototype.logIn = function (data, callback) {
  this.conexao.query(`SELECT usuario.usu_nome, login.tipo,
    login.log_login AS usu_login, usuario.usu_id
    FROM login join usuario ON usuario.fk_idlogin = login.log_id
    WHERE login.log_login = ? AND login.log_senha = sha2(?,512)`,
    [data.log_login, data.log_senha], callback);
}

// BANCO DE ACESSO
LoginDAO.prototype.selectCartoesConvidados = function (callback) {
  this.conexaoAcesso.query(`SELECT car_tag
    FROM cartoes JOIN logins ON cartoes.car_login_vinculado = logins.log_id
	JOIN usuarios ON logins.log_usuario_vinculado = usuarios.usu_id
	JOIN tipos ON usuarios.usu_tipo_vinculado = tipos.tip_id
	WHERE tip_descricao = 'Convidado'`, callback);
}

module.exports = function () {
  return LoginDAO
}
