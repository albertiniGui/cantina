function CaixaDAO(conexao, polo) {
    this.conexao = conexao.conexao;
    this.conexaoAcesso = conexao.conexaoAcesso;
    this.polo = polo;
}

CaixaDAO.prototype.comandasAbertas = function (callback) {
    this.conexao.query(`select * 
    from venda join venda_aux on venda.id_venda = venda_aux.fk_idVenda
    where pag_dinheiro = 0 
    and pag_debito = 0 
    and pag_credito = 0 
    and pag_cardFiec = 0
    and venda.polo = ?
    group by id_venda
    order by data_venda desc`, polo, callback)
}

CaixaDAO.prototype.comandaAberta = function (tag, callback) {
    this.conexao.query(`select id_venda
    from venda join venda_aux on venda.id_venda = venda_aux.fk_idVenda
    where fk_venda_cartao = ?
    and pag_dinheiro = 0 
    and pag_debito = 0 
    and pag_credito = 0 
    and pag_cardFiec = 0
    group by id_venda
    order by data_venda desc
    limit 1`, tag, callback)
}

CaixaDAO.prototype.dadosComanda = function (id_venda, callback) {
    this.conexao.query(`select * 
    from venda join venda_aux on venda.id_venda = venda_aux.fk_idVenda
    join produto_final on venda_aux.fk_idProdutoFinal = produto_final.id_produtoFinal
    where venda.id_venda = ?`, id_venda, callback)
}

CaixaDAO.prototype.updatePagamento = function (data, callback) {
    this.conexao.query(`update venda 
    set fk_usu_venda = ?,
    pag_dinheiro = ?,
    pag_debito = ?,
    pag_credito = ?,
    pag_cardFiec = ?
    where id_venda = ?`,
        [data.id_func, data.pag_dinheiro, data.pag_debito, data.pag_credito, data.pag_cardFiec, data.id_comanda], callback)
}

CaixaDAO.prototype.updateEstoque = function (data, callback) {
    this.conexao.query(`update produto_final 
    set qtde_produtoFinal = qtde_produtoFinal - ? 
    where id_produtoFinal = ? and polo = ?`,
        [data.qtde_vendaAux, data.id_produtoFinal, this.polo], callback)
}

CaixaDAO.prototype.selectUltimaSangria = function (callback) {
    this.conexao.query(`select max(id_sangria) as id_sangria from sangria`, callback)
}

CaixaDAO.prototype.updateSangria = function (totalVenda, id_sangria, callback) {
    this.conexao.query(`update sangria 
    set valorAtual = valorAtual + ? where id_sangria = ?`, [totalVenda, id_sangria], callback)
}



CaixaDAO.prototype.checarCredito = function (tag, callback) {
    this.conexaoAcesso.query(`select car_credito from cartoes where car_tag = ?`, [tag], callback)
}

CaixaDAO.prototype.insertCredito = function (data, callback) {
    this.conexaoAcesso.query(`UPDATE cartoes 
    set car_credito = car_credito + ? 
    where car_tag = ?`, [data.credito, data.tag], callback)
}

CaixaDAO.prototype.liberarCartao = function (tag, callback) {
    this.conexaoAcesso.query(`update cartoes 
    set car_comanda_aberta = 0 where car_tag = ?`, tag, callback)
}

CaixaDAO.prototype.updateCredito = function (data, callback) {
    this.conexaoAcesso.query(`UPDATE cartoes 
    set car_credito = car_credito - ?
    where car_tag = ?`, [data.credito, data.tag], callback)
}

CaixaDAO.prototype.checarSenha = function (data, callback) {
    this.conexaoAcesso.query(`select count(*) as count 
    from cartoes where car_tag = ? and car_pass = sha2(?,512)`, [data.tag, data.senha], callback)
}
module.exports = function () {
    return CaixaDAO
}