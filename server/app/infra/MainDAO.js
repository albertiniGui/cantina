function MainDAO(conexao, polo) {
    this.conexao = conexao.conexao;
    this.conexaoAcesso = conexao.conexaoAcesso;
    this.polo = polo;
}

MainDAO.prototype.selectAllPolo = function (tabela, coluna, callback) {
    this.conexao.query(`SELECT * 
    FROM ${tabela} WHERE polo = ${this.polo} 
    ORDER BY ${coluna} ASC`, callback);
}

MainDAO.prototype.selectAll = function (tabela, coluna, callback) {
    this.conexao.query(`SELECT * 
    FROM ${tabela}
    ORDER BY ${coluna} ASC`, callback);
}

MainDAO.prototype.sangria = function (data, callback) {
    this.conexao.query(`INSERT into sangria (valorInicial, valorAtual, saldo_sangria, polo) 
    values (?,?,?,?)`, [data.valorInicial, data.valorAtual, data.saldo, this.polo], callback)
}

MainDAO.prototype.insertLogin = function (data, callback) {
    this.conexao.query(`INSERT login (log_login,log_senha,tipo) 
    VALUES (?,sha2(?,512),?)`, [data.login, data.senha, data.tipo], callback)
}

MainDAO.prototype.insertUsuario = function (data, id_login, callback) {
    this.conexao.query(`INSERT usuario (usu_nome,usu_rg,usu_telefone,fk_idlogin) 
    VALUES (?,?,?,?)`, [data.nome, data.rg, data.telefone, id_login], callback)
}

MainDAO.prototype.checarCreditoMain = function (tag, callback) {
    this.conexaoAcesso.query(`SELECT car_credito from cartoes WHERE car_tag = ?`, tag, callback);
}

module.exports = function () {
    return MainDAO
}