function ComprasDAO(conexao, polo) {
    this.conexao = conexao.conexao;
    this.polo = polo;
}

ComprasDAO.prototype.selectCompras = function (callback) {
    this.conexao.query(`SELECT *, date_format(data_compra, '%Y-%m-%d') as data_compra, 
    date_format(data_compra, '%d/%m/%Y') as data_compraTable 
    FROM compra where polo = ?`, this.polo, callback)
}

ComprasDAO.prototype.insertCompra = function (data, callback) {
    this.conexao.query(`INSERT INTO compra(data_compra,desc_compra,valor_compra,fk_idFornecedor, produtos, polo) 
    VALUES (?,?,?,?,?,?)`, [data.data, data.titulo, data.valor, data.fornecedor, data.produtos, this.polo], callback)
}

ComprasDAO.prototype.updateEstoqueProduto = function (data, operacao, callback) {
    this.conexao.query(`UPDATE produto_final
    SET qtde_produtoFinal = qtde_produtoFinal ${operacao} ${data.qtde}
    where id_produtoFinal = ?
    and polo = ?`, [data.idProd, this.polo], callback)
}

ComprasDAO.prototype.updateEstoqueInsumo = function (data, operacao, callback) {
    this.conexao.query(`UPDATE insumo
    SET qtdeEstoque_insumo = qtdeEstoque_insumo ${operacao} ${data.qtde}
    where id_insumo = ?
    and polo = ? `, [data.idIns, this.polo], callback)
}

ComprasDAO.prototype.insertCompraAux = function (prodins, data, id, callback) {
    this.conexao.query(`INSERT into compra_aux(qtde_compraAux, valUnit_compraAux, fk_idCompra, ${prodins}, polo)
    values(?,?, (select max(LAST_INSERT_ID(id_compra)) from compra),?,?) `,
        [data.qtde, data.valor, id, this.polo], callback)
}

ComprasDAO.prototype.updateCompra = function (data, callback) {
    this.conexao.query(`UPDATE compra
    set data_compra = ?,
    desc_compra = ?,
    valor_compra = ?,
    fk_idFornecedor = ?,
    produtos = ?
    where id_compra = ?
    and polo = ? `, [data.data, data.titulo, data.valor, data.fornecedor, data.produtos, data.id, this.polo], callback)
}

ComprasDAO.prototype.deleteCompraAux = function (prodins, data, id, produto, callback) {
    this.conexao.query(`DELETE from compra_aux
    where fk_idCompra = ?
    and ${prodins} = ?
    and qtde_compraAux = ?
    and valUnit_compraAux like ?`, [data.id, id, produto.qtde, produto.valor], callback)
}

module.exports = function () {
    return ComprasDAO
}