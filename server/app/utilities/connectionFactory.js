const mysql = require('mysql');

function criaPools() {
    const conexao = mysql.createPool({
        connectionLimit: 50,
        host: 'localhost',
        user: 'root',
        password: 'root',
        database: 'fiec_erp',
        port: 3306
    });

    // BANCO NO SERVIDOR DE TESTE/PROD  
    const conexaoAcesso = mysql.createPool({
        connectionLimit: 50,
        host: 'localhost',
        user: 'root',
        password: 'root',
        database: 'teste_acesso_bd',
        port: 3306
    });

    // BANCO NO KINGHOST
    // const conexaoAcesso = mysql.createPool({
    //     connectionLimit: 50,
    //     host: 'mysql.fiecdev.com.br',
    //     user: 'fiecdev06',
    //     password: 'dev1985dev',
    //     database: 'fiecdev06',//COLOCAR NOME DO BANCO
    //     port: 3306
    // });

    conexoes = {
        conexao: conexao,
        conexaoAcesso: conexaoAcesso
    };

    return conexoes;
};

module.exports = function () {
    return criaPools
}