const schedule = require('node-schedule');


module.exports = function (app) {
    const conexao = app.utilities.connectionFactory();
    const todo_dia = new schedule.RecurrenceRule();
    todo_dia.dayOfWeek = [1, 2, 3, 4, 5, 6, 7];
    todo_dia.hour = 02;
    todo_dia.minute = 00;

    const rotina = schedule.scheduleJob(todo_dia, () => {
        console.log('CHECKING THE EXPIRING DATES AND REMOVING EXPIRING PRODUCTS ON: ' + new Date())
        if (conexao.conexao) {
            try {
                conexao.conexao.query(`delete from produto_final where valor_produtoFinal = valorDesc_produtoFinal and tipo_produto != 'Credit'`,
                    (err, result, fields) => {
                        if (err) {
                            console.log('ERROR IN DELETE')
                            conexao.conexao.end();
                            rotina.cancel();
                        } else {
                            if (result.length > 0) {
                                console.log('ITENS REMOVED')
                            } else {
                                console.log('NO ITENS TO BE REMOVED')
                            }
                            conexao.conexao.query('SELECT * FROM producao inner join produto_final on fk_idProdutoFinal = id_produtoFinal where dataValidade = curdate() +1',
                                (err, result, fields) => {
                                    if (err) {
                                        conexao.conexao.end();
                                        console.log('ERROR IN SELECT')
                                        rotina.cancel();
                                    } else {
                                        console.log('PRODUCTS SELECTED')
                                        if (result.length > 0) {
                                            for (let i = 0; i < result.length; i++) {
                                                conexao.conexao.query(`INSERT INTO produto_final(qtde_produtoFinal, valor_produtoFinal, valorDesc_produtoFinal, desc_produtoFinal, unidadeVenda_produtoFinal, tipo_produto, qtdeMax_produtoFinal, qtdeMin_produtoFinal, status_produtoFinal, img_produtoFinal, polo) values(?,?,?,?,?,?,?,?,?,?,?)`, [result[i].qtde_produtoFinal, result[i].valorDesc_produtoFinal, result[i].valorDesc_produtoFinal, result[i].desc_produtoFinal, result[i].unidadeVenda_produtoFinal, result[i].tipo_produto, result[i].qtdeMax_produtoFinal, result[i].qtdeMin_produtoFinal, result[i].status_produtoFinal, result[i].img_produtoFinal, result[i].polo], (err, result2, fields) => {
                                                    if (err) {
                                                        conexao.conexao.end();
                                                        console.log('ERROR IN INSERT')
                                                    } else {
                                                        console.log('PROMOCIONAL ITENS INSERTED')
                                                    }
                                                })
                                            }
                                        } else {
                                            console.log('NO PRODUCTS TO BE ADDED')
                                        }
                                        conexao.conexao.end();
                                        rotina.cancel();
                                    }
                                })
                        }
                    })
            }
            catch (err) {
                console.log(err)
            }
        }
        rotina.cancel();
    });

    return rotina;
}