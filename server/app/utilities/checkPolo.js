const prefixo_fiec1 = '10.1'
const prefixo_fiec2 = '10.0.200'

function set_localOuServidor(req) {
    if (req.header('x-forwarded-for')) {
        return checkPolos(req.header('x-forwarded-for'));
    }
    else {
        return polo = 1;
    }
}

function checkPolos(ip) {
    const octetos = ip.split('.')
    let prefixo = octetos[0] + '.' + octetos[1]
    if (prefixo == prefixo_fiec1) {
        return polo = 1;
    }
    else {
        prefixo += '.' + octetos[2]
        if (prefixo == prefixo_fiec2) {
            return polo = 2;
        }
    }
}

module.exports = function () {
    return set_localOuServidor;
}