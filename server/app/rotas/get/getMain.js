module.exports = function (app) {

    selectAll = (req, res) => {
        const tabela = req.query.tabela;
        const coluna = req.query.coluna;
        const conexao = app.utilities.connectionFactory();
        const polo = app.utilities.checkPolo(req);
        const MainDAO = new app.infra.MainDAO(conexao, polo);
        switch (tabela) {
            case 'compra':
            case 'compra_aux':
            case 'insumo':
            case 'producao':
            case 'produto_final':
            case 'venda':
            case 'venda_aux':
            case 'sangria':
                MainDAO.selectAllPolo(tabela, coluna, (err, result) => {
                    if (err) res.json({ status: 500 })
                    else res.json({ status: 200, result: result })
                    conexao.conexao.end();
                    conexao.conexaoAcesso.end();
                })
                break;
            default:
                MainDAO.selectAll(tabela, coluna, (err, result) => {
                    if (err) res.json({ status: 500 })
                    else res.json({ status: 200, result: result })
                    conexao.conexao.end();
                    conexao.conexaoAcesso.end();
                })
                break;
        }

    }

    checarCreditoMain = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        const polo = app.utilities.checkPolo(req);
        const MainDAO = new app.infra.MainDAO(conexao, polo);
        MainDAO.checarCreditoMain(req.params.tag, (err, result) => {
            if (err) res.json({ status: 500 })
            else res.json({ status: 200, result: result[0]["car_credito"] })
            conexao.conexao.end();
            conexao.conexaoAcesso.end();
        })
    }

    app.get('/fiecCantina/selectAll', selectAll)
    app.get('/fiecCantina/checarCreditoMain/:tag', checarCreditoMain)
}