module.exports = function (app) {

    selectCartoesConvidado = (req, res) => {
        const conexao = app.infra.connctionFactory();
        const LoginDAO = new app.infra.LoginDAO(conexao);
        LoginDAO.selectCartoesConvidado((err, result) => {
            if (err) res.status(500).json({ status: 500 })
            else res.json({ status: 200, result: result })
            conexao.conexao.end();
            conexao.conexaoAcesso.end();
        })
    }

    app.get('/fiecCantina/selectCartoesConvidados', selectCartoesConvidados);
}