const fs = require('fs');

module.exports = function (app) {

    verificarPolo = (req, res) => {
        const polo = app.utilities.checkPolo(req);
        res.json({ result: polo })
    }

    abrirPdf = (req, res) => {
        let file = fs.readFileSync(`./app/utilities/pdf/${req.query.fileName}.pdf`);
        res.writeHead(200, { 'Content-Type': 'Application/pdf' });
        res.end(file);
    }

    app.get('/fiecCantina/verificarPolo', verificarPolo)
    app.get('/fiecCantina/abrirPdf', abrirPdf)
}