module.exports = function (app) {
    function initDAO(req, conexao) {
        const polo = app.utilities.checkPolo(req);
        return ComprasDAO = new app.infra.ComprasDAO(conexao, polo);
    }

    function closeConnection(conexao) {
        conexao.conexao.end();
        conexao.conexaoAcesso.end();
    }

    function endDAO(err, res, result, conexao) {
        if (err) {
            console.log(err)
            res.json({ status: 500 })
        }
        else {
            res.json({ status: 200, result: result });
        }
        closeConnection(conexao);
    }

    // selectAll fica no getMain.js

    selectCompras = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ComprasDAO.selectCompras((err, result) => endDAO(err, res, result, conexao))
    }

    app.get('/fiecCantina/selectCompras', selectCompras)
}