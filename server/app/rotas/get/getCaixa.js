module.exports = function (app) {

    // FUNCTIONS REUTILIZAVEIS
    function initDAO(req, conexao) {
        const polo = app.utilities.checkPolo(req);
        return CaixaDAO = new app.infra.CaixaDAO(conexao, polo);
    }

    function closeConnection(conexao) {
        conexao.conexao.end();
        conexao.conexaoAcesso.end();
    }

    function endDAO(err, res, result, conexao) {
        if (err) {
            console.log(err)
            res.json({ status: 500 })
        }
        else {
            res.json({ status: 200, result: result });
        }
        closeConnection(conexao);
    }

    function endDAO2(err, res, result, conexao) {
        if (err) {
            console.log(err)
            res.json({ status: 500 })
        }
        else if (result.length > 0) res.json({ status: 200, result: result })
        else res.json({ status: 100 })
        closeConnection(conexao);
    }

    function erro(res, err, conexao) {
        console.log(err);
        res.json({ status: 500 })
        closeConnection(conexao)
    }
    // FUNCTIONS REUTILIZAVEIS

    comandasAbertas = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        CaixaDAO.comandasAbertas((err, result) => endDAO2(err, res, result, conexao))
    }

    checarCredito = (req, res) => {
        var tag = req.params.tag;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        CaixaDAO.checarCredito(tag, (err, result) => endDAO(err, res, result, conexao))
    }

    verificarComandaAbertaCaixa = (req, res) => {
        var tag = req.params.tag;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        CaixaDAO.comandaAberta(tag, (err, result) => {
            if (err) erro(res, err, conexao)
            else if (result.length > 0) {
                CaixaDAO.dadosComanda(result[0].id_venda, (err, result) => {
                    endDAO(err, res, result, conexao)
                })
            }
            else {
                res.json({ status: 100 })
                closeConnection(conexao)
            }

        })
    }

    app.get('/fiecCantina/comandasAbertas', comandasAbertas);
    app.get('/fiecCantina/checarCredito/:tag', checarCredito);
    app.get('/fiecCantina/verificarComandaAbertaCaixa/:tag', verificarComandaAbertaCaixa);
}