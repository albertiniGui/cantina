module.exports = function (app) {

    // FUNCTIONS REUTILIZAVEIS
    function initDAO(req, conexao) {
        const polo = app.utilities.checkPolo(req);
        return ComandaDAO = new app.infra.ComandaDAO(conexao, polo);
    }

    function closeConnection(conexao) {
        conexao.conexao.end();
        conexao.conexaoAcesso.end();
    }

    function endDAO(err, res, result, conexao) {
        if (err) {
            console.log(err)
            res.status(500).json({ status: 500 })
        }
        else {
            res.json({ status: 200, result: result });
        }
        closeConnection(conexao);
    }

    function endDAO2(err, res, result, conexao) {
        if (err) {
            console.log(err)
            res.status(500).json({ status: 500 })
        }
        else if (result.length > 0) res.json({ status: 200, result: result })
        else res.json({ status: 100 })
        closeConnection(conexao);
    }

    function erro(res, err, conexao) {
        console.log(err);
        closeConnection(conexao);
        res.status(500).json({ status: 500 })
    }
    // FUNCTIONS REUTILIZAVEIS


    selectProdutos = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ComandaDAO.selectProdutos((err, result) => endDAO(err, res, result, conexao))
    }

    selectCartoesConvidados = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ComandaDAO.selectCartoesConvidados((err, result) => endDAO(err, res, result, conexao))
    }

    verificarExistenciaCartao = (req, res) => {
        var tag = req.params.tag;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ComandaDAO.verificarExistenciaCartao(tag, (err, result) => endDAO2(err, res, result, conexao))
    }

    verificarTipoCartao = (req, res) => {
        var tag = req.params.tag;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ComandaDAO.verificarTipoCartao(tag, (err, result) => endDAO(err, res, result, conexao))
    }

    verificarComandaAberta = (req, res) => {
        var cartao = req.params.cartao;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ComandaDAO.verificarComandaAberta(cartao, (err, result) => {
            if (err) erro(res, err, conexao)
            else if (result.length > 0) ComandaDAO.selectDadosComandaAberta(result, (err, result) => endDAO(err, res, result, conexao))
            else {
                res.json({ status: 100 })
                closeConnection(conexao);
            }
        })
    }

    selectPorBarras = (req, res) => {
        var codigo_barras = req.params.codigo_barras;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ComandaDAO.selectPorBarras(codigo_barras, (err, result) => {
            endDAO2(err, res, result, conexao)
        })
    }

    app.get('/fiecCantina/selectProdutos', selectProdutos);
    app.get('/fiecCantina/selectCartoesConvidados', selectCartoesConvidados);
    app.get('/fiecCantina/verificarExistenciaCartao/:tag', verificarExistenciaCartao);
    app.get('/fiecCantina/verificarTipoCartao/:tag', verificarTipoCartao);
    app.get('/fiecCantina/verificarComandaAberta/:cartao', verificarComandaAberta);
    app.get('/fiecCantina/selectPorBarras/:codigo_barras', selectPorBarras);
}