module.exports = function (app) {

    // FUNCTIONS REUTILIZAVEIS
    function initDAO(req, conexao) {
        const polo = app.utilities.checkPolo(req);
        return ControleDAO = new app.infra.ControleDAO(conexao, polo);
    }

    function closeConnection(conexao) {
        conexao.conexao.end();
        conexao.conexaoAcesso.end();
    }

    function endDAO(err, res, result, conexao) {
        if (err) {
            console.log(err)
            res.json({ status: 500 })
        }
        else {
            res.json({ status: 200, result: result });
        }
        closeConnection(conexao);
    }

    function erro(res, err, conexao) {
        console.log(err);
        closeConnection(conexao)
        res.status(500).json({ status: 500 })
    }
    // FUNCTIONS REUTILIZAVEIS

    selectControleProduto = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ControleDAO.selectControleProduto((err, result) => endDAO(err, res, result, conexao))
    }

    selectProdReceitas = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ControleDAO.selectProdReceitas((err, result) => endDAO(err, res, result, conexao))
    }

    selectControleFreezer = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ControleDAO.selectControleFreezer((err, result) => endDAO(err, res, result, conexao))
    }

    // selectAll fica no getMain.js
    app.get(`/fiecCantina/selectControleProduto`, selectControleProduto)
    app.get(`/fiecCantina/selectProdReceitas`, selectProdReceitas)
    app.get(`/fiecCantina/selectControleFreezer`, selectControleFreezer)
}