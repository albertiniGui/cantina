module.exports = function (app) {

    logIn = function (req, res) {
        const conexao = app.utilities.connectionFactory();
        const LoginDAO = new app.infra.LoginDAO(conexao);
        LoginDAO.logIn(req.body, (err, result) => {
            if (err) res.status(500).json({ status: 500 })
            else if (result[0] != undefined) res.json({ status: 200, result: result, tipo: result[0].tipo })
            else res.json({ status: 100 })
            conexao.conexao.end();
            conexao.conexaoAcesso.end();
        })
    }

    app.post('/fiecCantina/logIn', logIn)

}