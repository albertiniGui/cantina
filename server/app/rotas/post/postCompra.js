module.exports = function (app) {
    // FUNCTIONS REUTILIZAVEIS
    function initDAO(req, conexao) {
        const polo = app.utilities.checkPolo(req);
        return ComprasDAO = new app.infra.ComprasDAO(conexao, polo);
    }

    function closeConnection(conexao) {
        conexao.conexao.end();
        conexao.conexaoAcesso.end();
    }

    function endDAO(err, res, result, conexao) {
        if (err) {
            console.log(err)
            res.json({ status: 500 })
        }
        else {
            res.json({ status: 200, result: result });
        }
        closeConnection(conexao);
    }

    function erro(res, err, conexao) {
        console.log(err);
        closeConnection(conexao)
        res.status(500).json({ status: 500 })
    }

    function contagemEnd(contagem, produtosAdd, produtosRem, err, res, conexao) {
        if (contagem == produtosAdd.length + produtosRem.length) {
            endDAO(err, res, null, conexao)
        }
    }
    // FUNCTIONS REUTILIZAVEIS

    cadastrarCompra = (req, res) => {
        var data = req.body
        var produtos = JSON.parse(data.produtos);
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ComprasDAO.insertCompra(data, (err) => {
            if (err) erro(res, err, conexao)
            else {
                produtos.forEach((produto, index) => {
                    if (produto.idProd) {
                        ComprasDAO.updateEstoqueProduto(produto, '+', (err) => {
                            if (err) erro(res, err, conexao)
                            else {
                                ComprasDAO.insertCompraAux('fk_idProduto', produto, produto.idProd, err => {
                                    if (index == produtos.length - 1) {
                                        endDAO(err, res, null, conexao)
                                    }
                                })
                            }
                        })
                    }
                    else {
                        ComprasDAO.updateEstoqueInsumo(produto, '+', (err) => {
                            if (err) erro(res, err, conexao)
                            else {
                                ComprasDAO.insertCompraAux('fk_idInsumo', produto, produto.idIns, err => {
                                    if (index == produtos.length - 1) {
                                        endDAO(err, res, null, conexao)
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }

    editarCompra = (req, res) => {
        var data = req.body;
        var produtos = JSON.parse(data.produtos);
        var produtosAdd = JSON.parse(data.produtosAdd);
        var produtosRem = JSON.parse(data.produtosRem);
        let contagem = 0;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ComprasDAO.updateCompra(data, err => {
            if (err) erro(res, err, conexao)
            else {
                produtosAdd.forEach(produto => {
                    if (produto.idProd) {
                        ComprasDAO.updateEstoqueProduto(produto, '+', err => {
                            if (err) erro(res, err, conexao)
                            else {
                                ComprasDAO.insertCompraAux('fk_idProduto', produto, produto.idProd, err => {
                                    if (err) erro(res, err, conexao)
                                    else {
                                        contagem++;
                                        contagemEnd(contagem, produtosAdd, produtosRem, err, res, conexao)
                                    }
                                })
                            }
                        })
                    }
                    else if (produto.idIns) {
                        ComprasDAO.updateEstoqueInsumo(produto, '+', err => {
                            if (err) erro(res, err, conexao)
                            else {
                                ComprasDAO.insertCompraAux('fk_idInsumo', produto, produto.idIns, err => {
                                    if (err) erro(res, err, conexao)
                                    else {
                                        contagem++;
                                        contagemEnd(contagem, produtosAdd, produtosRem, err, res, conexao)
                                    }
                                })
                            }
                        })
                    }
                })
                produtosRem.forEach(produto => {
                    if (produto.idProd) {
                        ComprasDAO.updateEstoqueProduto(produto, '-', err => {
                            if (err) erro(res, err, conexao)
                            else {
                                ComprasDAO.deleteCompraAux('fk_idProduto', data, produto.idProd, produto, err => {
                                    if (err) erro(res, err, conexao)
                                    else {
                                        contagem++;
                                        contagemEnd(contagem, produtosAdd, produtosRem, err, res, conexao)
                                    }
                                })
                            }
                        })
                    }
                    else if (produto.idIns) {
                        ComprasDAO.updateEstoqueInsumo(produto, '-', err => {
                            if (err) erro(res, err, conexao)
                            else {
                                ComprasDAO.deleteCompraAux('fk_idInsumo', data, produto.idIns, produto, err => {
                                    if (err) erro(res, err, conexao)
                                    else {
                                        contagem++;
                                        contagemEnd(contagem, produtosAdd, produtosRem, err, res, conexao)
                                    }
                                })
                            }
                        })
                    }
                })
                if (produtosAdd.length == 0 && produtosRem.length == 0) {
                    endDAO(err, res, null, conexao)
                }
            }
        })
    }

    app.post('/fiecCantina/cadastrarCompra', cadastrarCompra)
    app.post('/fiecCantina/editarCompra', editarCompra)
}