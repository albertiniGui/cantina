const pdf = require('html-pdf');
const htmlTemplate = require('angular-template');
const options = { prefix: 'ng' };

module.exports = function (app) {
    // FUNCTIONS REUTILIZAVEIS
    function initDAO(req, conexao) {
        const polo = app.utilities.checkPolo(req);
        return RelatorioDAO = new app.infra.RelatorioDAO(conexao, polo);
    }

    function closeConnection(conexao) {
        conexao.conexao.end();
        conexao.conexaoAcesso.end();
    }

    function endDAO(err, res, result, conexao) {
        if (err) {
            console.log(err)
            res.json({ status: 500 })
        }
        else if (result.length > 0) {
            res.json({ status: 200, result: result });
        }
        else {
            res.json({ status: 100 })
        }
        closeConnection(conexao);
    }

    function endDAO2(err, res, result, conexao) {
        if (err) {
            console.log(err)
            res.json({ status: 500 })
        }
        else {
            res.json({ status: 200, result: result });
        }
        closeConnection(conexao);
    }

    function erro(res, err, conexao) {
        console.log(err);
        closeConnection(conexao)
        res.status(500).json({ status: 500 })
    }
    // FUNCTIONS REUTILIZAVEIS

    relatorioCompras = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        var count = 0;
        var sqlQuery = ``;
        var titulo = req.body.titulo;
        var valor = req.body.valor;
        var dataIni = req.body.dataIni;
        var dataFim = req.body.dataFim;

        if (req.body.privilegio == 1) {
            polo = '1 or polo = 2'
        }

        if (count == 0) {
            if (titulo) {
                var tituloi = 1;
                sqlQuery += `and desc_compra like '%${titulo}%' `;
                count++;
            } else if (valor) {
                var valori = 1;
                sqlQuery += `and valor_compra like ${valor} `;
                count++;
            } else if (dataIni && dataFim) {
                var datasi = 1;
                if (dataFim == 'sysdate()' && dataIni == 'sysdate()') {
                    sqlQuery += `and data_compra between ${dataIni} and ${dataFim} `;
                } else if (dataFim == 'sysdate()' && dataIni != 'sysdate()') {
                    sqlQuery += `and data_compra >= '${dataIni}' `;
                } else if (dataFim != 'sysdate()' && dataIni == 'sysdate()') {
                    sqlQuery += `and data_compra <= '${dataFim}'`;
                } else {
                    sqlQuery += `and data_compra between '${dataIni}' and '${dataFim}' `;
                }
                count++;
            }
        }
        if (count > 0) {
            if (titulo && tituloi == undefined) {
                sqlQuery += `and desc_compra like '%${titulo}%'`;
                count++;
            }
            if (valor && valori == undefined) {
                sqlQuery += `and valor_compra like ${valor}`;
                count++;
            }
            if (dataIni && dataFim && datasi == undefined) {
                if (dataFim == 'sysdate()' && dataIni == 'sysdate()') {
                    sqlQuery += `and data_compra between ${dataIni} and ${dataFim} `;
                } else if (dataFim == 'sysdate()' && dataIni != 'sysdate()') {
                    sqlQuery += `and data_compra >= '${dataIni}' `;
                } else if (dataFim != 'sysdate()' && dataIni == 'sysdate()') {
                    sqlQuery += `and data_compra <= '${dataFim}'`;
                } else {
                    sqlQuery += `and data_compra between '${dataIni}' and '${dataFim}' `;
                }
            }
        }
        RelatorioDAO.relatorioCompras(sqlQuery, polo, (err, result) => endDAO(err, res, result, conexao))
    }

    relatorioInsumos = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        var count = 0;
        var sqlQuery = ``;
        var titulo = req.body.titulo;
        var estoque = req.body.estoque;
        var unidade = req.body.unidade;
        
        if (req.body.privilegio == 1) {
            polo = '1 or polo = 2'
        }
        
        if (count == 0) {
            if (titulo) {
                var tituloi = 1;
                sqlQuery += `and desc_insumo like '%${titulo}%' `;
                count++;
            } else if (estoque) {
                var estoquei = 1;
                sqlQuery += `and qtdeEstoque_insumo = ${estoque} `;
                count++;
            } else if (unidade) {
                var unidadei = 1;
                sqlQuery += `and unidade_insumo = '${unidade}' `;
                count++;
            }
        }
        if (count > 0) {
            if (titulo && tituloi == undefined) {
                sqlQuery += `and desc_compra like '%${titulo}%' `;
            }
            if (estoque && estoquei == undefined) {
                sqlQuery += `and qtdeEstoque_insumo = ${estoque} `;
            }
            if (unidade && unidadei == undefined) {
                sqlQuery += `and unidade_insumo = '${unidade}' `;
            }
        }
        RelatorioDAO.relatorioInsumos(sqlQuery, polo, (err, result) => endDAO(err, res, result, conexao))
    }

    relatorioProducoes = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        var count = 0;
        var sqlQuery = ``;
        var titulo = req.body.titulo;
        var dataIni = req.body.dataIni;
        var dataFim = req.body.dataFim;
        var status = req.body.status;
        var tipos = req.body.tipos;
        var hoje = req.body.hoje;

        if (req.body.privilegio == 1) {
            polo = '1 or producao.polo = 2'
        }

        if (count == 0) {
            if (titulo) {
                var tituloi = 1;
                sqlQuery += `and desc_produtoFinal like '%${titulo}%' `;
                count++;
            } else if (dataIni && dataFim) {
                var datasi = 1;
                if (dataFim == 'sysdate()' && dataIni == 'sysdate()') {
                    sqlQuery += `and dataValidade between ${dataIni} and ${dataFim} `;
                } else if (dataFim == 'sysdate()' && dataIni != 'sysdate()') {
                    sqlQuery += `and dataValidade >= '${dataIni}' `;
                } else if (dataFim != 'sysdate()' && dataIni == 'sysdate()') {
                    sqlQuery += `and dataValidade <= '${dataFim}'`;
                } else {
                    sqlQuery += `and dataValidade between '${dataIni}' and '${dataFim}' `;
                }
                count++;
            } else if (status) {
                var statusi = 1;
                sqlQuery += `and status_producao = '${status}' `;
                count++;
            } else if (tipos) {
                var tiposi = 1;
                sqlQuery += `and tipo_produto = '${tipos}' `;
            } else if (hoje) {
                var hojei = 1;
                sqlQuery += `and dataProducao between current_date() and sysdate() `;
            }
        }
        if (count > 0) {
            if (titulo && tituloi == undefined) {
                sqlQuery += `and desc_produtoFinal like '%${titulo}%'`;
            }
            if (dataIni && dataFim && datasi == undefined) {

                if (dataFim == 'sysdate()' && dataIni == 'sysdate()') {
                    sqlQuery += `and dataValidade between ${dataIni} and ${dataFim} `;
                } else if (dataFim == 'sysdate()' && dataIni != 'sysdate()') {
                    sqlQuery += `and dataValidade between '${dataIni}' and ${dataFim} `;
                } else if (dataFim != 'sysdate()' && dataIni == 'sysdate()') {
                    sqlQuery += `and dataValidade <= '${dataFim}' `;
                } else {
                    sqlQuery += `and dataValidade between '${dataIni}' and '${dataFim}' `;
                }
            }
            if (status && statusi == undefined) {
                sqlQuery += `and status_producao = '${status}' `
            }
            if (tipos && tiposi == undefined) {
                sqlQuery += `and tipo_produto = '${tipos}' `
            }
            if (hoje) {
                var hojei = 1;
                sqlQuery += `and dataProducao between current_date() and sysdate() `;
            }
        }
        RelatorioDAO.relatorioProducoes(sqlQuery, polo, (err, result) => endDAO(err, res, result, conexao))
    }

    relatorioProdutos = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        var count = 0;
        var sqlQuery = ``;
        var titulo = req.body.titulo;
        var tipos = req.body.tipos;
        var status = req.body.status;
        var valor = req.body.valor;
        var valorPromo = req.body.valorPromo;

        if (req.body.privilegio == 1) {
            polo = '1 or polo = 2'
        }

        if (count == 0) {
            if (titulo) {
                var tituloi = 1;
                sqlQuery += `and desc_produtoFinal like '%${titulo}%' `;
                count++;
            } else if (valor) {
                var valori = 1;
                sqlQuery += `and valor_produtoFinal = ${valor} `;
                count++;
            } else if (valorPromo) {
                var valorPromoi = 1;
                sqlQuery += `and valorDesc_produtoFinal = ${valorPromo} `;
                count++;
            } else if (status) {
                var statusi = 1;
                sqlQuery += `and status_produtoFinal = '${status}' `;
                count++;
            } else if (tipos) {
                var tiposi = 1;
                sqlQuery += `and tipo_produto = '${tipos}' `;
            }
        }
        if (count > 0) {
            if (titulo && tituloi == undefined) {
                sqlQuery += `and desc_produtoFinal like '%${titulo}%' `;
            }
            if (valor && valori == undefined) {
                sqlQuery += `and valor_produtoFinal = ${valor} `
            }
            if (valorPromo && valorPromoi == undefined) {
                sqlQuery += `and valorDesc_produtoFinal = ${valorPromo} `
            }
            if (status && statusi == undefined) {
                sqlQuery += `and status_produtoFinal = '${status}' `
            }
            if (tipos && tiposi == undefined) {
                sqlQuery += `and tipo_produto = '${tipos}' `
            }
        }
        RelatorioDAO.relatorioProdutos(sqlQuery, polo, (err, result) => endDAO(err, res, result, conexao))
    }

    relatorioReceitas = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        var count = 0;
        var sqlQuery = ``;
        var titulo = req.body.titulo;
        var tempo = req.body.tempo;
        var rendimento = req.body.rendimento;
        var custo = req.body.custo;

        if (count == 0) {
            if (titulo) {
                var tituloi = 1;
                sqlQuery += `where desc_receita like '%${titulo}%' `;
                count++;
            } else if (tempo) {
                var tempoi = 1;
                sqlQuery += `where tempoPreparo_receita = ${tempo} `;
                count++;
            } else if (rendimento) {
                var rendimentoi = 1;
                sqlQuery += `where rendimento = ${rendimento} `;
                count++;
            } else if (custo) {
                var custoi = 1;
                sqlQuery += `where custo = ${custo} `;
                count++;
            }
        }
        if (count > 0) {
            if (titulo && tituloi == undefined) {
                sqlQuery += `and desc_receita like '%${titulo}%' `;
            }
            if (tempo && tempoi == undefined) {
                sqlQuery += `and tempoPreparo_receita = ${tempo} `;
            }
            if (rendimento && rendimentoi == undefined) {
                sqlQuery += `and rendimento = ${rendimento} `
            }
            if (custo && custoi == undefined) {
                sqlQuery += `and custo = ${custo} `
            }
        }
        RelatorioDAO.relatorioReceitas(sqlQuery, (err, result) => endDAO(err, res, result, conexao))
    }

    relatorioVendas = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        var count = 0;
        var sqlQuery = ``;
        var dataIni = req.body.dataIni;
        dataIni += ' 00:00:00';
        var dataFim = req.body.dataFim;
        dataFim += ' 23:59:59';
        var total = req.body.total;
        var comprador = req.body.comprador;
        var funcionario = req.body.funcionario;
        var dinheiro = req.body.dinheiro;
        var debito = req.body.debito;
        var credito = req.body.credito;
        var fiec = req.body.fiec;
        var manha = req.body.manha;
        var tarde = req.body.tarde;
        var noite = req.body.noite;
        var hoje = req.body.hoje;
        var FIECI = false;
        var FIECII = false;
        if (req.body.privilegio != 1) {
            if (polo == 1) {
                FIECI = true;
            }
            else {
                FIECII = true;
            }
        }
        if (count == 0) {
            if (total) {
                var totali = 1;
                sqlQuery += `where total_venda = ${total} `;
                count++;
            }
            else if (comprador) {
                var compradori = 1;
                sqlQuery += `where fk_venda_cartao = ${comprador} `;
                count++;
            }
            else if (funcionario) {
                var funcionarioi = 1;
                sqlQuery += `where fk_usu_venda = ${funcionario} `;
                count++;
            }
            else if (dinheiro) {
                var dinheiroi = 1;
                sqlQuery += `where pag_dinheiro != 0 `;
                count++;
            }
            else if (debito) {
                var debitoi = 1;
                sqlQuery += `where pag_debito != 0 `;
                count++;
            }
            else if (credito) {
                var creditoi = 1;
                sqlQuery += `where pag_credito != 0 `;
                count++;
            }
            else if (fiec) {
                var fieci = 1;
                sqlQuery += `where pag_cardFiec != 0 `;
                count++;
            } else if (manha) {
                var manhai = 1;
                sqlQuery += `where time(data_venda) between '00:00:00' and '11:59:59' `;
                count++;
            } else if (tarde) {
                var tardei = 1;
                sqlQuery += `where time(data_venda) between '12:00:00' and '17:59:59' `;
                count++;
            } else if (FIECI) {
                var FIECIi = 1;
                sqlQuery += `where polo = 1 `;
                count++;
            } else if (FIECII) {
                var FIECIIi = 1;
                sqlQuery += `where polo = 2 `;
                count++;
            } else if (hoje) {
                var hojei = 1;
                sqlQuery += `where data_venda between current_date() and sysdate() `;
                count++;
            } else if (noite) {
                var noitei = 1;
                sqlQuery += `where time(data_venda) between '18:00:00' and '23:59:59' `;
                count++;
            } else if (dataIni && dataFim) {
                var datasi = 1;
                if (dataFim == 'sysdate()' && dataIni == 'sysdate()') {
                    sqlQuery += `where data_venda between ${dataIni} and ${dataFim} `;
                } else if (dataFim == 'sysdate()' && dataIni != 'sysdate()') {
                    sqlQuery += `where data_venda >= '${dataIni}' `;
                } else if (dataFim != 'sysdate()' && dataIni == 'sysdate()') {
                    sqlQuery += `where data_venda <= '${dataFim}' `;
                } else {
                    sqlQuery += `where data_venda between '${dataIni}' and '${dataFim}' `;
                }
                count++;
            }
        }

        if (count > 0) {
            if (total && totali == undefined) {
                sqlQuery += `and total_venda = ${total} `;
            }
            if (comprador && compradori == undefined) {
                sqlQuery += `and fk_venda_cartao = ${comprador} `;
            }
            if (funcionario && funcionarioi == undefined) {
                sqlQuery += `and fk_usu_venda = ${funcionario} `;
            }
            if (dinheiro && dinheiroi == undefined) {
                sqlQuery += `and pag_dinheiro != 0 `;
            }
            if (debito && debitoi == undefined) {
                sqlQuery += `and pag_debito != 0 `;
            }
            if (credito && creditoi == undefined) {
                sqlQuery += `and pag_credito != 0 `;
            }
            if (fiec && fieci == undefined) {
                sqlQuery += `and pag_cardFiec != 0 `;
            }
            if (FIECI && FIECIi == undefined) {
                if (FIECIIi) {
                    sqlQuery += `or polo = 1 `;
                } else {
                    sqlQuery += `and polo = 1 `;
                    FIECIIi = 1;
                }
            }
            if (FIECII && FIECIIi == undefined) {
                if (FIECIi) {
                    sqlQuery += `or polo = 2 `;
                } else {
                    sqlQuery += `and polo = 2 `;
                    FIECIi = 1;
                }
            }
            if (hoje) {
                sqlQuery += `and data_venda between current_date() and sysdate() `;
            }
            if (manha && manhai == undefined) {
                sqlQuery += `and time(data_venda) between '00:00:00' and '11:59:59' `;
            }
            if (tarde && tardei == undefined) {
                sqlQuery += `and time(data_venda) between '12:00:00' and '17:59:59' `;
            }
            if (noite && noitei == undefined) {
                sqlQuery += `and time(data_venda) between '18:00:00' and '23:59:59' `;
            }
            if (dataIni && dataFim && datasi == undefined) {
                if (dataFim == 'sysdate()' && dataIni == 'sysdate()') {
                    sqlQuery += `and data_venda between ${dataIni} and ${dataFim} `;
                } else if (dataFim == 'sysdate()' && dataIni != 'sysdate()') {
                    sqlQuery += `and data_venda >= '${dataIni}' `;
                } else if (dataFim != 'sysdate()' && dataIni == 'sysdate()') {
                    sqlQuery += `and data_venda <= '${dataFim}' `;
                } else {
                    sqlQuery += `and data_venda between '${dataIni}' and '${dataFim}' `;
                }

            }
        }
        RelatorioDAO.relatorioVendas(sqlQuery, (err, result) => {
            if (err) erro(res, err, conexao)
            else if (result.length > 0) {
                let count = 0;
                result.forEach((r, index) => {
                    RelatorioDAO.relatorioVendasAux(r.id_venda, polo, (err, resultAux) => {
                        if (err) erro(res, err, conexao)
                        else {
                            let newResult = [];
                            r.detalhesVenda = resultAux;
                            newResult = result;
                            if (index == result.length - 1) {
                                res.json({ status: 200, result: result })
                                // GAMBIARRA PARA ESPERAR TODOS OS RESULTS ANTES DE FECHAR CONEXAO
                                // ARRUMAR
                                setTimeout(() => {
                                    closeConnection(conexao)
                                }, 70)
                            }
                        }
                    })
                })
            }
            else {
                res.json({ status: 100 })
                closeConnection(conexao)
            }
        })
    }

    relatorioRegistros = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        var count = 0;
        var sqlQuery = ``;
        var titulo = req.body.titulo;
        var dataIni = req.body.dataIni;
        var dataFim = req.body.dataFim;
        var tempMin = req.body.tempMin;
        var tempMax = req.body.tempMax;

        if (req.body.privilegio == 1) {
            polo = '1 or freezer.polo = 2'
        }

        if (count == 0) {
            if (titulo) {
                var tituloi = 1;
                sqlQuery += `and id_freezer like '%${titulo}%' `;
                count++;
            } else if (tempMin) {
                var tempMini = 1;
                sqlQuery += `and tempMin_freezer = ${tempMin} `;
                count++;
            } else if (tempMax) {
                var tempMaxi = 1;
                sqlQuery += `and tempMax_freezer = ${tempMax} `;
                count++;
            } else if (dataIni && dataFim) {
                var datasi = 1;
                if (dataFim == 'sysdate()' && dataIni == 'sysdate()') {
                    sqlQuery += `and data_freezer between ${dataIni} and ${dataFim} `;
                } else if (dataFim == 'sysdate()' && dataIni != 'sysdate()') {
                    sqlQuery += `and data_freezer >= '${dataIni} 00:00:00' `;
                } else if (dataFim != 'sysdate()' && dataIni == 'sysdate()') {
                    sqlQuery += `and data_freezer <= '${dataFim} 00:00:00'`;
                } else {
                    sqlQuery += `and data_freezer between '${dataIni} 00:00:00' and '${dataFim} 00:00:00' `;
                }
                count++;
            }
        }
        if (count > 0) {
            if (titulo && tituloi == undefined) {
                sqlQuery += `and id_freezer like '%${titulo}%' `;
            }
            if (tempMin && tempMini == undefined) {
                sqlQuery += `and tempMin_freezer = ${tempMin} `;
            }
            if (tempMax && tempMaxi == undefined) {
                sqlQuery += `and tempMax_freezer = ${tempMax} `;
            }
            if (dataIni && dataFim && datasi == undefined) {
                if (dataFim == 'sysdate()' && dataIni == 'sysdate()') {
                    sqlQuery += `and data_freezer between ${dataIni} and ${dataFim} `;
                } else if (dataFim == 'sysdate()' && dataIni != 'sysdate()') {
                    sqlQuery += `and data_freezer >= '${dataIni} 00:00:00' `;
                } else if (dataFim != 'sysdate()' && dataIni == 'sysdate()') {
                    sqlQuery += `and data_freezer <= '${dataFim} 00:00:00' `;
                } else {
                    sqlQuery += `and data_freezer between '${dataIni} 00:00:00' and '${dataFim} 00:00:00' `;
                }
            }
        }
        RelatorioDAO.relatorioRegistros(sqlQuery, polo, (err, result) => endDAO(err, res, result, conexao))
    }

    relatorioUsuarios = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        var count = 0;
        var sqlQuery = `SELECT usuario.*, sum(total_venda) as total_vendido FROM venda join usuario on venda.fk_usu_venda = usuario.usu_id where month(data_venda) = month(CURDATE()) `;

        var nomeFunc = req.body.nomeFunc;
        var codFunc = req.body.codFunc;
        var hoje = req.body.hoje;
        var mes = req.body.mes;
        var newQuery = String;
        if (count == 0) {
            if (nomeFunc) {
                var nomeFunci = 1;
                newQuery = `where usu_nome like '%${nomeFunc}%' `;
                sqlQuery += newQuery;
                count++;
            } else if (codFunc) {
                var codFunci = 1;
                newQuery = `where usu_id = ${codFunc} `
                sqlQuery += newQuery;
                count++;
            } else if (hoje) {
                var hojei = 1;
                sqlQuery = `SELECT usuario.*, (select sum(total_venda) from venda where Date(data_venda) = CURDATE()) as total_vendido from usuario inner join venda on usu_id = venda.fk_usu_venda `
                newQuery = `where DATE('${hoje}') = CURDATE() `;
                sqlQuery += newQuery;
                count++;
            } else if (mes) {
                var mesi = 1;
                sqlQuery = `SELECT usuario.*, (select sum(total_venda) from venda where month(data_venda) = month(CURDATE())) as total_vendido from usuario inner join venda on usu_id = venda.fk_usu_venda `;
                newQuery = `where MONTH('${mes}') = MONTH(CURDATE()) AND YEAR('${mes}') = YEAR(CURDATE()) `;
                sqlQuery += newQuery;
                count++;
            }

        }
        if (count > 0) {
            if (nomeFunc && nomeFunci == undefined) {
                sqlQuery += `and usu_nome like '%${nomeFunc}%' `;
            }
            if (codFunc && codFunci == undefined) {
                sqlQuery += `and usu_id = ${codFunc} `;
            }
            if (hoje && hojei == undefined) {
                sqlQuery = `SELECT usuario.*, (select sum(total_venda) from venda where Date(data_venda) = CURDATE()) as total_vendido from usuario inner join venda on usu_id = venda.fk_usu_venda ` + newQuery;
                sqlQuery += `and Date('${hoje}') = CURDATE() `
            }
            if (mes && mesi == undefined) {
                sqlQuery += `and MONTH('${mes}') = MONTH(CURDATE()) AND YEAR('${mes}') = YEAR(CURDATE()) `
            }
        }
        RelatorioDAO.relatorioUsuarios(sqlQuery, (err, result) => endDAO(err, res, result, conexao))
    }

    somarProdutos = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        let sqlQuery = '';
        req.body.id_vendas.forEach(id => {
            sqlQuery += `or id_venda = ${id} `;
        })
        RelatorioDAO.somarProdutos(sqlQuery, (err, result) => endDAO2(err, res, result, conexao))
    }

    gerarPdf = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        var options = { format: 'Letter', base: './html' };
        var data = req.body;
        var path = `./app/utilities/html/${req.body.htmlFile}.html`;
        data.nodeenv = process.env.NODE_ENV;
        data.total = 0;
        data.dinheiro = 0;
        data.credito = 0;
        data.debito = 0;
        data.cardFiec = 0;
        data.index = 0;
        var renderedHtml = htmlTemplate(path, data, options);

        pdf.create(renderedHtml, options).toFile(`./app/utilities/pdf/${req.body.fileName}.pdf`, (err, result) => {
            if (err) erro(res, err, conexao)
            else res.json({ status: 200 })
        });
    }

    app.post('/fiecCantina/relatorioCompras', relatorioCompras)
    app.post('/fiecCantina/relatorioInsumos', relatorioInsumos)
    app.post('/fiecCantina/relatorioProducoes', relatorioProducoes)
    app.post('/fiecCantina/relatorioProdutos', relatorioProdutos)
    app.post('/fiecCantina/relatorioReceitas', relatorioReceitas)
    app.post('/fiecCantina/relatorioVendas', relatorioVendas)
    app.post('/fiecCantina/relatorioRegistros', relatorioRegistros)
    app.post('/fiecCantina/relatorioUsuarios', relatorioUsuarios)
    app.post('/fiecCantina/somarProdutos', somarProdutos)
    app.post('/fiecCantina/gerarPdf', gerarPdf)
}