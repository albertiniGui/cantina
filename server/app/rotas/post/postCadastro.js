const fs = require('fs')

module.exports = function (app) {

    // FUNCTIONS REUTILIZAVEIS
    function initDAO(req, conexao) {
        const polo = app.utilities.checkPolo(req);
        return CadastroDAO = new app.infra.CadastroDAO(conexao, polo);
    }

    function closeConnection(conexao) {
        conexao.conexao.end();
        conexao.conexaoAcesso.end();
    }

    function endDAO(err, res, result, conexao) {
        if (err) {
            console.log(err)
            res.json({ status: 500 })
        }
        else{
            res.json({ status: 200, result: result });
        }
        closeConnection(conexao);
    }

    function erro(res, err, conexao) {
        console.log(err);
        closeConnection(conexao)
        res.status(500).json({ status: 500 })
    }
    // FUNCTIONS REUTILIZAVEIS

    // ----------------PRODUTO-------------------
    cadastrarProduto = (req, res) => {
        var data = req.body;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        CadastroDAO.checarExistencia('id_produtofinal', 'produto_final',
            'desc_produtoFinal', `'${data.desc_produtoFinal}' or codigo_barras like ${data.codigo_barras}`, (err, result) => {
                if (err) erro(res, err, conexao)
                else if (result.length == 0) {
                    CadastroDAO.cadastrarProduto(data, 1, (err, result) => {
                        if (err) erro(res, err, conexao)
                        else {
                            CadastroDAO.cadastrarProduto(data, 2, (err, result) => endDAO(err, res, result, conexao))
                        }
                    })
                }
                else res.status(100).json({ status: 100 })
            })
    }

    editarProduto = (req, res) => {
        var data = req.body;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        CadastroDAO.editarProduto(data, (err, result) => {
            if (err) erro(res, err, conexao)
            else {
                CadastroDAO.editarProdutoReceita(data, (err, result) => endDAO(err, res, result, conexao))
            }
        })
    }

    // ----------------INSUMO-------------------
    cadastrarInsumo = (req, res) => {
        var data = req.body;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        CadastroDAO.checarExistencia('id_insumo', 'insumo', 'desc_insumo', data.desc_insumo, (err, result) => {
            if (err) erro(res, err, conexao)
            else if (result.length == 0) {
                CadastroDAO.cadastrarInsumo(data, 1, (err, result) => {
                    if (err) erro(res, err, conexao)
                    else {
                        CadastroDAO.cadastrarInsumo(data, 2, (err, result) => endDAO(err, res, result, conexao))
                    }
                })
            }
            else res.status(100).json({ status: 100 })
        })
    }

    editarInsumo = (req, res) => {
        var data = req.body;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        CadastroDAO.editarInsumo(data, (err, result) => endDAO(err, res, result, conexao))
    }

    // ----------------RECEITA-------------------
    cadastrarReceita = (req, res) => {
        var data = req.body;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        CadastroDAO.checarExistencia('desc_receita', 'receita', 'desc_receita', data.desc_receita, (err, result) => {
            if (err) erro(res, err, conexao)
            else if (result.length == 0) CadastroDAO.cadastrarReceita(data, (err, result) => endDAO(err, res, result, conexao))
            else res.status(100).json({ status: 100 })
        })
    }

    editarReceita = (req, res) => {
        var data = req.body;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        CadastroDAO.editarReceita(data, (err, result) => endDAO(err, res, result, conexao))
    }

    // ----------------FREEZER-------------------
    cadastrarFreezer = (req, res) => {
        var data = req.body;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        CadastroDAO.cadastrarFreezer(data, (err, result) => endDAO(err, res, result, conexao))
    }

    editarFreezer = (req, res) => {
        var data = req.body;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        CadastroDAO.editarFreezer(data, (err, result) => endDAO(err, res, result, conexao))
    }

    saveImagem = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        fs.writeFile(__dirname.split('server')[0] + `/dist/assets/images/${req.body.name}`, req.body.base, { encoding: 'base64' },
            (err, result) => endDAO(err, res, null, conexao))
    }

    app.post('/fiecCantina/cadastrarProduto', cadastrarProduto);
    app.post('/fiecCantina/editarProduto', editarProduto);
    app.post('/fiecCantina/cadastrarInsumo', cadastrarInsumo);
    app.post('/fiecCantina/editarInsumo', editarInsumo);
    app.post('/fiecCantina/cadastrarReceita', cadastrarReceita);
    app.post('/fiecCantina/editarReceita', editarReceita);
    app.post('/fiecCantina/cadastrarFreezer', cadastrarFreezer);
    app.post('/fiecCantina/editarFreezer', editarFreezer);
    app.post('/fiecCantina/saveImage', saveImagem);
}