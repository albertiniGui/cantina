module.exports = function (app) {

    // FUNCTIONS REUTILIZAVEIS
    function initDAO(req, conexao) {
        const polo = app.utilities.checkPolo(req);
        return CaixaDAO = new app.infra.CaixaDAO(conexao, polo);
    }

    function closeConnection(conexao) {
        conexao.conexao.end();
        conexao.conexaoAcesso.end();
    }

    function endDAO(err, res, result, conexao) {
        if (err) {
            console.log(err)
            res.json({ status: 500 })
        }
        else {
            res.json({ status: 200, result: result });
        }
        closeConnection(conexao);
    }

    function erro(res, err, conexao) {
        console.log(err);
        closeConnection(conexao)
        res.status(500).json({ status: 500 })
    }
    // FUNCTIONS REUTILIZAVEIS

    insertCredito = (req, res) => {
        var data = req.body;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        CaixaDAO.insertCredito(data, (err, result) => endDAO(err, res, result, conexao))
    }

    pagarConta = (req, res) => {
        var data = req.body;
        let lista = JSON.parse(req.body.lista_pedidos);
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        CaixaDAO.liberarCartao(data.id_comprou, (err, result) => {
            if (err) erro(res, err, conexao)
            else {
                CaixaDAO.updatePagamento(data, (err, result) => {
                    if (err) erro(res, err, conexao)
                    else {
                        lista.forEach((item, index) => {
                            CaixaDAO.updateEstoque(item, (err, result) => {
                                if (index == lista.length - 1) endDAO(err, res, null, conexao)
                            })
                        })
                    }
                })
            }
        })
    }

    updateCredito = (req, res) => {
        var data = req.body;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        CaixaDAO.updateCredito(data, (err, result) => endDAO(err, res, result, conexao))
    }

    atualizaSangria = (req, res) => {
        var data = req.body;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        CaixaDAO.selectUltimaSangria((err, result) => {
            if (err) erro(res, err, conexao)
            else {
                CaixaDAO.updateSangria(data.totalVenda, result[0].id_sangria, (err, result) => endDAO(err, res, result, conexao))
            }
        })
    }

    checarSenha = (req, res) => {
        var data = req.body;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        CaixaDAO.checarSenha(data, (err, result) => {
            if (err) erro(res, err, conexao)
            else if (result[0].count == 1) {
                res.send({ status: 200 })
                closeConnection(conexao)
            }
            else {
                res.send({ status: 100 });
                closeConnection(conexao)
            }
        })

    }

    app.post('/fiecCantina/insertCredito', insertCredito);
    app.post('/fiecCantina/pagarConta', pagarConta);
    app.post('/fiecCantina/updateCredito', updateCredito);
    app.post('/fiecCantina/atualizaSangria', atualizaSangria);
    app.post('/fiecCantina/checarSenha', checarSenha);
}