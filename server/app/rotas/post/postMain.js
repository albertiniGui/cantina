module.exports = function (app) {

    sangria = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        const polo = app.utilities.checkPolo(req);
        const MainDAO = new app.infra.MainDAO(conexao, polo);
        MainDAO.sangria(req.body, (err, result) => {
            if (err) {
                res.json({ status: 500 })
            }
            else res.json({ status: 200, result: result })
            conexao.conexao.end();
            conexao.conexaoAcesso.end();
        })
    }

    cadastrarUsuario = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        const polo = app.utilities.checkPolo(req);
        const MainDAO = new app.infra.MainDAO(conexao, polo);
        MainDAO.insertLogin(req.body, (err, result) => {
            if (err) res.status(500).json({ status: 500 })
            else {
                MainDAO.insertUsuario(req.body, result.insertId, (err, result) => {
                    if (err) res.json({ status: 500 })
                    else res.json({ status: 200 })
                    conexao.conexao.end();
                    conexao.conexaoAcesso.end();
                })
            }
        })
    }

    app.post('/fiecCantina/sangria', sangria);
    app.post('/fiecCantina/cadastrarUsuario', cadastrarUsuario);
}