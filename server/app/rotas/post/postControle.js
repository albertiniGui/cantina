module.exports = function (app) {

    // FUNCTIONS REUTILIZAVEIS
    function initDAO(req, conexao) {
        const polo = app.utilities.checkPolo(req);
        return ControleDAO = new app.infra.ControleDAO(conexao, polo);
    }

    function closeConnection(conexao) {
        conexao.conexao.end();
        conexao.conexaoAcesso.end();
    }

    function endDAO(err, res, result, conexao) {
        if (err) {
            console.log(err)
            res.json({ status: 500 })
        }
        else {
            res.json({ status: 200, result: result });
        }
        closeConnection(conexao);
    }

    function erro(res, err, conexao) {
        console.log(err);
        closeConnection(conexao)
        res.status(500).json({ status: 500 })
    }
    // FUNCTIONS REUTILIZAVEIS

    insertProducao = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ControleDAO.selectIDReceita(req.body, (err, result) => {
            if (err) erro(res, err, conexao)
            else if (result.length == 0) {
                closeConnection(conexao)
                res.json({ status: 100 })
            }
            else {
                var rendimento = result[0].rendimento;
                ControleDAO.insertProducao(req.body, result[0].id_receita, err => {
                    if (err) erro(res, err, conexao)
                    else {
                        ControleDAO.updateProdutoFinal(req.body, rendimento, err => endDAO(err, res, null, conexao))
                    }
                })
            }
        })
    }

    updateProducao = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ControleDAO.updateProducao(req.body, err => endDAO(err, res, null, conexao))
    }

    deleteProducao = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ControleDAO.deleteProducao(req.body, err => endDAO(err, res, null, conexao))
    }

    entradaEstoqueInsumo = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ControleDAO.entradaEstoqueInsumo(req.body, err => endDAO(err, res, null, conexao))
    }

    updateEntrada = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ControleDAO.updateEntrada(req.body, err => endDAO(err, res, null, conexao))
    }

    deleteInsumo = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        let contagem = 0;
        let prosseguir = true;
        ControleDAO.selectInsumoReceita((err, result) => {
            if (err) erro(res, err, conexao)
            else {
                const nome_insumo_a_excluir = req.body.insumo;
                for (let i = 0; i < result.length; i++) {
                    const lista_de_ingredientes = JSON.parse(result[i].ingredientes)
                    const achados = lista_de_ingredientes.filter((ingredientesObj) => ingredientesObj.nome.trim() === nome_insumo_a_excluir.trim())
                    if (achados.length == 0) {
                        contagem++;
                        if (contagem == result.length) {
                            ControleDAO.deleteInsumo(req.body, (err, result) => {
                                if (err) erro(res, err, conexao)
                                else {
                                    ControleDAO.selectInsumos((err, result) => endDAO(err, res, result, conexao));
                                }
                            })
                        }
                    }
                    else {
                        prosseguir = false;
                    }
                }
                if (prosseguir == false) {
                    res.json({ status: 100 })
                    closeConnection(conexao)
                }
            }
        })
    }

    cadastrarRegistro = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ControleDAO.cadastrarRegistro(req.body, err => endDAO(err, res, null, conexao))
    }

    editarRegistro = (req, res) => {
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ControleDAO.editarRegistro(req.body, err => endDAO(err, res, null, conexao))
    }

    app.post('/fiecCantina/insertProducao', insertProducao)
    app.post('/fiecCantina/updateProducao', updateProducao)
    app.post('/fiecCantina/deleteProducao', deleteProducao)
    app.post('/fiecCantina/entradaEstoqueInsumo', entradaEstoqueInsumo)
    app.post('/fiecCantina/updateEntrada', updateEntrada)
    app.post('/fiecCantina/deleteInsumo', deleteInsumo)
    app.post('/fiecCantina/cadastrarRegistro', cadastrarRegistro)
    app.post('/fiecCantina/editarRegistro', editarRegistro)
}