const winston = require('winston');
const fs = require('fs');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;

const myFormat = printf(({ message }) => {
    return `${message}`;
});

function logger(folderName, fileName) {
    return winston.createLogger({
        format: myFormat,
        transports: [
            new winston.transports.File({ filename: __dirname.split('app')[0] + `logs_cancelamento/${folderName}/${fileName}.log` })
        ]
    });
}

module.exports = function (app) {

    // FUNCTIONS REUTILIZAVEIS
    function initDAO(req, conexao) {
        const polo = app.utilities.checkPolo(req);
        return ComandaDAO = new app.infra.ComandaDAO(conexao, polo);
    }

    function closeConnection(conexao) {
        conexao.conexao.end();
        conexao.conexaoAcesso.end();
    }

    function endDAO(err, res, result, conexao) {
        if (err) {
            console.log(err)
            res.json({ status: 500 })
        }
        else {
            res.json({ status: 200, result: result });
        }
        closeConnection(conexao);
    }

    function erro(res, err, conexao) {
        console.log(err);
        closeConnection(conexao)
        res.status(500).json({ status: 500 })
    }
    // FUNCTIONS REUTILIZAVEIS

    salvarComanda = (req, res) => {
        var data = req.body;
        var lista = JSON.parse(data.pedidos);
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ComandaDAO.bloquearCartao(data.id_cartao, (err, result) => {
            if (err) erro(res, err, conexao)
            else {
                ComandaDAO.insertVenda(data, (err, result) => {
                    if (err) erro(res, err, conexao)
                    lista.forEach((item, index) => {
                        ComandaDAO.insertVendaAux(item, result.insertId, (err, result) => {
                            if (index == lista.length - 1) {
                                endDAO(err, res, null, conexao)
                            }
                        })
                    })
                })
            }
        })
    }

    salvarComandaAberta = (req, res) => {
        var data = req.body;
        var lista = JSON.parse(req.body.pedidos);
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ComandaDAO.verificarComandaAberta(data.id_cartao, (err, result) => {
            if (err) erro(res, err, conexao)
            else if (result.length > 0) {
                ComandaDAO.selectDadosComandaAberta(result, (err, result) => {
                    if (err) erro(res, err, conexao)
                    else {
                        let id_venda = result[0].id_venda;
                        ComandaDAO.updateTotalVenda(data, id_venda, (err, result) => {
                            if (err) erro(res, err, conexao)
                            else {
                                ComandaDAO.deleteVendaAux(id_venda, (err, result) => {
                                    if (err) erro(res, err, conexao)
                                    else {
                                        lista.forEach((item, index) => {
                                            ComandaDAO.insertVendaAux(item, id_venda, (err, result) => {
                                                if (index == lista.length - 1) {
                                                    endDAO(err, res, null, conexao)
                                                }
                                            })
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }

    senhaSupervisor = (req, res) => {
        var data = req.body;
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ComandaDAO.senhaSupervisor(data.senha, (err, result) => {
            if (err) erro(res, err, conexao)
            else if (result[0].count > 0) res.json({ status: 200 })
            else res.json({ status: 100 })
        })
    }

    cancelarComanda = (req, res) => {
        let tag = req.body.tag;
        let produtos_cancelados = {};
        const conexao = app.utilities.connectionFactory();
        initDAO(req, conexao);
        ComandaDAO.selectComandaAberta(tag, (err, result) => {
            let id_venda = result[0].id_venda;
            if (err) erro(res, err, conexao)
            else {
                ComandaDAO.selectDadosComandaAberta(result, (err, result) => {
                    if (err) erro(res, err, conexao)
                    else {
                        result.forEach(resultado => {
                            produtos_cancelados[resultado.desc_produtoFinal] = resultado.qtde_vendaAux;
                        })
                        ComandaDAO.deleteVenda(id_venda, (err, result) => {
                            if (err) erro(res, err, conexao)
                            else {
                                ComandaDAO.deleteVendaAux(id_venda, (err, result) => {
                                    if (err) erro(res, err, conexao)
                                    else {
                                        ComandaDAO.liberarCartao(tag, (err, result) => endDAO(err, res, null, conexao))
                                        criar_log(req.body.nome, req.body.login, req.body.tag, JSON.stringify(produtos_cancelados))
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }

    function newFile(folderName, fileName) {
        fs.writeFileSync(__dirname.split('app')[0] + `logs_cancelamento/${folderName}/${fileName}.log`, '', '')
    }

    function criar_log(nome, login, tag, produtos) {
        let string_produtos = '';
        Object.entries(JSON.parse(produtos)).forEach(produto => {
            string_produtos += ` -> ${produto[0]}: ${produto[1]} unidade;\n`
        })
        let folderName = new Date().toLocaleDateString()
        let fileName = new Date().toLocaleTimeString().replace(/:/g, '-')
        var dir = `${__dirname.split('app')[0]}logs_cancelamento/${folderName}`;
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
        newFile(folderName, fileName)
        logger(folderName, fileName).info(` Usuário: ${nome}\n Login: ${login}\n Comanda cancelada: ${tag}\n Produtos:\n${string_produtos}`);
    }

    app.post('/fiecCantina/salvarComanda', salvarComanda);
    app.post('/fiecCantina/salvarComandaAberta', salvarComandaAberta);
    app.post('/fiecCantina/senhaSupervisor', senhaSupervisor);
    app.post('/fiecCantina/cancelarComanda', cancelarComanda);
}