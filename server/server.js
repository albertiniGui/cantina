const app = require('./config/express')();
const server = require('http').Server(app);
const port = 3000;
server.listen(port, () => console.log(`FIEC Cantina - ${process.env.NODE_ENV}\nPorta: ${port}`));