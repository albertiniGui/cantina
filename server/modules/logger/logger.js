try{
    var fs = require('fs');
    var path = require('path');
}catch(e){
    console.log("Import error on Logger -> " + e.toString());
}

var logger = {
    "SearchInDir" : (startPath, filter) => {
        if (!fs.existsSync(startPath)){
            console.log("Logger, searchInDir function -> Path not found ",startPath);
            return false;
        }
    
        var files = fs.readdirSync(startPath);
    
        for(var i = 0; i < files.length; i++){
            var filename = path.join(startPath, files[i]);
            var stat = fs.lstatSync(filename);
            if (stat.isDirectory()){
                SearchInDir(filename,filter); // recursiva para entrar no diretorio
            }
            else if (filename.indexOf(filter)>=0) {
                return true;
            };
        };
    }, 
    
    "save": (message) => {
        if (logger.SearchInDir("./logs", "log-"+new Date().toISOString().substr(0, 10)+".txt")){
            fs.appendFile("./logs/log-"+new Date().toISOString().substr(0, 10)+".txt", '\r\n'+message + '\r\n', (err) => {
                if (err) throw err;
                console.log('Log appended -> log-'+new Date().toISOString().substr(0, 10)+'.txt');
                return true;
            });
        }else{
            fs.writeFile("./logs/log-"+new Date().toISOString().substr(0, 10)+".txt", message+'\r\n', (err) => {
                if (err) throw err;
                return true;
            });
        }
    }
};

module.exports = logger;