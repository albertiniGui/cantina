import { Component, OnInit } from '@angular/core';
import {Router,RouterModule, Routes} from '@angular/router'


@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})  

export class LoadingComponent implements OnInit {

  constructor(private router : Router) { }
  
  redi2(){
    this.router.navigate(['main'])
    this.router.events.subscribe(e => console.log(e));
  }
 
  ngOnInit() {
    
  }

}
