import { Component, OnInit } from '@angular/core';
import { AppTableHead } from '../../app.component';
import { AppTableBody } from '../../app.component';
import { AppBottomBar } from '../../app.component';
import { Router } from '@angular/router';
import { ComprasService } from '../../services/compras.service';
import swal from 'sweetalert';

declare var $: any;

@Component({
  selector: 'app-compras',
  templateUrl: './compras.component.html',
  styleUrls: ['./compras.component.css']
})
export class ComprasComponent implements OnInit {

  constructor(private service: ComprasService, private router: Router) { }
  public resultados;
  public fornecedores;
  public parametro;
  public produtos = [];
  public produtosAdd = [];
  public produtosRem = [];
  public idFornecedor;
  public nomeFornecedor;
  public cadastro;
  public resultadosFinais;
  public produtosExistentes = [];
  public insumosExistentes = [];
  public produtosFiltrados;
  public insumosFiltrados;
  public filtroStatus;
  public valorProduto;
  public nomeProdutoEdit;
  public nomeProduto;
  public idProdTemp = 0;
  public idInsTemp = 0;
  public valorTotal = 0;
  filterResults = [];

  ngOnInit() {
    let tipo = JSON.parse(localStorage.getItem('session')).tipo;
    if (tipo == 2) {
      this.router.navigate(['main'])
    }
    this.service.selectCompras().subscribe(res => {
      this.resultados = res.json().result;
      this.resultadosFinais = res.json().result;
    });
    this.service.selectAll('fornecedor', 'razaoSocial').subscribe(res => {
      this.fornecedores = res.json().result;
    });
    this.service.selectAll('produto_final', 'desc_produtoFinal').subscribe(res => {
      this.produtosExistentes = res.json().result;
    });
    this.service.selectAll('insumo', 'desc_insumo').subscribe(res => {
      this.insumosExistentes = res.json().result;
    });
  }

  atualizar() {
    this.service.selectCompras().subscribe(res => {
      this.resultados = res.json().result;
      this.resultadosFinais = res.json().result;
    });
  }

  selectFornecedor(id) {
    for (let i = 0; i < this.fornecedores.length; i++) {
      if (this.fornecedores[i].id_fornecedor == id) {
        this.nomeFornecedor = this.fornecedores[i].razaoSocial;
      }
    }
  }

  dialog(event) {
    this.parametro = event;
    this.produtos = [];
    this.produtosAdd = [];
    this.produtosRem = [];
    this.valorProduto = null;
    this.nomeProduto = null;
    this.nomeProdutoEdit = null;
    this.filtroStatus = false;
    this.valorTotal = 0;
  }

  adicionarProduto() {
    document.getElementById('nomeProduto').focus();
    let nome = $("#nomeProduto").val();
    let qtde = $("#qtdeProduto").val();
    let valor = this.valorProduto;
    if (qtde > 0 && valor) {
      if (nome && qtde && valor != "" && this.idProdTemp != 0) {
        this.produtos.push({ 'nome': nome, 'qtde': qtde, 'valor': valor, 'idProd': this.idProdTemp });
        this.produtosAdd.push({ 'nome': nome, 'qtde': qtde, 'valor': valor, 'idProd': this.idProdTemp });
        $("#nomeProduto").val("");
        $("#qtdeProduto").val("");
        this.valorTotal += valor * qtde;
        this.valorProduto = null;
        this.idProdTemp = 0;
      } else if (nome && qtde && valor != "" && this.idInsTemp != 0) {
        this.produtos.push({ 'nome': nome, 'qtde': qtde, 'valor': valor, 'idIns': this.idInsTemp });
        this.produtosAdd.push({ 'nome': nome, 'qtde': qtde, 'valor': valor, 'idIns': this.idInsTemp });
        $("#nomeProduto").val("");
        $("#qtdeProduto").val("");
        this.valorTotal += valor * qtde;
        this.valorProduto = null;
        this.idInsTemp = 0;
      }
    } else {
      if (qtde <= 0) {
        swal('Atenção!', 'Número negativo.', 'warning')
        // this.confirmation('Numero Negativo');
      }
      else {
        swal('Atenção!', 'Valor negativo.', 'warning')
        // this.confirmation('Valor Negativo');
      }
    }
  }

  editProdutos(event, valor) {
    this.valorTotal = valor;
    let temp = event.split("-");
    event = (temp[1]);
    if (this.resultados[event].produtos != null) {
      this.produtos = JSON.parse(this.resultados[event].produtos);
    } else {
      this.produtos = []
    }
  }


  excluirProdutos(index) {
    this.valorTotal -= this.produtos[index].valor * this.produtos[index].qtde;
    if (this.produtos[index].idIns) {
      this.produtosRem.push({ 'nome': this.produtos[index].nome, 'qtde': this.produtos[index].qtde, 'valor': this.produtos[index].valor, 'idIns': this.produtos[index].idIns });
    } else if (this.produtos[index].idProd) {
      this.produtosRem.push({ 'nome': this.produtos[index].nome, 'qtde': this.produtos[index].qtde, 'valor': this.produtos[index].valor, 'idProd': this.produtos[index].idProd });
    }
    this.produtos.splice(index, 1);
  }


  confirmation(value) {
    this.cadastro = value;
  }

  cadastrarCompra() {
    if ($("#titulo").val() && $("#data").val() && $("#valor").val() && this.produtos.length >= 1) {
      var valorCompra;
      for (let i = 0; i < this.fornecedores.length; i++) {
        if (this.fornecedores[i].razaoSocial == (<HTMLInputElement>document.getElementById("fornecedor")).value) {
          this.idFornecedor = this.fornecedores[i].id_fornecedor;
        }
      }

      let tituloCompra = $("#titulo").val();
      let dataCompra = $("#data").val();

      try {
        let compraParc = $("#valor").val().split('R$');

        let verifyCompra = compraParc[1].split('')
        if (verifyCompra.length > 6) {
          let teste2 = compraParc[1].replace('.', '')
          valorCompra = teste2.replace(',', '.')
        } else {
          valorCompra = compraParc[1].split(',')[0] + '.' + compraParc[1].split(',')[1];
        }
      } catch (e) { }
      let somaProdutos = 0;
      this.produtos.forEach((produto) => {
        somaProdutos += produto.valor;
      });
      let produtos = JSON.stringify(this.produtos);
      let fornecedor = this.idFornecedor;

      let data = {
        'titulo': tituloCompra,
        'data': dataCompra,
        'valor': parseFloat(valorCompra),
        'produtos': produtos,
        'fornecedor': fornecedor
      };
      this.parametro = false;
      this.service.cadastrarCompra(data).subscribe(res => {
        if (res.json().status == 200) {
          swal('Sucesso!', 'Compra cadastrada com sucesso.', 'success')
          this.atualizar()
          // this.confirmation('Compra Cadastrada');
        } else {
          swal('Erro!', 'Erro ao cadastrar compra.', 'error')
          // this.confirmation('Erro Cadastro');
        }
      });
    } else {
      swal('Atenção!', 'Preencha os campos corretamente.', 'warning')
      // this.confirmation('Preencha Corretamente');
    }

  }

  editarCompra(id) {
    if ($("#titulo").val() && $("#data").val() && $("#valorEdit").val() && this.produtos.length >= 1) {
      var valorCompra;
      for (let i = 0; i < this.fornecedores.length; i++) {
        if (this.fornecedores[i].razaoSocial == (<HTMLInputElement>document.getElementById("fornecedor")).value) {
          this.idFornecedor = this.fornecedores[i].id_fornecedor;
        }
      }

      let tituloCompra = $("#titulo").val();
      let dataCompra = $("#data").val();

      try {
        let compraParc = String;
        compraParc = $("#valorEdit").val().split('R$');
        let verifyCompra = compraParc[1].split('')
        if (verifyCompra.length > 6) {
          let teste2 = compraParc[1].replace('.', '')
          valorCompra = teste2.replace(',', '.')
        } else {
          valorCompra = compraParc[1].split(',')[0] + '.' + compraParc[1].split(',')[1];
        }
      } catch (e) { }
      let somaProdutos = 0;
      this.produtos.forEach((produto) => {
        somaProdutos += produto.valor;
      });
      let produtos = JSON.stringify(this.produtos);
      let produtosAdd = JSON.stringify(this.produtosAdd);
      let produtosRem = JSON.stringify(this.produtosRem);
      let fornecedor = this.idFornecedor;
      let data = {
        'id': id,
        'titulo': tituloCompra,
        'data': dataCompra,
        'valor': parseFloat(valorCompra),
        'produtos': produtos,
        'produtosAdd': produtosAdd,
        'produtosRem': produtosRem,
        'fornecedor': fornecedor
      }
      this.parametro = false;
      this.service.editarCompra(data).subscribe(res => {
        if (res.json().status == 200) {
          swal('Sucesso!', 'Compra editada com sucesso.', 'success');
          this.atualizar();
        } else {
          swal('Erro!', 'Erro ao editar compra.', 'error');
        }
      });
    } else {
      swal('Atenção!', 'Preencha os campos corretamente.', 'warning');
    }

  }

  openInputSearch(name) {
    $("#inputSearch" + name).css('width', '');
    if ($("#inputSearch" + name).hasClass("openSearch")) {
      $("#inputSearch" + name).removeClass("openSearch");
      $("#inputSearch" + name).addClass("closeSearch");
    } else {
      $("#inputSearch" + name).removeClass("closeSearch");
      $("#inputSearch" + name).addClass("openSearch");
      $(`#${name}`).focus();
    }
  }

  bottomFilter(input) {
    var regexp = new RegExp((".*" + input.split("").join('.*') + ".*"), "i");
    this.resultadosFinais = this.resultados.filter((r) => {
      return regexp.test(r.desc_compra);
    });
  }

  filterProd(input) {
    if (input) {
      this.filtroStatus = true;
    } else {
      this.filtroStatus = false;
    }
    var regexp = new RegExp((".*" + input.split(" ").join('.*') + ".*"), "i");
    this.produtosFiltrados = this.produtosExistentes.filter((r) => {
      return regexp.test(r.desc_produtoFinal);
    });
    this.insumosFiltrados = this.insumosExistentes.filter((r) => {
      return regexp.test(r.desc_insumo);
    });
  }
  completarProduto(campo, index) {
    let produtoEscolhido = this.produtosFiltrados[index];
    $(`#${campo}`).val(produtoEscolhido.desc_produtoFinal + '(' + produtoEscolhido.unidadeVenda_produtoFinal + ')');
    this.idProdTemp = produtoEscolhido.id_produtoFinal
    this.filtroStatus = false;
  }
  completarInsumo(campo, index) {
    let insumoEscolhido = this.insumosFiltrados[index];
    $(`#${campo}`).val(insumoEscolhido.desc_insumo + '(' + insumoEscolhido.unidade_insumo + ')');
    this.idInsTemp = insumoEscolhido.id_insumo;
    this.filtroStatus = false;
  }

}
