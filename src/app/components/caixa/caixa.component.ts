import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { Http, HttpModule } from '@angular/http'
import { Router } from '@angular/router'

import { MainComponent } from '../main/main.component'

import { CaixaService } from '../../services/caixa.service';
import swal from 'sweetalert';

declare var $: any;

@Component({
  selector: 'app-caixa',
  templateUrl: './caixa.component.html',
  styleUrls: ['./caixa.component.css']
})


export class CaixaComponent implements OnInit, OnChanges {

  constructor(private service: CaixaService, private http: Http, private router: Router, private main: MainComponent) { }

  isCartaoFiec = false;
  loadingCaixa = false;
  isCartaoNull = false;
  confirmaCaixa = false;
  statusCaixa = false;

  payment = true;
  paymentCondition = true;

  dinheiro = 0;
  debito = 0;
  credito = 0;
  outro = 0;
  trocoInput = 0;
  valorRestante = 0;
  valorParcial = 0;
  paymentScreen = -1;

  comandasAbertas = [];
  comandas = [];
  tempCaixaData = [];

  estilo = {};

  textoCaixa = "";
  textoCredito = "";

  creditoRestante;
  senhaCartaoFiec;
  id_cartao;

  ngOnInit() {
    this.loadingCaixa = true;
    let tipo = JSON.parse(localStorage.getItem('session')).tipo;
    if (tipo == 4) {
      this.router.navigate(['main'])
    }
    setTimeout(() => {
      document.getElementById('id_cartao_focus').focus();
    }, 100)
  }

  focar() {
    document.getElementById('botaoVoltar').focus();
  }

  voltar() {
    this.loadingCaixa = true;
    this.id_cartao = null;
    this.comandasAbertas = [];
    this.isCartaoNull = false;
    setTimeout(() => {
      document.getElementById('id_cartao_focus').focus();
    }, 100)
  }

  liberarCaixa() {

    // CASO O INPUT FOR VAZIO, VIRÁ UNDEFINED
    // PORTANTO, DEVERÁ FAZER UM SELECT DAS COMANDAS EM ABERTO
    // E APRESENTA-LAS NO CAIXA VAZIO, SOMENTE PARA VISUALIZAÇÃO
    if (this.id_cartao == undefined) {
      this.loadingCaixa = false;
      this.isCartaoNull = true;
      this.service.comandasAbertas().subscribe(res => {
        if (res.json().status == 200) {
          this.comandasAbertas = res.json().result;
          setTimeout(() => {
            document.getElementById('botaoVoltar').focus();
          }, 100)
        }
        else {
          this.isCartaoNull = false;
          setTimeout(() => {
            document.getElementById('botaoVoltar').focus();
          }, 100)
        }
      })
    }
    else {
      this.divBotoes[0].divs.forEach(div => {
        div.model = '0'
      })
      this.loadingCaixa = false;
      if (this.paymentCondition == false) {
        this.payment = !this.payment;
        this.paymentCondition = !this.paymentCondition;
      }

      this.service.verificarComandaAbertaCaixa(this.id_cartao).subscribe(res => {
        let pedidos = res.json().result;
        this.comandas = [{
          fk_venda_cartao: res.json().result[0].fk_venda_cartao,
          id_comanda: res.json().result[0].id_venda,
          pedidos: pedidos,
          total: res.json().result[0].total_venda
        }]
        if (this.comandas.length > 0) {
          this.statusCaixa = true;
        } else {
          this.statusCaixa = false;
        }
        this.valorRestante = this.comandas[0].total
        this.valorParcial = this.valorRestante;
      })
    }
  }

  ngOnChanges() {
  }
  //ARRAY DE BOTÕES DE PAGAMENTO DO CAIXA
  divBotoes = [
    {
      botoes:
        [
          { nome: 'Dinheiro', icone: 'far fa-money-bill-alt' },
          { nome: 'Débito', icone: 'fas fa-credit-card' },
          { nome: 'Crédito', icone: 'far fa-credit-card' },
          { nome: 'Cartão Fiec', icone: 'far fa-id-card' }
        ],
      divs:
        [
          { span: 'Adicionar valor em Dinheiro', model: 'dinheiro', id: 'dinheiro' },
          { span: 'Adicionar valor em Débito', model: 'debito', id: 'debito' },
          { span: 'Adicionar valor em Crédito', model: 'credito', id: 'credito' },
          { span: 'Adicionar valor em Cartão Fiec', model: 'outro', id: 'outro' }
        ]
    }
  ]

  //VERIFICA O PAGAMENTO E ATUALIZA DIV
  altPayment() {
    this.senhaCartaoFiec = null;
    $("#finishPayment").removeClass('finishPayment')
    $("#finishPayment").addClass('returnToPayment')
    setTimeout(() => {
      this.payment = true;
    }, 100)
  }

  //ABRIR TELA DE INSERÇÃO DE PAGAMENTO
  openPaymentScreen(id) {
    this.paymentScreen = id;
    let ids = {
      '0': 'dinheiro',
      '1': 'debito',
      '2': 'credito',
      '3': 'outro',
    }
    setTimeout(() => {
      document.getElementById(`${ids[id]}`).focus();
    }, 100);
  }

  //FECHAR TELA DE PAGAMENTO
  closePaymentScreen(id) {
    $(`#paymentScreen-${id}`).removeClass('showPaymentScreen')
    $(`#paymentScreen-${id}`).addClass('hidePaymentScreen')
    setTimeout(() => {
      this.paymentScreen = -1;
    }, 100)
  }

  //FUNÇÃO DE SOMA PARA GETVALUE()
  sum(a, b) {
    return a + b;
  }
  //ADICIONA VALOR PAGO
  totalPago = 0;
  valorTotal = 0;
  getValue(model, nome, id) {
    switch (nome) {
      case 'dinheiro':
        if (model = model || 0) {
          if (typeof model === 'number') {
            this.dinheiro = model
          } else {
            this.dinheiro = 0;
          }
        } else {
          this.dinheiro = 0;
        }
        break;
      case 'debito':
        if (model = model || 0) {
          if (typeof model === 'number') {
            this.debito = model
          } else {
            this.debito = 0;
          }
        } else {
          this.debito = 0;
        }
        break;
      case 'credito':
        if (model = model || 0) {
          if (typeof model === 'number') {
            this.credito = model
          } else {
            this.credito = 0;
          }
        } else {
          this.credito = 0;
        }
        break;
      case 'outro':
        if (model = model || 0) {
          if (typeof model === 'number') {
            this.outro = model
          } else {
            this.outro = 0;
          }
        } else {
          this.outro = 0;
        }
        break;
    }
    let valorRecebido = {
      dinheiro: this.dinheiro,
      debito: this.debito,
      credito: this.credito,
      outro: this.outro
    }
    this.valorTotal = Object.values(valorRecebido).reduce(this.sum, 0)
    if (this.valorTotal >= this.valorRestante) {
      $("#checkPayment").prop('disabled', false)
      this.paymentCondition = false;
    } else {
      this.paymentCondition = true;
      this.valorParcial = this.valorRestante - this.valorTotal;
      $("#checkPayment").prop('disabled', true)
    }
    $(`#paymentScreen-${id}`).removeClass('showPaymentScreen')
    $(`#paymentScreen-${id}`).addClass('hidePaymentScreen')
    setTimeout(() => {
      this.paymentScreen = -1;
    }, 100)
  }

  verificaUsoCartao() {
    if (this.outro > 0) {
      this.isCartaoFiec = true;
    }
    else {
      this.isCartaoFiec = false;
    }
    this.comandas.forEach(comanda => {
      comanda.pedidos.forEach(pedido => {
        if (pedido.tipo_produto == 'Credit') {
          this.isCartaoFiec = true;
        }
      })
    })

    if (this.isCartaoFiec) {
      setTimeout(() => {
        document.getElementById('senha_cartao').focus();
      }, 100);
    }

    return this.isCartaoFiec;
  }

  //VERIFICAÇÃO DE VALOR PAGO
  dinheiroFinal = 0;
  calcTroco() {
    this.verificaUsoCartao();

    this.dinheiroFinal = 0;
    let troco;
    let somaCartoes;
    let totalCompra = this.comandas[0].total;
    let totalParcial;

    if (this.valorTotal >= totalCompra) {
      somaCartoes = this.credito + this.debito + this.outro;
      totalParcial = totalCompra - somaCartoes;
      if (somaCartoes > totalCompra) {
        swal('Atenção!', 'Valor da compra foi ultrapassado.', 'warning')
      } else {
        this.dinheiroFinal = totalParcial;
        troco = this.valorTotal - totalCompra;
        this.trocoInput = troco;
        this.payment = false;
      }
    }
  }
  // ---------------------------------

  //FUNÇÃO DE FECHAR COMANDA
  fechaComanda() {
    this.senhaCartaoFiec = null;

    let dataCartao = {
      id_cartao: this.id_cartao,
      tag: this.id_cartao,
      credito: this.outro
    }

    let total = this.comandas[0].total

    let data = {
      //DADOS INSERT EM VENDA
      id_func: JSON.parse(localStorage.getItem('session')).usu_id,
      id_comprou: this.id_cartao,    //  ID REFERENTE AO SELECT DO CARTÃO
      id_comanda: this.comandas[0].id_comanda,
      total_venda: total,
      pag_dinheiro: this.dinheiroFinal,
      pag_debito: this.debito,
      pag_credito: this.credito,
      pag_cardFiec: this.outro,
      //DADOS INSERT EM VENDA AUXILIAR
      lista_pedidos: JSON.stringify(this.comandas[0].pedidos)
    }

    this.service.checarCredito(this.id_cartao).subscribe(res => {
      if (res.json().result[0].car_credito >= this.outro) {
        this.service.pagarConta(data).subscribe(res => {
          if (res.json().status == 200) {
            this.service.updateCredito(dataCartao).subscribe(res => {
              if (res.json().status == 200) {
                JSON.parse(data.lista_pedidos).forEach(item => {
                  if (item.tipo_produto == 'Credit') {
                    let credito_adicionar = {
                      tag: this.id_cartao,
                      credito: item.qtde_vendaAux
                    }
                    this.service.insertCredito(credito_adicionar).subscribe()
                  }
                })
                swal('Sucesso!', "Compra efetuada com sucesso.", "success")
                this.goMain();
                this.isCartaoFiec = false;
                this.senhaCartaoFiec = null;
                localStorage.removeItem('comanda')
                this.service.atualizaSangria({ totalVenda: this.dinheiro - this.trocoInput }).subscribe()
              }
              else {
                swal('Erro!', "Erro ao realizar a compra.", "error")
              }
            })
          } else {
            swal('Erro!', "Erro ao realizar a compra.", "error")
          }
        })
      }
      else {
        swal('Atenção!', "Crédito FIEC insuficiente.", "warning")
        this.liberarCaixa();
      }

    })
  }

  // FUNÇÃO goMain() NA VDD VAI PARA A TELA DA COMANDA
  goMain() {
    this.statusCaixa = false;
    this.confirmaCaixa = false;
    this.loadingCaixa = true;
    this.id_cartao = null;
    setTimeout(() => {
      document.getElementById('id_cartao_focus').focus();
    }, 100)
  }

  verificarSenha() {
    let credito_compra = 0;

    let data = {
      senha: this.senhaCartaoFiec,
      tag: this.id_cartao
    }

    this.service.checarSenha(data).subscribe(res => {
      if (res.json().status == 200) {
        this.senhaCartaoFiec = null;
        this.isCartaoFiec = false;
        this.service.checarCredito(this.id_cartao).subscribe(res => {
          this.comandas[0].pedidos.forEach(pedido => {
            if (pedido.desc_produtoFinal == "Crédito") {
              credito_compra += pedido.qtde_vendaAux
            }
          });
          this.creditoRestante = res.json().result[0].car_credito - this.outro + credito_compra;
        })
      }
      else if (res.json().status == 100) {
        swal('Atenção!', "Senha incorreta.", "warning")
          .then(() => {
            setTimeout(() => {
              document.getElementById('senha_cartao').focus();
            }, 100)
          })
        this.senhaCartaoFiec = null;
      }
      else {
        swal('Erro!', "Erro de conexão.", "error")
      }
    })
  }
}
