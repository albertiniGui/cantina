import { Component, OnInit } from '@angular/core';
import { HttpModule, Http } from '@angular/http'
import { Properties } from '../../class/properties';
import { AppTableHead } from '../../app.component';
import { AppTableBody } from '../../app.component';
import { RelatorioService } from '../../services/relatorio.service';
import swal from 'sweetalert';

declare var $: any;

@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorio.component.html',
  styleUrls: ['./relatorio.component.css']
})
export class RelatorioComponent implements OnInit {

  constructor(private service: RelatorioService, private http: Http) { }

  public resultados;
  public produtos = ['Bebidas Quentes', 'Bebidas Frias', 'Bebidas Industrializadas', 'Lanche Natural', 'Salgados Comprados', 'Salgados Produzidos Assados', 'Salgados Produzidos Fritos', 'Salgados Congelados', 'Doces Comprados', 'Doces Produzidos', 'Doces Congelados', 'Prato do Dia', 'Prato Congelado'];
  public unidades = ['Kg', 'Gr', 'Un', 'L', 'Ml'];
  public produtosCompra = [];
  public ingredientes = [];
  public filtroComposto = false;
  public table;           //= 'Compra';
  public filtroActive = 'filtroCompra';
  public filtrosRapidos = true;
  public viewParam;
  public vendaPdf = false;
  public compraPdf = false;
  public produtoValor;
  public produtoValorPromo;
  public custoReceita;
  public totalVenda;
  public valorTotalCompra;
  public nomeFunc;
  public codFunc;
  public hoje = false;
  public fiecI = false;
  public fiecII = false;
  public privilegio = localStorage.getItem('tipo')

  ngOnInit() {
    //$("#compositeFilter").css('top','96.5%'); 
    let tipo = localStorage.getItem('tipo');
    if (tipo != '1') {
      this.service.verificarPolo().subscribe(res => {
        if (res.json().result == 1) {
          setTimeout(() => {
            document.getElementById('fiec2').style.display = 'none';
          }, 100)
        }
        else {
          setTimeout(() => {
            document.getElementById('fiec1').style.display = 'none';
          }, 100)
        }
      })
    }
  }

  openFilter() {
    $("#compositeFilter").css('top', '');
    if ($("#compositeFilter").hasClass("turnUp")) {
      $("#arrow").removeClass('btAnim');
      $("#arrow").addClass('btAnim_hide');
      $("#compositeFilter").removeClass("turnUp");
      $("#compositeFilter").addClass("turnDown");
    } else {
      $("#arrow").addClass('btAnim');
      $("#arrow").removeClass('btAnim_hide');
      $("#compositeFilter").removeClass("turnDown");
      $("#compositeFilter").addClass("turnUp");
    }
  }

  //ABRE A TABELA DO RESPECTIVO FILTRO DESEJADO
  setTable(value) {
    this.table = value;
    this.filtroActive = 'filtro' + value;
    this.search();
  }

  viewCompras(event, index) {
    this.viewParam = event;
    if (this.resultados[index].produtos != null) {
      this.produtosCompra = JSON.parse(this.resultados[index].produtos);
    } else {
      this.produtosCompra = []
    }
  }

  viewReceita(event, index) {
    this.viewParam = event;
    if (this.resultados[index].ingredientes && this.resultados[index].ingredientes != null) {
      this.ingredientes = JSON.parse(this.resultados[index].ingredientes);
    } else {
      this.ingredientes = []
    }
  }

  view(event) {
    this.viewParam = event;
  }
  closeView() {
    this.viewParam = 'Closed';
  }

  filterVendaHojeFI() {
    this.filtrosRapidos = false;
    this.table = 'Venda';
    var d = new Date;
    this.fiecI = true;
    this.hoje = true;
    this.search();
  }

  filterVendaHojeFII() {
    this.filtrosRapidos = false;
    this.table = 'Venda';
    this.fiecII = true;
    this.hoje = true;
    this.search();
  }

  filterProducaoHoje() {
    this.filtrosRapidos = false;
    this.table = 'Producao';
    setTimeout(() => {
      this.hoje = true;
      this.search();
    }, 100);
  }

  openCompositeFilter(table) {
    if (this.filtrosRapidos == true) {
      this.table = table;
      this.search();
      this.filtroActive = 'filtro' + table;
      this.filtrosRapidos = false;
    } else {
      this.filtrosRapidos = true;
    }
  }
  //  PEGA OS ELEMENTOS NA TABELA PELO ID E POSSIBILITA GERAR PDF COM APENAS OS QUE ESTÃO A MOSTRA
  //  SALVANDO OS DADOS RESPECTIVOS EM UM JSON E MANDANDO PRO service
  search() {

    switch (this.table) {
      case 'Compra':
        this.totalVenda = undefined;
        this.custoReceita = undefined;
        this.produtoValor = undefined;
        this.produtoValorPromo = undefined;
        let dataCompra = {};
        dataCompra['privilegio'] = this.privilegio;
        if ($("#tituloCompra").val()) {
          dataCompra['titulo'] = $("#tituloCompra").val();
        }
        if (this.valorTotalCompra) {
          dataCompra['valor'] = this.valorTotalCompra;
        }
        if ($("#dataIniCompra").val() || $("#dataFimCompra").val()) {
          let dataIni = $("#dataIniCompra").val();
          let dataFim = $("#dataFimCompra").val();
          if ($("#dataFimCompra").val() && $("#dataIniCompra").val()) {
            dataCompra['dataIni'] = dataIni;
            dataCompra['dataFim'] = dataFim;
          } else if ($("#dataIniCompra").val() && $("#dataFimCompra").val() == "") {
            dataCompra['dataIni'] = dataIni;
            dataCompra['dataFim'] = 'sysdate()';
          } else {
            dataCompra['dataIni'] = 'sysdate()';
            dataCompra['dataFim'] = dataFim;
          }
        }
        this.service.relatorioCompras(dataCompra).subscribe(res => {
          this.resultados = res.json().result;
        });
        break;
      case 'Insumo':
        this.valorTotalCompra = undefined;
        this.totalVenda = undefined;
        this.custoReceita = undefined;
        this.produtoValor = undefined;
        this.produtoValorPromo = undefined;
        let dataInsumo = {};
        dataInsumo['privilegio'] = this.privilegio;
        if ($("#tituloInsumo").val()) {
          dataInsumo['titulo'] = $("#tituloInsumo").val();
        }
        if ($("#qtdeEstoque").val()) {
          dataInsumo['estoque'] = $("#qtdeEstoque").val();
        }
        if ((<HTMLInputElement>document.getElementById("unidade")) && (<HTMLInputElement>document.getElementById("unidade")).value && (<HTMLInputElement>document.getElementById("unidade")).value != 'Unidades') {
          dataInsumo['unidade'] = (<HTMLInputElement>document.getElementById("unidade")).value;
        }
        this.service.relatorioInsumos(dataInsumo).subscribe(res => {
          console.log(res.json().result)
          this.resultados = res.json().result;
        });
        break;
      case 'Producao':
        this.valorTotalCompra = undefined;
        this.totalVenda = undefined;
        this.custoReceita = undefined;
        this.produtoValor = undefined;
        this.produtoValorPromo = undefined;
        let dataProducao = {};
        dataProducao['privilegio'] = this.privilegio;
        if ($("#tituloProducao").val()) {
          dataProducao['titulo'] = $("#tituloProducao").val();
        }
        if ($("#dataIniProducao").val() || $("#dataFimProducao").val()) {
          let dataIni = $("#dataIniProducao").val();
          let dataFim = $("#dataFimProducao").val();
          if ($("#dataFimProducao").val() && $("#dataIniProducao").val()) {
            dataProducao['dataIni'] = dataIni;
            dataProducao['dataFim'] = dataFim;
          } else if ($("#dataIniProducao").val() && $("#dataFimProducao").val() == "") {
            dataProducao['dataIni'] = dataIni;
            dataProducao['dataFim'] = 'sysdate()';
          } else {
            dataProducao['dataIni'] = 'sysdate()';
            dataProducao['dataFim'] = dataFim;
          }
        }

        if ((<HTMLInputElement>document.getElementById("status")) && (<HTMLInputElement>document.getElementById("status")).value && (<HTMLInputElement>document.getElementById("status")).value != 'Status') {
          dataProducao['status'] = (<HTMLInputElement>document.getElementById("status")).value;
        }

        if ((<HTMLInputElement>document.getElementById("tipos")) && (<HTMLInputElement>document.getElementById("tipos")).value && (<HTMLInputElement>document.getElementById("tipos")).value != 'Tipo de Produto') {
          dataProducao['tipos'] = (<HTMLInputElement>document.getElementById("tipos")).value;
        }
        if (this.hoje) {
          dataProducao['hoje'] = true;
        }
        this.service.relatorioProducoes(dataProducao).subscribe(res => {
          this.resultados = res.json().result;
          this.hoje = false;
        });
        break;
      case 'Produto':
        this.valorTotalCompra = undefined;
        this.totalVenda = undefined;
        this.custoReceita = undefined;
        let dataProduto = {};
        dataProduto['privilegio'] = this.privilegio;

        if ($("#tituloProduto").val()) {
          dataProduto['titulo'] = $("#tituloProduto").val();
        }

        if (this.produtoValor) {
          dataProduto['valor'] = this.produtoValor;
        }

        if (this.produtoValorPromo) {
          dataProduto['valorPromo'] = this.produtoValorPromo;
        }

        if ((<HTMLInputElement>document.getElementById("status")) && (<HTMLInputElement>document.getElementById("status")).value && (<HTMLInputElement>document.getElementById("status")).value != 'Status') {
          dataProduto['status'] = (<HTMLInputElement>document.getElementById("status")).value;
        }

        if ((<HTMLInputElement>document.getElementById("tipos")) && (<HTMLInputElement>document.getElementById("tipos")).value && (<HTMLInputElement>document.getElementById("tipos")).value != 'Tipo de Produto') {
          dataProduto['tipos'] = (<HTMLInputElement>document.getElementById("tipos")).value;
        }

        this.service.relatorioProdutos(dataProduto).subscribe(res => {
          this.resultados = res.json().result;
        });
        break;
      case 'Receita':
        this.valorTotalCompra = undefined;
        this.totalVenda = undefined;
        this.produtoValor = undefined;
        this.produtoValorPromo = undefined;
        let dataReceita = {};

        if ($("#tituloReceita").val()) {
          dataReceita['titulo'] = $("#tituloReceita").val();
        }
        if ($("#rendimento").val()) {
          dataReceita['rendimento'] = $("#rendimento").val();
        }
        if ($("#tempo").val()) {
          dataReceita['tempo'] = $("#tempo").val();
        } if (this.custoReceita) {
          dataReceita['custo'] = this.custoReceita;
        }
        this.service.relatorioReceitas(dataReceita).subscribe(res => {
          this.resultados = res.json().result;
        });
        break;
      case 'Venda':
        console.log("chegou")
        this.valorTotalCompra = undefined;
        this.custoReceita = undefined;
        this.produtoValor = undefined;
        this.produtoValorPromo = undefined;
        let dataVenda = {};

        dataVenda['privilegio'] = localStorage.getItem("tipo");

        if (this.totalVenda) {
          dataVenda['total'] = this.totalVenda;
        }
        if ($("#comprador").val()) {
          dataVenda['comprador'] = $("#comprador").val();
        }
        if ($("#funcionario").val()) {
          dataVenda['funcionario'] = $("#funcionario").val();
        }

        if ((<HTMLInputElement>document.getElementById("dinheiro")) && (<HTMLInputElement>document.getElementById("dinheiro")).checked) {
          dataVenda['dinheiro'] = true;
        }

        if ((<HTMLInputElement>document.getElementById("dinheiro")) && (<HTMLInputElement>document.getElementById("debito")).checked) {
          dataVenda['debito'] = true;
        }

        if ((<HTMLInputElement>document.getElementById("dinheiro")) && (<HTMLInputElement>document.getElementById("credito")).checked) {
          dataVenda['credito'] = true;
        }

        if ((<HTMLInputElement>document.getElementById("dinheiro")) && (<HTMLInputElement>document.getElementById("fiec")).checked) {
          dataVenda['fiec'] = true;
        }
        if ((<HTMLInputElement>document.getElementById("manha")) && (<HTMLInputElement>document.getElementById("manha")).checked) {
          dataVenda['manha'] = true;
        }
        if ((<HTMLInputElement>document.getElementById("tarde")) && (<HTMLInputElement>document.getElementById("tarde")).checked) {
          dataVenda['tarde'] = true;
        }
        if ((<HTMLInputElement>document.getElementById("noite")) && (<HTMLInputElement>document.getElementById("noite")).checked) {
          dataVenda['noite'] = true;
        }
        if (this.fiecI) {
          dataVenda['FIECI'] = true;
        }
        if (this.fiecII) {
          dataVenda['FIECII'] = true;
        }
        if (this.hoje) {
          dataVenda['hoje'] = true;
        }

        if ($("#dataIniVenda").val() || $("#dataFimVenda").val()) {
          let dataIni = $("#dataIniVenda").val();
          let dataFim = $("#dataFimVenda").val();
          if ($("#dataFimVenda").val() && $("#dataIniVenda").val()) {
            dataVenda['dataIni'] = dataIni;
            dataVenda['dataFim'] = dataFim;
          } else if ($("#dataIniVenda").val() && $("#dataFimVenda").val() == "") {
            dataVenda['dataIni'] = dataIni;
            dataVenda['dataFim'] = 'sysdate()';
          } else {
            dataVenda['dataIni'] = 'sysdate()';
            dataVenda['dataFim'] = dataFim;
          }
        }
        this.service.relatorioVendas(dataVenda).subscribe(res => {
          this.resultados = res.json().result;
          this.hoje = false;
          this.fiecI = false;
          this.fiecII = false;
        })
        break;
      case 'Freezer':
        this.valorTotalCompra = undefined;
        this.custoReceita = undefined;
        this.produtoValor = undefined;
        this.produtoValorPromo = undefined;
        this.totalVenda = undefined;
        let dataFreezer = {};
        dataFreezer['privilegio'] = localStorage.getItem('tipo');

        if ($("#tituloFreezer").val()) {
          dataFreezer['titulo'] = $("#tituloFreezer").val();
        }
        if ($("#tempMin").val()) {
          dataFreezer['tempMin'] = $("#tempMin").val();
        }
        if ($("#tempMax").val()) {
          dataFreezer['tempMax'] = $("#tempMax").val();
        }
        if ($("#dataIniFreezer").val() || $("#dataFimFreezer").val()) {
          let dataIni = $("#dataIniFreezer").val();
          let dataFim = $("#dataFimFreezer").val();
          if ($("#dataFimFreezer").val() && $("#dataIniFreezer").val()) {
            dataFreezer['dataIni'] = dataIni;
            dataFreezer['dataFim'] = dataFim;
          } else if ($("#dataIniFreezer").val() && $("#dataFimFreezer").val() == "") {
            dataFreezer['dataIni'] = dataIni;
            dataFreezer['dataFim'] = 'sysdate()';
          } else {
            dataFreezer['dataIni'] = 'sysdate()';
            dataFreezer['dataFim'] = dataFim;
          }
        }
        this.service.relatorioRegistros(dataFreezer).subscribe(res => {
          this.resultados = res.json().result;
        });
        break;
      case 'Usuario':
        this.nomeFunc = undefined;
        this.codFunc = undefined;
        let dataUsu = {}

        if ($("#nomeFunc").val()) {
          dataUsu['nomeFunc'] = $("#nomeFunc").val();
        }
        if ($("#codFunc").val()) {
          dataUsu['codFunc'] = $("#codFunc").val();
        }
        var d = new Date();

        if ((<HTMLInputElement>document.getElementById("hoje")) && (<HTMLInputElement>document.getElementById("hoje")).checked) {
          dataUsu['hoje'] = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate()
        }
        if ((<HTMLInputElement>document.getElementById("mes")) && (<HTMLInputElement>document.getElementById("mes")).checked) {
          dataUsu['mes'] = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate()
        }

        this.service.relatorioUsuarios(dataUsu).subscribe(res => {
          try {
            if (res.json().result[0].usu_id == null) {
              this.resultados = []
            } else {
              this.resultados = res.json().result;
            }
          } catch (error) {
            this.resultados = []
          }
        });

        break;
    }
  }

  pdfStatus = false;
  pdfText;
  receitas = []
  erroIngrediente;

  pdfTeste(name, ident, quant) {
    this.erroIngrediente = false;
    swal({
      title: 'Aguarde.',
      text: 'Gerando PDF...',
      icon: 'info',
      buttons: [false, false],
    });
    this.receitas = [];
    var d = new Date();
    var mes = d.getMonth() + 1
    var time = d.getDate() + "-" + mes;
    if (this.resultados.length == 0) {
      swal('Atenção!', 'Sem resultado filtre novamente os dados.', 'warning')
    } else {

      switch (name) {
        case 'receita':
          this.resultados.forEach((r) => {
            this.receitas.push({
              id_receita: r.id_receita,
              nome_receita: `Receita de ${r.desc_receita}`,
              ingredientes: JSON.parse(r.ingredientes),
              modo_preparo: r.modoPreparo_receita,
              rendimento: r.rendimento,
              tempoPreparo: r.tempoPreparo_receita
            })
          });
          let receita = {
            htmlFile: name,
            fileName: name + "_" + time,
            valores: this.receitas
          }
          this.service.gerarPdf(receita).subscribe((res) => {
            if (res.json().status == 200) {
              window.open(`${Properties.URL}/abrirPdf?fileName=${receita.fileName}`, '_blank');
              swal.close();
            } else {
              swal('Erro!', 'Erro ao gerar relatório.', 'error')
            }
          })
          break;
        case 'CompraDetalhado':
          let compra = this.resultados;
          for (var i = 0; i < this.resultados.length; i++) {
            let prods = JSON.parse(this.resultados[i].produtos)
            compra[i].prods = prods;
          }

          if (localStorage.getItem('tipo') == '1') {
            compra[0].polo = '1 & 2'
          }
          let compras = {
            htmlFile: name,
            fileName: name + "_" + time,
            valores: compra
          }
          this.service.gerarPdf(compras).subscribe(res => {
            if (res.json().status == 200) {
              window.open(`${Properties.URL}/abrirPdf?fileName=${compras.fileName}`, '_blank');
              swal.close();
            } else {
              swal('Erro!', 'Erro ao gerar o relatório.', 'error')
            }
          })
          break;
        case 'receitaAula':
          this.resultados.forEach((r) => {
            if (r.id_receita == ident) {
              let newReceita = []
              if (r.ingredientes == null || r.ingredientes == '[]') {
                this.erroIngrediente = true;
              } else {
                JSON.parse(r.ingredientes).forEach((rec) => {
                  newReceita.push({
                    nome: rec.nome,
                    qtde: rec.qtde * quant,
                    unidade: rec.unidade

                  })
                })
              }
              this.receitas.push({
                id_receita: r.id_receita,
                nome_receita: `Receita de ${r.desc_receita}`,
                ingredientes: newReceita,
                modo_preparo: r.modoPreparo_receita,
                rendimento: r.rendimento * quant,
                tempoPreparo: r.tempoPreparo_receita
              })
            }
          })
          let receitaAula = {
            htmlFile: 'receita',
            fileName: "receita" + "_" + time,
            valores: this.receitas
          }
          if (this.erroIngrediente == false) {
            this.service.gerarPdf(receitaAula).subscribe(res => {
              if (res.json().status == 200) {
                window.open(`${Properties.URL}/abrirPdf?fileName=${receitaAula.fileName}`, '_blank');
                swal.close();
              } else {
                swal('Erro!', 'Erro ao gerar relatório.', 'error')
              }
            })
          } else {
            swal('Atenção!', 'Adicione os ingredientes à receita.', 'warning')
          }
          break;

        case 'VendaProdutos':
          if (localStorage.getItem('tipo') == '1') {
            this.resultados[0].polo = '1 & 2'
          }
          let vendas = [];
          this.resultados.forEach(resultado => {
            vendas.push(resultado.id_venda)
          })
          let dados = {
            id_vendas: vendas
          }
          this.service.somarProdutos(dados).subscribe(res => {
            if (res.json().status == 200) {
              let valor_total = 0;
              res.json().result.forEach(resultado => valor_total += resultado.total)
              let data = {
                htmlFile: 'VendaProdutos',
                fileName: 'VendaProdutos' + "_" + time,
                valores: res.json().result,
                valor_total : valor_total,
                polo: this.resultados[0].polo
              }
              this.service.gerarPdf(data).subscribe(res => {
                if (res.json().status == 200) {
                  window.open(`${Properties.URL}/abrirPdf?fileName=${data.fileName}`, '_blank');
                  swal.close();
                } else {
                  swal('Erro!', 'Erro ao gerar relatório.', 'error')
                }
              })
            }
            else {
              swal('Erro!', 'Erro ao gerar relatório.', 'error')
            }
          })
          break;

        default:
          if (localStorage.getItem('tipo') == '1') {
            this.resultados[0].polo = '1 & 2'
          }
          let data = {
            htmlFile: name,
            fileName: name + "_" + time,
            valores: this.resultados
          }
          this.service.gerarPdf(data).subscribe((res) => {
            if (res.json().status == 200) {
              window.open(`${Properties.URL}/abrirPdf?fileName=${data.fileName}`, '_blank');
              swal.close();
            } else {
              swal('Erro!', 'Erro ao gerar relatório.', 'error')
            }
          })
          break;

      }
    }
  }

  escolhaPdf(titulo, texto, botoes) {
    let buttons = {};
    botoes.forEach(botao => {
      buttons[botao] = {
        text: botao,
        value: botao,
        className: 'swalButtons',
      }
    })
    swal({
      title: titulo,
      icon: 'info',
      text: texto,
      buttons: buttons,
    }).then(value => {
      if (value) this.pdfTeste(titulo + value, null, null)
    })
  }

}
