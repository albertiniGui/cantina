import swal from 'sweetalert';
import { Http, HttpModule } from '@angular/http';
import { Router, RouterModule } from '@angular/router';
import { MainComponent } from '../main/main.component';
import { ComandaService } from '../../services/comanda.service';
import { Component, OnInit, Output, Input, OnChanges, EventEmitter } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-comanda',
  templateUrl: './comanda.component.html',
  styleUrls: ['./comanda.component.css']
})

export class ComandaComponent implements OnInit {

  @Input() quantidade: number;

  autocomplete = false;
  erroComplete = false;
  loadingComanda = false;
  isComandaAberta = false;
  confirmaComanda = false;
  isPesquisarCodigo = false;
  isCancelarComanda = false;

  produtos = [];
  listaPedidos = [];

  iconStyle = {};

  icon = "";
  autComp = "";
  textoComanda = "";
  tip_descricao = "";
  senha_supervisor = "";
  titleComanda = "Comanda"

  id_cartao;
  filterProds;
  totalCompra = 0;

  constructor(private service: ComandaService, private http: Http, private router: Router, private main: MainComponent) { }

  tempComanda = []

  getProds() {
    this.service.selectProdutos().subscribe(res => {
      res.json().result.forEach(resultado => {
        this.produtos.push(resultado)
      })
    })
  }

  ngOnInit() {
    let tipo = JSON.parse(localStorage.getItem('session')).tipo;
    if (tipo == 4) {
      this.router.navigate(['main'])
    }
    setTimeout(() => {
      document.getElementById('id_cartao_focus').focus();
    }, 100)
    this.esperandoCartao();
    this.filterProds = this.produtos;
    this.getProds();
  }

  focus() {
    $('#input_codigoBarras').focus();
  }

  checarLength(){
    if(this.id_cartao.length > 10){
      var temp = this.id_cartao.toString().split('');
      temp.pop();
      this.id_cartao = Number(temp.concat());
    }
  }

  // METODO PARA ESPERAR ENTRADA DE CARTAO
  // PARA PERMITIR VENDA
  esperandoCartao() {
    this.loadingComanda = true;
  }

  checarCartoesConvidados() {
    let cartoes = [];
    let convidados;
    // VINCULAR COMANDA DE CONVIDADO COM A COMANDA DO JN.
    if (localStorage.getItem('convidados') == null) {
      this.service.selectCartoesConvidados().subscribe(res => {
        res.json().result.forEach(resultado => {
          cartoes.push(resultado.car_tag)
        })
        localStorage.setItem('convidados', JSON.stringify(cartoes))
        convidados = JSON.parse(localStorage.getItem('convidados'));
        this.liberarComanda(convidados);
      })
    }
    else {
      convidados = JSON.parse(localStorage.getItem('convidados'));
      this.liberarComanda(convidados);
    }
  }

  liberarComanda(convidados) {
    convidados.forEach(convidado => {
      if (convidado == this.id_cartao) {
        // TAG DO CARTAO "JOAO NETO"
        this.id_cartao = '1111111111';
      }
    })

    this.isCancelarComanda = false;
    if (this.id_cartao != undefined && this.id_cartao != null) {
      this.service.verificarExistenciaCartao(this.id_cartao).subscribe(res => {
        if (res.json().status == 200) {
          this.service.verificarTipoCartao(this.id_cartao).subscribe(res => {
            if (res.json().status == 200) {
              this.tip_descricao = res.json().result[0].tip_descricao;
            }
          })

          this.loadingComanda = false;
          this.isComandaAberta = false;
          this.service.verificarComandaAberta(this.id_cartao).subscribe(res => {
            if (res.json().status == 200) {
              this.totalCompra = res.json().result[0].total_venda;
              res.json().result.forEach(pedido => {
                this.listaPedidos.push(pedido)
                setTimeout(() => {
                  $(`#imgProd-${pedido.id_produtoFinal}`).removeClass('imgProd');
                  $(`#imgProd-${pedido.id_produtoFinal}`).css('cursor', 'not-allowed');
                }, 100)
                pedido.quantidade = pedido.qtde_vendaAux
              })
              this.isComandaAberta = true;
              this.loadingComanda = false;
            }
            else {
              this.totalCompra = 0;
              this.loadingComanda = false;
            }
          })
        }
        else {
          // PARA TESTES
          swal('Cartão Inexistente', `Este cartão não está cadastrado!`, 'warning')
            .then((value) => document.getElementById('id_cartao_focus').focus())
          this.id_cartao = null;
        }
      })

    }
  }

  //FUNÇÕES DO AUTOCOMPLETE
  filtroProd(model) {
    var regexp = new RegExp((".*" + model.split(" ").join('.*') + ".*"), "i");
    this.filterProds = this.produtos.filter((r) => {
      return regexp.test(r.desc_produtoFinal);
    })
    if (this.filterProds.length <= 0) {
      this.autocomplete = true;
    } else {
      this.autocomplete = false;
    }
  }

  // FUNÇÃO DE ADICIONAR ITEM Á LISTA DE PEDIDOS
  addItem(ind) {
    let valorInicial = 0;
    if (document.getElementById(`imgProd-${ind}`).className == 'imgProd imagem_formatada') {
      this.produtos.filter((produto) => {
        if (ind == produto.id_produtoFinal) {
          produto.quantidade = 1;
          this.listaPedidos.push(produto);
          $(`#imgProd-${ind}`).removeClass('imgProd')
          $(`#imgProd-${ind}`).css('cursor', 'not-allowed')
          setTimeout(() => {
            $(`#qtde-${ind}`).focus()
            $(`#qtde-${ind}`).val(1)
          }, 100)
        }
      })
      this.listaPedidos.forEach((pedido) => {
        pedido.totalPedido = pedido.valor_produtoFinal * parseFloat(pedido.quantidade);
        valorInicial += pedido.totalPedido;
      })
      this.totalCompra = valorInicial;
    }
  }

  // FUNÇÃO DE REMOVER ITEM DA LISTA DE PEDIDOS
  removeItem(ind) {
    let valorAtualizado = 0;
    this.listaPedidos.filter((pedido) => {
      pedido.totalPedido = pedido.valor_produtoFinal * parseFloat(pedido.quantidade);
      valorAtualizado += pedido.totalPedido;
    })
    this.listaPedidos.filter((pedido) => {
      if (ind == pedido.id_produtoFinal) {
        this.listaPedidos.splice(this.listaPedidos.indexOf(pedido), 1)
        valorAtualizado -= pedido.totalPedido;
        $(`#imgProd-${ind}`).addClass('imgProd')
        $(`#imgProd-${ind}`).css('cursor', 'pointer')
      }
    })
    this.totalCompra = valorAtualizado;
  }

  //FUNÇÃO DE ATUALIZAÇÃO DO TOTAL
  updateTotal(ind) {
    let totalPedido = this.totalCompra;
    let valorTotal = 0;
    this.listaPedidos.forEach((pedido) => {
      if (pedido.id_produtoFinal == ind) {
        pedido.quantidade = parseFloat($(`#qtde-${ind}`).val())
        pedido.totalPedido = pedido.valor_produtoFinal * parseFloat(pedido.quantidade);
      }
      if (pedido.totalPedido) {
        valorTotal += pedido.totalPedido;
      }
      else {
        valorTotal += pedido.valor_produtoFinal * parseFloat(pedido.quantidade);
      }

    })
    this.totalCompra = valorTotal;
  }

  //FUNÇÃO DE SALVAR COMANDA
  salvaComanda() {
    let check = true;
    let nome = "";
    let estoque = 0;
    let valorNegativo: boolean = false;

    this.listaPedidos.forEach((item) => {
      if (item.totalPedido < 0) {
        valorNegativo = true;
      }
      if (item.quantidade > item.qtde_produtoFinal && item.tipo_produto != 'Credit') {
        nome = item.desc_produtoFinal;
        estoque = item.qtde_produtoFinal;
        check = false;
      }
    })
    if (check == true) {
      if (this.listaPedidos.length > 0) {
        if (valorNegativo == false) {
          let data = {
            id_func: JSON.parse(localStorage.getItem('session')).usu_id,
            id_cartao: this.id_cartao,
            pedidos: JSON.stringify(this.listaPedidos),
            total: this.totalCompra
          }
          if (this.isComandaAberta == false) {
            this.service.salvarComanda(data).subscribe(res => {
              swal("Aguarde", "", "info");
              if (res.json().status == 200) {
                swal.close();
                swal("Comanda Salva com Sucesso!", "Comanda salva no sistema.", "success").then((value) => {
                  setTimeout(() => {
                    document.getElementById('id_cartao_focus').focus();
                  }, 100)
                })
                this.goMain();
                this.totalCompra = 0;
              }
              else {
                swal.close();
                swal("Erro Interno!", `Internal Error: Status ${res.json().status}.`, "error")
              }
            })
          }
          else {
            this.service.salvarComandaAberta(data).subscribe(res => {
              if (res.json().status == 200) {
                swal("Comanda Salva com Sucesso!", "Comanda salva no sistema.", "success").then((value) => {
                  setTimeout(() => {
                    document.getElementById('id_cartao_focus').focus();
                  }, 100)
                })
                this.goMain();
                this.totalCompra = 0;
              }
              else {
                swal("Erro Interno!", `Internal Error: Status ${res.json().status}.`, "error");
              }
            })
          }
        } else {
          swal("Atenção!", `Valores negativos não são aceitos.`, "warning");
        }
      } else {
        swal({
          title: "Atenção!",
          text: `Comanda Vazia.`,
          icon: "warning",
          content: {
            element: "input",
            attributes: {
              placeholder: "Senha de Administrador",
              type: "password",
            },
          },
        }).then((senha) => this.cancelarComanda(senha));
      }
    } else {
      swal("Atenção!", `Estoque Insuficiente.\nEstoque atual de ${nome} : ${estoque} UN.`, "warning");
    }
  }

  cleanList() {
    this.listaPedidos.forEach((pedido) => {
      $(`#imgProd-${pedido.id_produtoFinal}`).addClass('imgProd');
      $(`#imgProd-${pedido.id_produtoFinal}`).css('cursor', 'pointer');
    })
    this.totalCompra = 0;
    this.listaPedidos = []
  }

  goMain() {
    this.senha_supervisor = null;
    this.cleanList();
    this.esperandoCartao();
    this.confirmaComanda = false;
    this.id_cartao = null;
  }

  //FUNÇÃO DE ESTILO DOS INPUTS
  estilo(id) {
    switch (id) {
      case 'nomeProd':
        $(`#${id}`).removeClass('nomeAnimHide');
        $(`#${id}`).addClass('nomeAnim');
        break;
    }
  }
  estiloNone(id) {
    switch (id) {
      case 'nomeProd':
        $(`#${id}`).removeClass('nomeAnim');
        $(`#${id}`).addClass('nomeAnimHide');
        break;
    }
  }
  // -----------------------------------------------------

  abrirPesquisa() {
    this.isPesquisarCodigo = true;
    setTimeout(() => {
      document.getElementById('input_codigoBarras').focus();
    }, 200)

  }

  pesquisarCodBarras() {
    let codigo_barras = $('#input_codigoBarras').val();
    let zeros = '0000000000000';
    let limite = 13 - codigo_barras.length;
    let codigo_barras_formatado = zeros.slice(0, limite) + '' + codigo_barras

    this.service.selectPorBarras(codigo_barras_formatado).subscribe(res => {
      if (res.json().status == 200) {
        let pesquisa = res.json().result[0].desc_produtoFinal;
        let ind = res.json().result[0].id_produtoFinal;
        this.addItem(ind);
        $('#input_codigoBarras').val('');
      }
      else {
        $('#input_codigoBarras').val('');
      }
    })
  }

  verificarLength() {
    let codigo_barras = $('#input_codigoBarras').val();
    if (codigo_barras.length > 13) {
      $('#input_codigoBarras').val('');
    }
    else if (codigo_barras.length == 13) {
      this.pesquisarCodBarras();
    }
  }

  cancelarComanda(senha) {
    let data = {
      senha: senha,
      tag: this.id_cartao,
      nome: JSON.parse(localStorage.getItem('session')).usu_nome,
      login: JSON.parse(localStorage.getItem('session')).usu_login,
    }

    this.service.senhaSupervisor(data).subscribe(res => {
      if (res.json().status == 200) {
        this.service.cancelarComanda(data).subscribe(res => {
          if (res.json().status == 200) {
            swal('Sucesso!', 'Comanda Cancelada.\nCancelamento registrado e disponível ao administrador.', 'success').then((value) => {
              setTimeout(() => {
                document.getElementById('id_cartao_focus').focus();
              }, 100)
            });
            this.goMain();
          }
          else {
            swal('Erro!', 'Erro ao cancelar a Comanda.', 'error');
          }
        })
      }
      else if (res.json().status == 100) {
        swal('Atenção!', 'Senha Incorreta.', 'warning');
      }
      else {
        swal('Erro!', 'Erro ao cancelar a Comanda.', 'error');
      }
    })
  }
}
