import swal from 'sweetalert';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  public user: string;
  public pass: string;
  public loginResult: any;

  constructor(private router: Router, private service: LoginService) { }

  ngOnInit() {
    if (localStorage.getItem('session')) {
      this.router.navigate(['main'])
    }
  }

  saveSession(res) {
    //SALVA A SESSÃO
    this.loginResult = res.json();
    localStorage.setItem('session', JSON.stringify(this.loginResult.result[0]))
    // Utilizo esses LocalStorages para verificação de atualização da sangria
    localStorage.setItem('first', 'false')
    localStorage.setItem('welcome', 'false')
    localStorage.setItem('status', 'false')
    // Utilizo esse LocalStorage para armazenar o tipo do usuário
    localStorage.setItem('tipo', this.loginResult.tipo);
    // Utilizo esse LocalStorage para armazenar os cartoes Convidado
    let cartoes = [];
    this.service.selectCartoesConvidados().subscribe(res => {
      if (res.json().status == 200) {
        res.json().result.forEach(resultado => {
          cartoes.push(resultado.car_tag)
        })
        localStorage.setItem('convidados', JSON.stringify(cartoes))
      }
      else {
        swal("Erro Interno!", `Internal Error: Status ${res.json().status}`, 'error');
      }
    })
    this.router.navigate(['main'])
  }

  login(user, pass) {
    //VERIFICA SE AS CREDENCIAIS NÃO SÃO NULAS/VAZIAS
    if (user == undefined || user == null || user == '' || pass == undefined || pass == null || pass == '') {
      swal("Atenção!", "Por favor, preencha todos os campos corretamente.", "warning")
    }
    //GUARDA AS CREDENCIAIS PARA COMPARAR NO BANCO
    else {
      const data = {
        log_login: user,
        log_senha: pass
      }
      this.service.logIn(data).subscribe((res) => {
        if (res.json().status == 500) {
          swal("Erro Interno!", `Internal Error: Status ${res.json().status}.`, 'error');
        }
        //SE CORRETO SALVA SESSÃO
        else {
          if (res.json().status == 200) {
            this.saveSession(res);
          }
          //SENAO STATUS DIALOG
          else {
            swal("Atenção!", `Usuário ou senha incorretos!`, 'warning');
          }
        }
      })
    }
  }
}