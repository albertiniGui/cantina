import { Component, OnInit } from '@angular/core';
import { FilterService } from '../../services/filter.service';
import { Properties } from '../../class/properties';
import { Router, RouterModule, Routes } from '@angular/router';
import { AppTableHead } from '../../app.component';
import { AppTableBody } from '../../app.component';
import { AppTopBar } from '../../app.component';
import { AppBottomBar } from '../../app.component';
import { PatternDialog } from '../../app.component';
import { inspect } from 'util';
import { ControleService } from '../../services/controle.service';

import swal from 'sweetalert';

declare var $: any;

@Component({
  selector: 'app-controle',
  templateUrl: './controle.component.html',
  styleUrls: ['./controle.component.css'],
  providers: [
    ControleService,
    FilterService
  ]
})
export class ControleComponent implements OnInit {

  constructor(private service: ControleService, private filter: FilterService) { }

  public resultados;
  public parametro;
  public tela;
  public cadastro;
  public resultadosFinais;
  public produtosExistentes;
  public receitasExistentes = [];
  public produtosFiltrados;
  public prodReceitas;
  public filtroStatus = false;
  public valor = 0;
  public valorPromocional = 0;
  public nomeProdutoCad = null;
  textDialog = '';
  idProducao;
  insumoEdit = '';
  insumoEditId;

  ngOnInit() {
    this.service.selectControleProduto().subscribe(res => {
      this.resultados = res.json().result;
      this.resultadosFinais = res.json().result;
    });
    this.service.selectAll('produto_final', 'desc_produtoFinal').subscribe(res => {
      this.produtosExistentes = res.json().result;
    })
    this.service.selectProdReceitas().subscribe(res => {
      this.prodReceitas = res.json().result;
    })
    this.service.selectAll('receita', 'desc_receita').subscribe(res => {
      this.receitasExistentes = res.json().result;
    })
    this.tela = 'producao';
  }

  dialog(event, value, idInsumo) {
    this.idProducao = value;
    this.insumoEdit = value;
    this.insumoEditId = idInsumo;
    this.parametro = event;
  }

  telas(event) {
    if (event.tela == 'producao') {
      this.service.selectControleProduto().subscribe(res => {
        this.resultados = null;
        this.resultados = res.json().result;
        this.resultadosFinais = this.resultados;
      });
      this.tela = event.tela;
    } else if (event.tela == 'freezer') {
      this.service.selectControleFreezer().subscribe(res => {
        this.resultados = null;
        this.resultados = res.json().result;
        this.resultadosFinais = this.resultados;
      });
      this.tela = event.tela;
    } else {
      this.service.selectAll(event.tela, event.nome).subscribe(res => {
        this.resultados = null;
        this.resultados = res.json().result;
        this.resultadosFinais = this.resultados;
      });
      this.tela = event.tela;
    }

  }

  confirmation(value) {
    this.cadastro = value;
  }

  cadastrarProducao() {
    var idProd = '';
    var idRec = '';
    let nomeProduto = $("#nomeProdutoCad").val();
    let qtde = $("#qtdeProdutoCad").val();
    let validade = $("#validadeProdutoCad").val();

    this.produtosExistentes.forEach((p) => {
      if (nomeProduto == p.desc_produtoFinal) {
        idProd = p.id_produtoFinal
      }
    })

    let data = {
      'nomeProduto': nomeProduto,
      'qtde': parseInt(qtde),
      'validade': validade,
      'id_produto': idProd
    }
    if (nomeProduto == '' || nomeProduto == undefined || qtde == '' || qtde == undefined || qtde < 0 || qtde == 0 || validade == '' || validade == undefined || validade == '-undefined-undefined') {
      if (qtde < 0 && qtde != '' || qtde == 0 && qtde != '') {
        this.textDialog = 'A quantidade deve ser maior que zero!'
        this.confirmation('Producao Cadastrada');
      } else {
        this.textDialog = 'Preencha todos os campos corretamente'
        this.confirmation('Producao Cadastrada');
      }
    } else {
      this.parametro = false
      this.service.insertProducao(data).subscribe(res => {
        if (res.json().status == 200) {
          swal('Sucesso!', 'Produção cadastrada com sucesso.', 'success')
          // this.textDialog = 'Produção Cadastrada'
          // this.confirmation('Producao Cadastrada');
          this.nomeProdutoCad = null;
          setTimeout(() => {
            this.telas({ tela: 'producao', nome: 'id_producao' })
          }, 100)
        } else if (res.json().status == 100) {
          swal('Atenção!', 'Receita não encontrada!', 'warning')
        }
        else {
          swal('Erro!', 'Falha ao cadastrar produção.', 'error')
        }
      })
    }
  }

  excluirProducao(id) {
    let data = {
      'producao': id
    }
    this.parametro = false
    this.textDialog = 'Produção excluida'
    this.service.deleteProducao(data).subscribe(res => {
      if (res.json().status == 200) {
        swal('Sucesso!', 'Produção excluída com sucesso.', 'success')
        // this.confirmation('Producao Excluida');
        this.telas({ tela: 'producao', nome: 'id_producao' })
      } else {
        swal('Erro!', 'Falha ao excluir produção.', 'error')
        // this.textDialog = 'Falha ao excluir Produção!'
        // this.confirmation('Producao Cadastrada');
      }
    });
  }

  excluirInsumo(nome) {
    let data = {
      'insumo': nome
    }
    this.parametro = false
    this.service.deleteInsumo(data).subscribe(res => {
      if (res.json().status == 200) {
        swal('Sucesso!', 'Insumo excluído com sucesso.', 'success')
        // this.textDialog = 'Insumo excluído'
        // this.confirmation('Producao Excluida');
        this.telas({ tela: 'insumo', nome: 'desc_insumo' })
      } else if (res.json().status == 100) {
        swal('Atenção!', 'Este insumo é usado em alguma receita.', 'warning')
      } else {
        swal('Erro!', 'Erro ao excluir insumo.', 'error')
        // this.textDialog = 'Falha ao excluir Insumo!'
        // this.confirmation('Producao Excluida');
        // this.telas({ tela: 'insumo', nome: 'desc_insumo' })
      }
    });
  }

  cadastrarEntrada() {
    let qtde = $("#quantidadeCad").val();
    let insumo = this.insumoEditId
    let data = {
      'qtde': qtde,
      'insumo': insumo
    }
    if (qtde == '' || qtde == undefined || qtde <= 0 || insumo == '' || insumo == undefined) {
      if (qtde <= 0) {
        swal('Atenção!', 'Os valores não podem ser inferiores ou iguais a zero.', 'warning')
        // this.textDialog = 'Os valores não podem ser inferiores ou iguais a zero!'
        // this.confirmation('Entrada Cadastrada')
      } else {
        swal('Atenção!', 'Preencha os campos corretamente.', 'warning')
        // this.textDialog = 'Preencha todos os campos corretamente!'
        // this.confirmation('Entrada Cadastrada')
      }
    } else {
      this.parametro = false
      this.textDialog = 'Entrada Cadastrada'
      this.service.entradaEstoqueInsumo(data).subscribe(res => {
        if (res.json().status == 200) {
          swal('Sucesso!', 'Entrada cadastrada com sucesso.', 'success')
          // this.confirmation('Entrada Cadastrada');
          setTimeout(() => {
            this.telas({ tela: 'insumo', nome: 'desc_insumo' })
          }, 100)
        } else {
          swal('Erro!', 'Falha ao cadastrar entrada.', 'error')
          this.textDialog = 'Falha ao cadastrar entrada!'
          this.confirmation('Entrada Cadastrada')
        }
      });
    }
  }

  cadastrarRegistro() {
    let freezer = $("#freezerCad").val();
    let temperatura = $("#temperaturaCad").val();
    let data = {
      'freezer': freezer,
      'temperatura': temperatura
    }
    if (freezer == '' || freezer == undefined || freezer <= 0 || temperatura == '' || temperatura == undefined) {
      if (freezer <= 0) {
        this.textDialog = 'O ID não pode ser inferior ou igual a zero!';
        this.confirmation('Registro Cadastrado');
      } else {
        this.textDialog = 'Preencha todos os campos corretamente!';
        this.confirmation('Registro Cadastrado');
      }
    } else {
      this.parametro = false
      this.service.cadastrarRegistro(data).subscribe(res => {
        if (res.json().status == 200) {
          swal('Sucesso!', 'Registro cadastrado com sucesso.', 'success')
          // this.textDialog = 'Registro Cadastrado!';
          // this.confirmation('Registro Cadastrado');
          this.telas({ tela: 'freezer', nome: 'id_freezer' })
        } else {
          swal('Erro!', 'Falha ao cadastrar registro.', 'error')
          // this.textDialog = 'Falha ao cadastrar o registro!';
          // this.confirmation('Registro Cadastrado');
        }
      });

    }
  }

  editarProducao(id) {
    let qtde = $("#qtdeProdutoEdit").val();
    let validade = $("#validadeProdutoEdit").val();
    let nome = $(`#produto-${id}`).val()

    var idProd;
    var nomeProduto;
    this.produtosExistentes.forEach((p) => {
      if (this.idProducao == p.id_produtoFinal) {
        idProd = p.id_produtoFinal
      }
    })

    let data = {
      'id': id,
      'qtde': qtde,
      'validade': validade,
      'id_produto': idProd
    }
    if (qtde == '' || qtde == undefined || validade == '' || validade == undefined || validade == '-undefined-undefined' || nome == '' || nome == undefined) {
      this.textDialog = 'Preencha todos os campos corretamente'
      this.confirmation('Producao Editada');
    } else {
      this.parametro = false
      this.textDialog = 'Produção Editada'
      this.service.updateProducao(data).subscribe(res => {
        if (res.json().status == 200) {
          swal('Sucesso!', 'Produção editada com sucesso.', 'success')
          // this.confirmation('Producao Editada');
          this.telas({ tela: 'producao', nome: 'id_producao' })
        } else {
          swal('Erro!', 'Falha ao editar produção.', 'error')
          // this.textDialog = 'Falha ao editar produção!';
          // this.confirmation('Producao Editada');
        }
      });
    }
  }

  editarEntrada(id) {
    let qtde = $("#quantidadeEdit").val();
    let data = {
      'id': id,
      'qtde': qtde,
    }
    if (qtde == '' || qtde == undefined) {
      this.textDialog = 'Preencha todos os campos corretamente'
      this.confirmation('Entrada Editada');
    } else {
      this.service.updateEntrada(data).subscribe(res => {
        if (res.json().status == 200) {
          this.parametro = false
          swal('Sucesso!', 'Entrada editada com sucesso.', 'success')
          // this.textDialog = 'Entrada Editada'
          // this.confirmation('Entrada Editada');
          this.telas({ tela: 'insumo', nome: 'desc_insumo' })
        } else {
          swal('Erro!', 'Falha ao editar entrada.', 'error')
          // this.textDialog = 'Falha ao editar entrada!'
          // this.confirmation('Entrada Editada');
        }
      });
    }
  }

  editarRegistro(id, dataFreez) {
    let temperatura = $("#temperaturaEdit").val();

    let dataTemp = dataFreez.split("/");
    let horaTemp = dataTemp[2].split(" às ")

    let dataRegistro = horaTemp[0] + '-' + dataTemp[1] + '-' + dataTemp[0] + ' ' + horaTemp[1];
    let data = {
      'id': id,
      'temperatura': temperatura,
      'dataRegistro': dataRegistro
    }
    if (temperatura == '' || temperatura == undefined || dataRegistro == '' || dataRegistro == undefined) {
      this.textDialog = 'Preencha todos os campos corretamente'
      this.confirmation('Registro Editado');
    } else {

      this.service.editarRegistro(data).subscribe(res => {
        if (res.json().status == 200) {
          swal('Sucesso!', 'Registro editado com sucesso.', 'success')
          // this.confirmation('Registro Editado');
          // this.textDialog = 'Registro Editado'
          this.parametro = false
          this.telas({ tela: 'freezer', nome: 'id_freezer' })
        } else {
          swal('Erro!', 'Falha ao editar registro.', 'error')
          // this.textDialog = 'Falha ao editar registro!'
          // this.confirmation('Registro Editado');
        }
      });
    }
  }

  openInputSearch(name) {
    $("#inputSearch" + name).css('width', '');
    if ($("#inputSearch" + name).hasClass("openSearch")) {
      $("#inputSearch" + name).removeClass("openSearch");
      $("#inputSearch" + name).addClass("closeSearch");
    } else {
      $("#inputSearch" + name).removeClass("closeSearch");
      $("#inputSearch" + name).addClass("openSearch");
      $(`#${name}`).focus();
    }
  }

  bottomFilterProducao(input) {
    var regexp = new RegExp((".*" + input.split("").join('.*') + ".*"), "i");
    this.resultadosFinais = this.resultados.filter((r) => {
      return regexp.test(r.desc_produtoFinal);
    });
  }

  bottomFilterInsumo(input) {
    var regexp = new RegExp((".*" + input.split("").join('.*') + ".*"), "i");
    this.resultadosFinais = this.resultados.filter((r) => {
      return regexp.test(r.desc_insumo);
    });
  }

  bottomFilterFreezer(input) {
    var regexp = new RegExp((".*" + input.split("").join('.*') + ".*"), "i");
    this.resultadosFinais = this.resultados.filter((r) => {
      return regexp.test(r.id_freezer);
    });
  }

  filterProdutos(input) {
    if (input) {
      this.filtroStatus = true;
    } else {
      this.filtroStatus = false;
    }
    var regexp = new RegExp((".*" + input.split("").join('.*') + ".*"), "i");
    this.produtosFiltrados = this.prodReceitas.filter((r) => {
      return regexp.test(r.desc_produtoFinal);
    });
  }

  filterProdutosEdit(input) {
    if (input) {
      this.filtroStatus = true;
    } else {
      this.filtroStatus = false;
    }
    var regexp = new RegExp((".*" + $(`#produto-${input}`).val().split("").join('.*') + ".*"), "i");
    this.produtosFiltrados = this.prodReceitas.filter((r) => {
      return regexp.test(r.desc_produtoFinal);
    });
  }
  completar(index) {
    let produtoEscolhido = this.produtosFiltrados[index];
    $("#nomeProdutoCad").val(produtoEscolhido.desc_produtoFinal);
    this.idProducao = produtoEscolhido.id_produtoFinal
    this.valor = produtoEscolhido.valor_produtoFinal;
    this.valorPromocional = produtoEscolhido.valorDesc_produtoFinal;
    this.filtroStatus = false;
  }

  completarEdit(index, id) {
    let produtoEscolhido = this.produtosFiltrados[index];
    $(`#produto-${id}`).val(produtoEscolhido.desc_produtoFinal);
    this.idProducao = produtoEscolhido.id_produtoFinal
    $("#valorEdit").val(produtoEscolhido.valor_produtoFinal)
    $("#valorPromoEdit").val(produtoEscolhido.valorDesc_produtoFinal)
    this.filtroStatus = false;
  }

  // Serviço de filtro AutoComplete, basta passar os parâmetros
  resultInsumo = [];
  filterComplete(model) {
    this.resultInsumo = this.filter.filterData(model, this.resultados, 'insumo')
  }

  closeComplete() {
    setTimeout(() => {
      this.resultInsumo = [];
    }, 200)
  }
}