import swal from 'sweetalert';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { MainService } from '../../services/main.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

declare var $: any;

@Component({
	selector: 'app-main',
	templateUrl: './main.component.html',
	styleUrls: ['./main.component.css']
})

export class MainComponent implements OnInit {

	@Output('status') status = new EventEmitter<any>();

	info = false;
	credito = false;
	recarga = false;
	abertas = false;
	sangria = false;
	welcome = false;
	caixaStatus = false;
	usuario = false;

	sessao = [];
	caixa = [];

	tipo = 'entregue';
	caixaInicial = 0;
	caixaAtual = 0;

	hoje;
	today;
	valorCaixa = this.caixaInicial;

	constructor(private router: Router, private service: MainService, private http: Http) { }

	redi() {
		this.router.navigate(['login'])
	}

	verificaTipoLogin() {
		let botaoComanda = <HTMLButtonElement>document.getElementById("buttonBar-0")
		let botaoCaixa = <HTMLButtonElement>document.getElementById("buttonBar-1")
		let botaoControle = <HTMLButtonElement>document.getElementById("buttonBar-2")
		let botaoCadastro = <HTMLButtonElement>document.getElementById("buttonBar-3")
		let botaoCompras = <HTMLButtonElement>document.getElementById("buttonBar-4")
		let botaoRelatorio = <HTMLButtonElement>document.getElementById("buttonBar-5")
		let botaoSangria = <HTMLButtonElement>document.getElementById("btSangria");
		let divSangria = <HTMLDivElement>document.getElementById("sangria");

		switch (parseInt(localStorage.getItem("tipo"))) {
			case 2:
				botaoCadastro.style.display = "none";
				botaoCompras.style.display = "none";
				break;
			case 3:
				botaoCadastro.style.display = "none";
				break;
			case 4:
				botaoComanda.style.display = 'none';
				botaoCaixa.style.display = 'none';
				botaoSangria.style.display = 'none';
				divSangria.style.display = 'none';
				break;
		}

	}

	ngOnInit() {
		if (!localStorage.getItem('session')) {
			this.router.navigate(['loading'])
			setTimeout(() => this.redi(), 1000)
		} else {
			this.router.navigate(['main']).then((resolve) => {
				this.verificaTipoLogin();
			})
		}

		this.caixa = []
		this.sessao = [JSON.parse(localStorage.getItem('session'))]
		this.service.selectAll('sangria', 'id_sangria').subscribe(res => {
			let r = res.json().result;
			this.caixa.push({ caixa: r[r.length - 1].valorAtual })
			this.caixaInicial = this.caixa[0].caixa
		})

		if (localStorage.getItem('welcome') == 'true' && localStorage.getItem('status') == 'true') {
		}
		else if (parseInt(localStorage.getItem("tipo")) == 4) {
			this.sangria = false;
		}
		else {
			this.today = new Date();
			localStorage.setItem('welcome', 'true');
			$("#icon-sangria").css('color', '#F44336');
			setTimeout(() => {
				this.hoje = Date.now();
				this.welcome = true;
				this.sangria = true;
			}, 50)
		}
	}
	setValue() {
		if (this.caixaInicial > this.caixaAtual && this.caixaAtual != null) {
			this.tipo = 'entregue';
			this.valorCaixa = (this.caixaInicial - this.caixaAtual);
		} else {
			this.tipo = 'recebido';
			this.valorCaixa = this.caixaAtual - this.caixaInicial;
		}
	}

	saveCash() {
		if (isNaN(this.caixaAtual) == true || this.valorCaixa == undefined || this.valorCaixa == null || this.caixaAtual == undefined || this.caixaAtual == null || this.caixaInicial == undefined || this.caixaInicial == null) {
			swal('Atenção!', 'Campos não podem estar vazios.', 'warning')
		} else {
			let data = {
				valorInicial: this.caixaInicial,
				valorAtual: this.caixaAtual,
				saldo: this.valorCaixa,
				func: this.sessao[0].usu_id
			}
			this.caixaInicial = this.caixaAtual
			this.service.sangria(data).subscribe(res => {
				if (res.json().status == 200) {
					$("#welcome").removeClass('welcome')
					$("#welcome").addClass('goodbye')
					localStorage.setItem('status', 'true');
					this.showSangria();
					this.caixaAtual = 0;
					this.valorCaixa = 0;
				} else {
					swal("Erro Interno.", `Internal Error: Status ${res.json().status}`)
				}
			})
		}
	}

	showSangria() {
		this.service.selectAll('sangria', 'id_sangria').subscribe(res => {
			let r = res.json().result;
			this.caixa.push({ caixa: r[r.length - 1].valorAtual })
			this.caixaInicial = r[r.length - 1].valorAtual
		})
		if (localStorage.getItem('status') == 'true') {
			if (this.sangria == false) {
				$("#icon-sangria").css('color', '#F44336');
				$("#sangria").addClass('jumpIn');
				$("#sangria").removeClass('jumpOut');
				this.hoje = Date.now();
				this.sangria = true;
			} else {
				$("#icon-sangria").css('color', '#fff');
				$("#sangria").removeClass('jumpIn');
				$("#sangria").addClass('jumpOut');
				setTimeout(() => {
					this.sangria = false;
				}, 201)
			}
		}
	}

	logOff() {
		localStorage.removeItem('session');
		localStorage.removeItem('welcome');
		localStorage.removeItem('tipo');
		localStorage.removeItem('usu_id');
		localStorage.removeItem('convidados');
		this.router.navigate(['loading']);
		setTimeout(() => {
			this.router.navigate(['login'])
		}, 1250)
	}

	showInfo() {
		if (this.info == true) {
			$("#infoDiv").removeClass('jumpIn');
			$("#infoDiv").addClass('jumpOut');
			setTimeout(() => {
				this.info = false;
			}, 201)
		} else {
			this.info = true;
		}
	}

	showCredito() {
		if (this.credito == true) {
			$("#CreditoDiv").removeClass('jumpIn');
			$("#CreditoDiv").addClass('jumpOut');
			setTimeout(() => {
				this.credito = false;
			}, 201)
		} else {
			this.credito = true;
		}
	}

	showCadastro() {
		if (this.usuario == true) {
			$("#usuarioDiv").removeClass('jumpIn');
			$("#usuarioDiv").addClass('jumpOut');
			setTimeout(() => {
				this.usuario = false;
			}, 201)
		} else {
			this.usuario = true;
		}
	}

	dialogs = [
		{
			title: 'Inserir na Comanda',
			istyle: {},
			media: 'comanda',
			style: { 'margin': '3px', 'margin-bottom': '12px', 'height': '12%', 'text-transform': 'none', 'width': '95%' },
			css: { 'display': 'table', 'margin': '0px auto', 'width': '100%', 'height': '100%', 'position': 'absolute', 'top': '0px', 'left': '0px', 'z-index': '10' },
			icon: "add_shopping_cart",
			visible: false,
			active: false,
			param: 'comanda'
		},
		{
			title: 'Caixa Cantina',
			istyle: {},
			media: 'caixa',
			style: { 'margin': '3px', 'margin-bottom': '12px', 'height': '12%', 'text-transform': 'none', 'width': '95%' },
			css: { 'display': 'table', 'margin': '0px auto', 'width': '100%', 'height': '100%', 'position': 'relative', 'top': '0px', 'left': '0px', 'z-index': '11' },
			icon: "shopping_cart",
			visible: false,
			active: false,
			param: 'caixa'
		},
		{
			title: 'Controle',
			istyle: {},
			media: 'controle',
			style: { 'margin': '3px', 'margin-bottom': '12px', 'height': '12%', 'text-transform': 'none', 'width': '95%' },
			css: { 'display': 'table', 'margin': '0px auto', 'width': '100%', 'height': '100%', 'position': 'absolute', 'top': '0px', 'left': '0px', 'z-index': '12' },
			icon: "unarchive",
			visible: false,
			active: false,
			param: 'controle'
		},
		{
			title: 'Cadastrar',
			istyle: {},
			media: 'cadastrar',
			style: { 'margin': '3px', 'margin-bottom': '12px', 'height': '12%', 'text-transform': 'none', 'width': '95%' },
			css: { 'display': 'table', 'margin': '0px auto', 'width': '100%', 'height': '100%', 'position': 'absolute', 'top': '0px', 'left': '0px', 'z-index': '13' },
			icon: "kitchen",
			visible: false,
			active: false,
			param: 'cadastrar'
		},
		{
			title: 'Compras',
			istyle: {},
			media: 'compras',
			style: { 'margin': '3px', 'margin-bottom': '12px', 'height': '12%', 'text-transform': 'none', 'width': '95%' },
			css: { 'display': 'table', 'margin': '0px auto', 'width': '100%', 'height': '100%', 'position': 'absolute', 'top': '0px', 'left': '0px', 'z-index': '14' },
			icon: "store_mall_directory",
			visible: false,
			active: false,
			param: 'compras'
		},
		{
			title: 'Relatórios',
			istyle: {},
			media: 'relatorios',
			style: { 'margin': '3px', 'margin-bottom': '12px', 'height': '12%', 'text-transform': 'none', 'width': '95%' },
			css: { 'display': 'table', 'margin': '0px auto', 'width': '100%', 'height': '100%', 'position': 'absolute', 'top': '0px', 'left': '0px', 'z-index': '15' },
			icon: "content_paste",
			visible: false,
			active: false,
			param: 'relatorios'
		}
	];

	// Função utilizada em outros components
	hideAnim(id) {
		$(`#buttonBar-${id}`).css('border', 'none');
		$(`#icon-${id}`).animate({
			fontSize: '24px',
			color: '#fff'
		}, 150)
		$(`#span-${id}`).animate({
			height: 'show'
		}, 200, () => {
			$(`#span-${id}`).html(this.dialogs[id].title);
		})
	}

	styleFunction(id, param) {
		for (let i = 0; i < this.dialogs.length; i++) {
			$(`#buttonBar-${i}`).css('border', 'none');
			$(`#icon-${i}`).animate({
				fontSize: '24px',
				color: '#fff'
			}, 150)
			$(`#span-${i}`).animate({
				height: 'show'
			}, 200, () => {
				$(`#span-${i}`).html(this.dialogs[i].title);
			})
		}
		switch (param) {
			case this.dialogs[id].param:
				if (this.dialogs[id].active == true && this.dialogs[id].visible == true) {
					$(`#buttonBar-${id}`).css('border-bottom', '1px solid #fff');
					$(`#buttonBar-${id}`).css('border-top', '1px solid #fff');

					$(`#icon-${id}`).animate({
						fontSize: '40px',
						color: '#f44336'
					}, 200)
					$(`#span-${id}`).animate({
						height: 'hide'
					}, 200)
				}
				break;
		}
	}

	hideAndShow(ind) {

		if (this.dialogs[ind].visible == true && this.dialogs[ind].active == true) {
			this.dialogs[ind].visible = false
			this.dialogs[ind].active = false
			this.router.navigate(['main']);
		} else {
			this.dialogs.map((d) => {
				d.visible = false;
			})
			this.dialogs[ind].active = false;
			this.dialogs[ind].visible = true;
			this.dialogs[ind].active = true;

			if (this.dialogs[ind].active == false) {
				this.dialogs[ind].visible = false;
			}
		}
	}

	configs = { 'sidebarColor': 'mdl-color--grey-900', 'sidebarTextColor': 'mdl-color-text--white', 'sidebarSeparatorBorder': { 'border': "1px solid #FAFAFA" } };

}

@Component({
	selector: 'app-info',
	templateUrl: './info.html',
	styleUrls: ['./main.component.css']
})

export class AppInfo {

	constructor(private main: MainComponent) { }


	public closeInfo() {
		$("#infoIcon").css("color", "#fff")
		$("#infoDiv").removeClass('jumpIn');
		$("#infoDiv").addClass('jumpOut');
		setTimeout(() => {
			this.main.info = false
		}, 201)
	}

	public organizadores = ["Ricardo Massaru", "Daniel Cruz"];
	public desenvolvedores = ["Guilherme Albertini", "Miguel Braz", "Fillipe Martins", "Gustavo Farias", "Felipe Bonazzi"];

}

@Component({
	selector: 'app-usuario',
	templateUrl: './usuario.html',
	styleUrls: ['./main.component.css']
})

export class AppUsuario {

	constructor(private main: MainComponent, private service: MainService) { }

	rg = null;
	nome = null;
	tipo = null;
	login = null;
	senha = null;
	telefone = null;
	icon;
	iconStyle;
	textoCadastro;
	confirmaCadastro;
	titleCadastro = 'Cadastro';

	isFecharTudo = false;

	public closeInfo() {
		$("#usuarioIcon").css("color", "#fff")
		$("#usuarioDiv").removeClass('jumpIn');
		$("#usuarioDiv").addClass('jumpOut');
		setTimeout(() => {
			this.main.usuario = false
		}, 201)
	}

	cadastrarUsuario() {
		if (this.nome && this.rg && this.telefone && this.login && this.senha && this.tipo) {
			this.isFecharTudo = false;
			let data = {
				nome: this.nome,
				rg: this.rg,
				telefone: this.telefone,
				login: this.login,
				senha: this.senha,
				tipo: this.tipo
			}
			this.service.cadastrarUsuario(data).subscribe(res => {
				if (res.json().status == 200) {
					swal('Sucesso!', "Usuário cadastrado com sucesso.", "success")
					this.nome = null;
					this.rg = null;
					this.telefone = null;
					this.login = null;
					this.senha = null;
					this.tipo = null;
				}
				else {
					swal('Erro!', "Erro ao cadastrar usuário.", "error")
				}
			})
		} else {
			swal('Atenção!', "Preencha os campos corretamente.", "warning")
		}
	}
}

@Component({
	selector: 'app-credito',
	templateUrl: './credito.html',
	styleUrls: ['./main.component.css']
})

export class AppCredito implements OnInit {

	constructor(private main: MainComponent, private service: MainService) { }

	public car_tag;
	public car_credito;
	public isCredito = false;

	ngOnInit() {
		setTimeout(() => {
			document.getElementById('input_carTag').focus();
		}, 100)
	}

	public closeCredito() {
		$("#CreditoIcon").css("color", "#fff")
		$("#CreditoDiv").removeClass('jumpIn');
		$("#CreditoDiv").addClass('jumpOut');
		setTimeout(() => {
			this.main.credito = false
		}, 201)
	}

	checarCredito() {
		this.service.checarCreditoMain(this.car_tag).subscribe(res => {
			this.isCredito = true;
			this.car_tag = '';
			this.car_credito = res.json().result;
		})
	}

	voltarCredito(){
		this.isCredito = false
		setTimeout(() => {
			document.getElementById('input_carTag').focus();
		}, 100)
	}

}
