import { Component, OnInit } from '@angular/core';
import { Http, HttpModule } from '@angular/http'
import { FilterService } from '../../services/filter.service';
import { Properties } from '../../class/properties';
import { AppTableHead } from '../../app.component';
import { AppTableBody } from '../../app.component';
import { AppTopBar } from '../../app.component';
import { AppBottomBar } from '../../app.component';
import { PatternDialog } from '../../app.component';
import { TextDialog } from '../../app.component';
import { Router } from '@angular/router';
import { CadastroService } from '../../services/cadastro.service';

import swal from 'sweetalert';

declare var $: any;

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css'],
  providers: [
    CadastroService,
    FilterService
  ]
})
export class CadastroComponent implements OnInit {

  constructor(private service: CadastroService, private http: Http, private filter: FilterService, private router: Router) { }

  public resultados;
  public resultadosFinais;
  public resultadosReceitas;
  public todosProdutos;
  public receitas = [];
  public parametro;
  public tela;
  public cadastro;
  public ingredientes = [];
  public imgEscolhida = [];
  public imgAtual;
  public produtos = ['Bebidas Quentes', 'Bebidas Frias', 'Bebidas Industrializadas', 'Lanche Natural', 'Salgados Comprados', 'Salgados Produzidos Assados', 'Salgados Produzidos Fritos', 'Salgados Congelados', 'Doces Comprados', 'Doces Produzidos', 'Doces Congelados', 'Prato do Dia', 'Prato Congelado'];
  public unidadesCadastro = ['UN', 'GR'];
  public tiposCadastro = ['Doce', 'Salgado', 'Bebida',];
  public statusCadastro = ['Ativo', 'Inativo'];
  public textDialog = ''
  public insumosExistentes;
  public insumosFiltrados;
  public filtroStatus = false;
  public completeNome = false;
  public nomeProdutoAntigo
  //ngModels valores
  nomeReceita;
  nomeEditReceita;
  valorProdutoEdit;
  valorPromocionalEdit;
  valorProduto;
  valorPromo;
  nomeIns;
  criador;
  aprovado;
  dataInicio;
  ativo;
  alterarCriador;
  alterarAprovado;
  alterarDataInicio;
  alterarAtivo;

  ngOnInit() {
    let tipo = JSON.parse(localStorage.getItem('session')).tipo;
    if (tipo == 3 || tipo == 2) {
      this.router.navigate(['main'])
    }
    try {
      this.service.selectAll('produto_final', 'desc_produtoFinal').subscribe(res => {
        this.resultados = res.json().result;
        this.resultadosFinais = res.json().result;
        this.todosProdutos = res.json().result;
      });
      this.service.selectAll('insumo', 'desc_insumo').subscribe(res => {
        this.insumosExistentes = res.json().result;
      })
      this.tela = 'produto_final';
    } catch (exception) {

    }
  }

  dialog(event) {
    this.parametro = event;
    this.ingredientes = [];
    this.nomeReceita = '';
    this.zerarVariaveis();
  }

  telas(event) {
    // Altera as "telas" entre receita, produto, insumo, etc.
    this.receitas = []
    if (event.tela == 'receita') {
      this.service.selectAll('receita', 'desc_receita').subscribe(res => {
        let i = 0;
        res.json().result.forEach((r) => {
          let newIngredients = JSON.parse(r.ingredientes)

          this.ingredientes.push({
            id: r.id_receita,
            ingredientes: JSON.parse(r.ingredientes)
          })

          this.receitas.push({
            custo: r.custo,
            desc_receita: r.desc_receita,
            id_receita: r.id_receita,
            ingredientes: JSON.parse(r.ingredientes),//this.ingredientes[i],
            modoPreparo_receita: r.modoPreparo_receita,
            rendimento: r.rendimento,
            tempoPreparo_receita: r.tempoPreparo_receita,
            ativo: r.ativo,
            criador: r.criador,
            aprovado: r.aprovado,
            dataIni: r.dataIni.split('T')[0]
          })
          i++;
        })
        this.resultados = this.receitas;
        this.resultadosFinais = this.receitas;
      });
      this.tela = event.tela;

    } else {
      this.service.selectAll(event.tela, event.nome).subscribe(res => {
        this.resultados = res.json().result;
        this.resultadosFinais = res.json().result;
      });
      this.tela = event.tela;
    }
  }

  setValues(numero, nomeAntigo) {
    this.imgEscolhida.unshift({ image: this.resultadosFinais[numero].img_produtoFinal });
    this.nomeProdutoAntigo = nomeAntigo;
  }

  private base64textString: String = "";
  getImage(evt) {
    var files = evt.target.files;
    var file = files[0];
    this.imgEscolhida.unshift({ image: file.name });
    if (files && file) {
      var reader = new FileReader();
      reader.onload = this.baseReader.bind(this);
      reader.readAsBinaryString(file);
    }
    $("#imgBack").css('color', 'transparent')
    setTimeout(() => {
      document.getElementById("fileImages").style.background = "url(./assets/images/" + file.name + ")";
      document.getElementById("fileImages").style.backgroundSize = "100% 100%";
    }, 500)
  }

  baseReader(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);

    let dataImg = {
      name: this.imgEscolhida[0].image,
      base: this.base64textString
    }
    this.service.saveImage(dataImg).subscribe(res => {
      if (res.json().code == 100) swal('Erro!', 'Erro ao salvar imagem.', 'error')
    })
  }

  //Recebe todos os valores e cadastra o produto
  cadastrarProduto() {
    let imagem;
    try {
      if (this.imgEscolhida[0].image == undefined) {
        imagem = ''
      } else {
        imagem = this.imgEscolhida[0].image;
      }
    } catch (e) {
    }
    let nome = $("#nomeCadProd").val();
    let codigo_barras = $("#codigoBarras").val();
    let valorParc = $("#valorProduto").val().split('R$ ');
    let valor = valorParc[1].split(',')[0] + '.' + valorParc[1].split(',')[1];
    let valorPromoParc = $("#valorPromo").val().split('R$ ');
    let valorPromo = valorPromoParc[1].split(',')[0] + '.' + valorPromoParc[1].split(',')[1];
    let qtdeMin = $("#qtdeMin").val();
    let qtdeMax = $("#qtdeMax").val();
    let unidade = (<HTMLInputElement>document.getElementById("unidadeVenda")).value;
    let tipo = (<HTMLInputElement>document.getElementById("tipoProduto")).value;
    let status = (<HTMLInputElement>document.getElementById("status")).value;

    let data = {
      'desc_produtoFinal': nome,
      'img_produtoFinal': imagem,
      'valor_produtoFinal': valor,
      'valorDesc_produtoFinal': valorPromo,
      'qtdeMin_produtoFinal': parseInt(qtdeMin),
      'qtdeMax_produtoFinal': parseInt(qtdeMax),
      'unidadeVenda_produtoFinal': unidade,
      'tipo_produto': tipo,
      'status_produtoFinal': status,
      'codigo_barras': codigo_barras
    }

    if (nome == undefined || nome == '' || imagem == undefined || imagem == '' || valor == undefined || valor == '' || valorPromo == undefined || valorPromo == '' ||
      qtdeMin == undefined || qtdeMin == '' || qtdeMin < 0 || qtdeMax == undefined || qtdeMax == '' || qtdeMax <= 0) {
      if (qtdeMin < 0 || qtdeMax <= 0) {
        swal("Atenção!", "Os valores não podem ser iguais ou inferiores a zero.", "warning")
      } else {
        swal("Atenção!", "Preencha os campos corretamente.", "warning")
      }
    } else {
      if (valorPromo >= valor) {
        swal("Atenção!", "Valor Promocional tem que ser menor que o valor.", "warning")
      } else {
        this.parametro = false;
        this.service.cadastrarProduto(data).subscribe(res => {
          if (res.json().status == 200) {
            this.imgEscolhida = [];
            swal("Sucesso!", "Produto final cadastrado.", "success")
            this.telas({ tela: 'produto_final', nome: 'desc_produtoFinal' })
          } else if (res.json().status == 100) {
            swal("Atenção!", "Nome e/ou Código de Barras já existentes.", "warning")
          }
          else {
            swal("Erro!", "Erro ao cadastrar produto final.", "error")
          }
        });
      }
    }
  }

  // Edita os ingredientes selecionados
  editIngredients(event) {
    event = event.split("-");
    this.resultadosFinais.map((r) => {
      if (r.id_receita == event[1]) {
        this.nomeEditReceita = r.desc_receita
        this.ingredientes = r.ingredientes
      }
    })
  }

  // Recebe os dados e realiza o cadastro do insumo
  cadastrarInsumo() {
    let nome = $("#nomeInsumoCad").val();
    let unidade = $("#unidadeInsumo").val();
    let qtdeMin = $("#qtdeMinCad").val();
    let qtdeMax = $("#qtdeMaxCad").val();
    let data = {
      'desc_insumo': nome,
      'unidade_insumo': unidade,
      'qtdeMin_insumo': qtdeMin,
      'qtdeMax_insumo': qtdeMax
    }
    if (nome == undefined || nome == '' || unidade == undefined || unidade == '' || qtdeMin == undefined || qtdeMin == '' || qtdeMin < 0 || qtdeMax == undefined || qtdeMax == '' || qtdeMax <= 0) {
      if (qtdeMin < 0 || qtdeMax <= 0) {
        swal("Atenção!", "Os valores não podem ser iguais ou inferiores a zero.", "warning")
      } else {
        swal("Atenção!", "Preencha os campos corretamente.", "warning")
      }
    } else {
      this.parametro = false;
      this.service.cadastrarInsumo(data).subscribe(res => {
        if (res.json().status == 200) {
          swal("Sucesso!", "Insumo cadastrado com sucesso.", "success")
          this.telas({ tela: 'insumo', nome: 'desc_insumo' })
        } else if (res.json().status == 100) {
          swal("Atenção!", "Insumo já existente.", "warning")
        } else {
          swal("Erro!", "Falha ao cadastrar insumo.", "error")
        }
      });

    }
  }

  // Recebe os dados e realiza o cadastro da receita
  cadastrarReceita() {
    let nome = $("#nomeReceita").val();
    let rendimento = $("#rendimentoReceita").val();
    let tempoPreparo = $("#tempoPreparoReceita").val();

    try {
      let custoParc = $("#custoProducao").val().split('R$ ');
      var custo = custoParc[1].split(',')[0] + '.' + custoParc[1].split(',')[1];
    } catch (e) {
      custo == undefined
    }
    let ingredientes = JSON.stringify(this.ingredientes).trim();
    let preparo = $("#modoPreparo").val();
    let data = {
      'desc_receita': nome,
      'rendimento': rendimento,
      'tempoPreparo_receita': tempoPreparo,
      'custo': custo,
      'ingredientes': ingredientes,
      'modoPreparo_receita': preparo,
      'criador': this.criador,
      'aprovado': this.aprovado,
      'dataIni': this.dataInicio,
      'ativo': this.ativo
    }
    if (this.dataInicio == '' || this.dataInicio == undefined || this.criador == '' || this.criador == undefined ||
      this.aprovado == '' ||
      this.aprovado == undefined || this.ativo == '' || this.ativo == undefined || nome == '' || nome == undefined ||
      rendimento == '' || rendimento == undefined || rendimento <= 0 || tempoPreparo == '' ||
      tempoPreparo == undefined || tempoPreparo <= 0 || custo == '' || custo == undefined ||
      ingredientes == '[]' || ingredientes == undefined || preparo == '' || preparo == undefined ||
      this.completeNome == false) {
      if (rendimento <= 0 || tempoPreparo <= 0) {
        swal("Atenção!", "Os valores não podem ser iguais ou inferiores a zero.", "warning")
      } else if (this.completeNome == false) {
        swal("Atenção!", "O nome da receita deve ser o mesmo de um produto já existente.", "warning")
      } else {
        swal("Atenção!", "Preencha todos os campos corretamente.", "warning")
      }
    } else {
      this.parametro = false;
      this.service.cadastrarReceita(data).subscribe(res => {
        if (res.json().status == 200) {
          swal("Sucesso!", "Receita cadastrada com Sucesso.", "success")
          this.telas({ tela: 'receita', nome: 'desc_receita' })
          this.nomeReceita = '';
          this.nomeIns = '';
          this.completeNome = false;
          this.zerarVariaveis();
        } else if (res.json().status == 100) {
          swal("Atenção!", "Receita já existente.", "warning")
          this.zerarVariaveis();
        } else {
          swal("Erro!", "Falha ao cadastrar receita.", "error")
          this.zerarVariaveis();
        }
        this.zerarVariaveis();
      });
      this.zerarVariaveis();
    }
  }

  zerarVariaveis() {
    this.criador = null;
    this.aprovado = null;
    this.dataInicio = null;
    this.ativo = null;
    this.nomeIns = null;
  }

  // Recebe os dados e realiza o cadastro do freezer
  cadastrarFreezer() {
    let tempMin = $("#tempMinCad").val();
    let tempMax = $("#tempMaxCad").val();
    let data = {
      'tempMin_freezer': tempMin,
      'tempMax_freezer': tempMax
    }
    if (tempMin == undefined || tempMin == '' || tempMax == undefined || tempMax == '') {
      swal("Atenção!", "Preencha os campos corretamente.", "warning")
    } else {
      this.parametro = false;
      this.service.cadastrarFreezer(data).subscribe(res => {
        if (res.json().status == 200) {
          swal("Sucesso!", "Freezer cadastrado com sucesso.", "success")
          this.telas({ tela: 'freezer', nome: 'id_freezer' })
        } else {
          swal("Erro!", "Falha ao cadastrar freezer.", "error")
        }
      });
    }
  }

  // Recebe os dados e realiza a alteração do insumo
  editarInsumo(id) {
    let unidade = $("#unidadeInsumo").val();
    let qtdeMin = $("#qtdeMinEdit").val();
    let qtdeMax = $("#qtdeMaxEdit").val();
    //
    // BLOQUEAR INPUT DE NOME
    //
    let data = {
      'id_insumo': id,
      'unidade_insumo': unidade,
      'qtdeMin_insumo': qtdeMin,
      'qtdeMax_insumo': qtdeMax
    }
    if (unidade == undefined || unidade == '' || qtdeMin == undefined || qtdeMin == '' || qtdeMin <= 0 ||
      qtdeMax == undefined || qtdeMax == '' || qtdeMax <= 0) {
      if (qtdeMin < 0 || qtdeMax <= 0) {
        swal("Atenção!", "Os valores não podem ser iguais ou inferiores a zero.", "warning")
      } else {
        swal("Atenção!", "Preencha os campos corretamente.", "warning")
      }
    } else {
      this.parametro = false;
      this.service.editarInsumo(data).subscribe(res => {
        if (res.json().status == 200) {
          swal("Sucesso!", "Insumo editado com sucesso.", "success");
          this.telas({ tela: 'insumo', nome: 'desc_insumo' })
        } else {
          swal("Erro!", "Falha ao editar insumo.", "error");
        }
      });
    }
  }

  // Recebe os dados e realiza a alteração da receita
  editarReceita(id) {
    let nome = $("#nomeReceita").val();
    let rendimento = $("#rendimentoReceita").val();
    let tempoPreparo = $("#tempoPreparoReceita").val();
    let criador = $("#criadorReceita").val();
    let aprovado = $("#aprovadoReceita").val();
    let dataIni = $("#dataIniReceita").val();
    let ativo = $("#ativoReceita option:selected").text();

    let custoParc = $("#custoProducao").val().split('R$');
    let custo = custoParc[1].split(',')[0] + '.' + custoParc[1].split(',')[1];

    let ingredientes = JSON.stringify(this.ingredientes);
    let preparo = $("#modoPreparo").val();
    let data = {
      'id_receita': id,
      'desc_receita': nome,
      'rendimento': rendimento,
      'tempoPreparo_receita': tempoPreparo,
      'custo': parseFloat(custo),
      'ingredientes': ingredientes,
      'modoPreparo_receita': preparo,
      'criador': criador,
      'aprovado': aprovado,
      'dataIni': dataIni,
      'ativo': ativo
    }
    if (criador == '' || criador == undefined || aprovado == '' || aprovado == undefined ||
      dataIni == '' || dataIni == undefined || ativo == '' || ativo == undefined ||
      nome == '' || nome == undefined || rendimento == '' || rendimento == undefined || rendimento <= 0 || tempoPreparo == '' || tempoPreparo == undefined || tempoPreparo <= 0 ||
      custo == '' || custo == undefined || ingredientes == '[]' || ingredientes == undefined || preparo == '' || preparo == undefined //||this.completeNome == false
    ) {
      if (rendimento <= 0 || tempoPreparo <= 0) {
        swal("Atenção!", "Os valores não podem ser iguais ou inferiores a zero.", "warning")
      } else {
        swal("Atenção!", "Preencha todos os campos corretamente.", "warning")
      }
    } else {
      this.parametro = false;
      this.service.editarReceita(data).subscribe(res => {
        if (res.json().status == 200) {
          swal("Sucesso!", "Receita editada com sucesso.", "success")
          this.nomeReceita = '';
          this.nomeIns = '';
          this.completeNome = false;
          this.telas({ tela: 'receita', nome: 'desc_receita' })
        } else {
          swal("Erro!", "Falha ao editar receita.", "error")
        }
      });

    }
  }

  // Recebe os dados e realiza a alteração do freezer
  editarFreezer(id) {
    let tempMin = $("#tempMinEdit").val();
    let tempMax = $("#tempMaxEdit").val();
    let data = {
      'id_freezer': id,
      'tempMin_freezer': tempMin,
      'tempMax_freezer': tempMax
    }
    if (tempMin == undefined || tempMin == '' || tempMax == undefined || tempMax == '') {
      swal("Atenção!", "Preencha os campos corretamente.", "warning")
    } else {
      this.parametro = false;
      this.textDialog = 'Freezer Editado'
      this.service.editarFreezer(data).subscribe(res => {
        if (res.json().status == 200) {
          swal("Sucesso!", "Freezer editado com sucesso.", "success")
          this.telas({ tela: 'freezer', nome: 'id_freezer' })
        } else {
          swal("Erro!", "Falha ao editar freezer.", "error")
          this.telas({ tela: 'freezer', nome: 'id_freezer' })
        }
      });
    }

  }

  // Recebe os dados e realiza a alteração do produto final
  editarProdutoFinal(id) {
    let nome = $("#nomeCadProdEdit").val();
    let codigo_barras = $("#alterarCodBarras").val();
    let imagem = this.imgEscolhida[0].image;
    let valorPromoParc = $("#valorPromocionalEdit").val().split('R$');
    let valorPromo = valorPromoParc[1].split(',')[0] + '.' + valorPromoParc[1].split(',')[1];
    let valorParc = $("#valorProdutoEdit").val().split('R$');
    let valor = valorParc[1].split(',')[0] + '.' + valorParc[1].split(',')[1];
    let qtdeMin = $("#qtdeMinimaEdit").val();
    let qtdeMax = $("#qtdeMaxEdit").val();
    let unidade = (<HTMLInputElement>document.getElementById("unidadeCad")).value;
    let tipo = (<HTMLInputElement>document.getElementById("tipoCad")).value;
    let status = (<HTMLInputElement>document.getElementById("statusCad")).value;

    let data = {
      'id_produtoFinal': id,
      'desc_produtoFinal': nome,
      'img_produtoFinal': imagem,
      'valor_produtoFinal': parseFloat(valor),
      'valorDesc_produtoFinal': parseFloat(valorPromo),
      'qtdeMin_produtoFinal': qtdeMin,
      'qtdeMax_produtoFinal': qtdeMax,
      'unidadeVenda_produtoFinal': unidade,
      'tipo_produto': tipo,
      'status_produtoFinal': status,
      'nomeAntigo': this.nomeProdutoAntigo,
      'codigo_barras': codigo_barras
    };
    if (nome == undefined || nome == '' || imagem == undefined || imagem == '' || valor == undefined || valor == '' || valorPromo == undefined || valorPromo == '' ||
      qtdeMin == undefined || qtdeMin == '' || qtdeMin < 0 || qtdeMax == undefined || qtdeMax == '' || qtdeMax <= 0) {
      if (qtdeMin < 0 || qtdeMax <= 0) {
        swal("Atenção!", "Os valores não podem ser iguais ou inferiores a zero.", "warning")
      } else {
        swal("Atenção!", "Preencha os campos corretamente.", "warning")
      }
    } else {
      if (valorPromo >= valor) {
        swal("Atenção!", "Valor Promocional tem que ser menor que o valor.", "warning")
      } else {
        this.parametro = false;
        this.service.editarProduto(data).subscribe(res => {
          if (res.json().status == 200) {
            swal("Sucesso!", "Produto final alterado com sucesso.", "success")
            this.telas({ tela: 'produto_final', nome: 'desc_produtoFinal' })
          } else {
            swal("Erro!", "Falha ao alterar produto final.", "error")
          }
        });

      }
    }
  }

  confirmation(value) {
    this.cadastro = value;
  }

  // Adiciona um insumo no cadastro da receita
  adicionarInsumo() {
    let nome = $("#nomeIns").val().split('(')[0];
    let qtde = $("#qtdeIns").val();
    if (nome == undefined || nome == '' || qtde == undefined || qtde == '' || qtde <= 0) {
      if (qtde <= 0) {
        this.textDialog = 'Os valores não podem ser iguais ou inferiores a zero!'
        this.confirmation('Insumo Adicionado')
      } else {
        this.textDialog = 'Preencha os campos corretamente!'
        this.confirmation('Insumo Adicionado')
      }
    } else {
      this.ingredientes.push({ 'nome': nome, 'qtde': qtde, 'unidade': $("#nomeIns").val().split('(')[1].split(')')[0] });
      $("#nomeIns").val("");
      $("#qtdeIns").val("");
    }
  }

  excluirIngredientes(index) {
    this.ingredientes.splice(index, 1);
  }
  adicionarInsumoEdicao() {
    let nome = $("#nomeIns").val().split('(')[0];
    let qtde = $("#qtdeIns").val();

    if (qtde <= 0) {
      swal("Atenção!", "Os valores não podem ser iguais ou inferiores a zero.", "warning")
    } else if (nome == undefined || nome == '' || qtde == undefined || qtde == '') {
      swal("Atenção!", "Preencha os campos corretamente.", "warning")
    } else {
      this.ingredientes.push({ 'nome': nome, 'qtde': qtde, 'unidade': $("#nomeIns").val().split('(')[1].split(')')[0] });
      $("#nomeIns").val("");
      $("#qtdeIns").val("");
    }
  }

  editarIngredientes(index) {
    this.ingredientes.splice(index, 1);
  }

  // Abre o input para realizar o filtro no canto inferior direito
  openInputSearch(name) {
    $("#inputSearch" + name).css('width', '');
    if ($("#inputSearch" + name).hasClass("openSearch")) {
      $("#inputSearch" + name).removeClass("openSearch");
      $("#inputSearch" + name).addClass("closeSearch");
    } else {
      $("#inputSearch" + name).removeClass("closeSearch");
      $("#inputSearch" + name).addClass("openSearch");
      $(`#${name}`).focus();

    }
  }

  // Filtros de Produto, Insumo, Receita e Freezer
  bottomFilterProduto(input) {
    var regexp = new RegExp((".*" + input.split(" ").join('.*') + ".*"), "i");
    this.resultadosFinais = this.resultados.filter((r) => {
      return regexp.test(r.desc_produtoFinal);
    });
  }

  bottomFilterInsumo(input) {
    var regexp = new RegExp((".*" + input.split(" ").join('.*') + ".*"), "i");
    this.resultadosFinais = this.resultados.filter((r) => {
      return regexp.test(r.desc_insumo);
    });
  }

  bottomFilterReceita(input) {
    var regexp = new RegExp((".*" + input.split(" ").join('.*') + ".*"), "i");
    this.resultadosFinais = this.resultados.filter((r) => {
      return regexp.test(r.desc_receita);
    });
  }

  bottomFilterFreezer(input) {
    var regexp = new RegExp((".*" + input.split("").join('.*') + ".*"), "i");
    this.resultadosFinais = this.resultados.filter((r) => {
      return regexp.test(r.id_freezer);
    });
  }

  filterInsumos(input) {
    if (input) {
      this.filtroStatus = true;
    } else {
      this.filtroStatus = false;
    }
    var regexp = new RegExp((".*" + input.split("").join('.*') + ".*"), "i");
    this.insumosFiltrados = this.insumosExistentes.filter((r) => {
      return regexp.test(r.desc_insumo);
    });
  }

  completar(index) {
    let insumoEscolhido = this.insumosFiltrados[index];
    $("#nomeIns").val(insumoEscolhido.desc_insumo + " (" + insumoEscolhido.unidade_insumo + ")");
    this.filtroStatus = false;
  }
  // Serviço de filtro AutoComplete, basta passar os parâmetros
  resultReceita = [];
  filterReceita(model) {
    this.resultReceita = this.filter.filterData(model, this.todosProdutos, `produto`)
  }

  closeComplete() {
    setTimeout(() => {
      this.resultReceita = [];
    }, 200)
  }

}
