import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Router,RouterModule, Routes} from '@angular/router';

import { Http, Headers, RequestOptions } from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  
  listaLogin = [];
  
  constructor(private router : Router) {
 }

  redi(){
    this.router.navigate(['login'])
  }

  ngOnInit() {
    if (!localStorage.getItem('session')) {
			this.router.navigate(['loading'])
			setTimeout(() => this.redi(), 2000)
		} else {
			this.router.navigate(['main'])
		}
  }
}

@Component({
  selector: 'app-topBar',
  template: `
  <div [id]="idDiv" style="position: absolute; height: 50%; width: 100%; top: 50%; background: #2d2c2c" >
    <button *ngIf="widthButtonA" class="mdl-button mdl-js-button mdl-js-ripple-effect" [id]="idButtonA" (click)="openTelaA()" [style.width]="widthButtonA+'%'" style="height: 100%">
      <span class="descChar mdl-color-text--white"> {{nomeBotaoA}} </span>
    </button>
    <button *ngIf="widthButtonB" class="mdl-button mdl-js-button mdl-js-ripple-effect" [id]="idButtonB" (click)="openTelaB()" [style.width]="widthButtonB+'%'" style="height: 100%">
      <span class="descChar mdl-color-text--white"> {{nomeBotaoB}} </span>
    </button>
    <button *ngIf="widthButtonC" class="mdl-button mdl-js-button mdl-js-ripple-effect" [id]="idButtonC" (click)="openTelaC()" [style.width]="widthButtonC+'%'" style="height: 100%">
     <span class="descChar mdl-color-text--white"> {{nomeBotaoC}} </span>
    </button>
    <button *ngIf="widthButtonD" class="mdl-button mdl-js-button mdl-js-ripple-effect" [id]="idButtonD" (click)="openTelaD()" [style.width]="widthButtonD+'%'" style="height: 100%">
      <span class="descChar mdl-color-text--white"> {{nomeBotaoD}} </span>
    </button>
  </div>
  `,
  styleUrls: ['./app.component.css']
    
  })
  export class AppTopBar{
    constructor(){} 
    @Input() idDiv: string;
    @Input() idButtonA : string;
    @Input() idButtonB : string;
    @Input() idButtonC : string;
    @Input() idButtonD : string;
    @Input() nomeBotaoA: string;
    @Input() nomeBotaoB: string;
    @Input() nomeBotaoC: string;
    @Input() nomeBotaoD: string;
    @Input() widthButtonA: number;
    @Input() widthButtonB: number;
    @Input() widthButtonC: number;
    @Input() widthButtonD: number;
    @Input() telaA: string;
    @Input() telaB: string;
    @Input() telaC: string;
    @Input() telaD: string;
    @Input() nameTelaA: string;
    @Input() nameTelaB: string;
    @Input() nameTelaC: string;
    @Input() nameTelaD: string;
    @Output() handler: EventEmitter<any> = new EventEmitter() ;
  
  openTelaA(){
    this.handler.emit({ tela: this.telaA, nome: this.nameTelaA});
  }
  openTelaB(){
    this.handler.emit({ tela: this.telaB, nome: this.nameTelaB});
  }
  openTelaC(){
    this.handler.emit({ tela: this.telaC, nome: this.nameTelaC});
  }
  openTelaD(){
    this.handler.emit({ tela: this.telaD, nome: this.nameTelaD});
  }
  
  }

@Component({
  selector: 'app-tableHead',
  template: `
  <div id="head" style="position: absolute; width:100%; height:40px; border-bottom: 1px solid #ccc">
     <div [style.width]="widthA+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float:left">
      <span style="position: relative; top: 10px">{{nomeCampoA}}</span>
    </div>
    <div [style.width]="widthB+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float: left">
      <span style="position: relative; top: 10px">{{nomeCampoB}}</span>
    </div>
    <div [style.width]="widthC+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float: left">
      <span style="position: relative; top: 10px">{{nomeCampoC}}</span>
    </div>
    <div [style.width]="widthD+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float: left">
      <span style="position: relative; top: 10px">{{nomeCampoD}}</span>
    </div>
    <div [style.width]="widthE+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float: left">
    <span style="position: relative; top: 10px">{{nomeCampoE}}</span>
    </div>
    <div [style.width]="widthF+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float: left">
     <span style="position: relative; top: 10px">{{nomeCampoF}}</span>
    </div>
    <div [style.width]="widthG+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float: left">
      <span style="position: relative; top: 10px">{{nomeCampoG}}</span>
    </div>
    <div [style.width]="widthH+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float: left">
      <span style="position: relative; top: 10px">{{nomeCampoH}}</span>
    </div>
    <div [style.width]="widthI+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float: left">
     <span style="position: relative; top: 10px">{{nomeCampoI}}</span>
    </div>
  </div>
  `,
  styleUrls: ['./app.component.css']

})

export class AppTableHead{
  constructor(){}
  @Input() nomeCampoA: string;
  @Input() nomeCampoB: string;
  @Input() nomeCampoC: string;
  @Input() nomeCampoD: string;
  @Input() nomeCampoE: string;
  @Input() nomeCampoF: string;
  @Input() nomeCampoG: string;
  @Input() nomeCampoH: string;
  @Input() nomeCampoI: string;
  @Input() widthA: number;
  @Input() widthB: number;
  @Input() widthC: number;
  @Input() widthD: number;
  @Input() widthE: number;
  @Input() widthF: number;
  @Input() widthG: number;
  @Input() widthH: number;
  @Input() widthI: number;
 
}

@Component({
  selector: 'app-tableBody',
  template: `
  <div class="rowHover tableRow" style="width: 100%; height: 40px; float:left; border-bottom: 1px solid #ccc">
  
    <div [style.width]="widthA+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float:left">
      <span style="margin: 10px auto; display: table">{{nomeCampoA}}</span>
    </div>
    
    <div [style.width]="widthB+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float:left">
      <span style="margin: 10px auto; display: table">{{nomeCampoB}}</span>
    </div>

    <div [style.width]="widthC+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float:left">
      <span style="margin: 10px auto; display: table">{{nomeCampoC}}</span>
    </div>

    <div [style.width]="widthD+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float:left">
     <span style="margin: 10px auto; display: table">{{nomeCampoD}}</span>
    </div>

    <div [style.width]="widthE+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float:left">
      <span style="margin: 10px auto; display: table">{{nomeCampoE}}</span>
    </div>

    <div [style.width]="widthF+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float:left">
      <span style="margin: 10px auto; display: table">{{nomeCampoF}}</span>
    </div>

    <div [style.width]="widthG+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float:left">
      <span style="margin: 10px auto; display: table">{{nomeCampoG}}</span>
    </div>
    
    <div *ngIf="widthEditButton" [style.width]="widthEditButton+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float:left">
      <span style="margin: 6px auto; display: table"><button id="btEdit-{{$index}}" class="mdl-button mdl-js-button mdl-button--icon" style="position: relative"(click)="openEdit()"><i class="material-icons">mode_edit</i></button></span>
    </div>

    <div *ngIf="widthViewButton" [style.width]="widthViewButton+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float:left">
      <span style="margin: 6px auto; display: table"><button id="btView-{{$index}}" class="mdl-button mdl-js-button mdl-button--icon" style="position: relative" (click)="openView()"><i class="material-icons">remove_red_eye</i></button></span>
    </div>

    <div *ngIf="widthExcludeButton" [style.width]="widthExcludeButton+'%'" style="height: 100%; text-align: center; display: table; margin: 0% auto; float:left">
      <span style="margin: 6px auto; display: table"><button id="btExclude-{{$index}}" class="mdl-button mdl-js-button mdl-button--icon" style="position: relative" (click)="openExclude()"><i class="material-icons">delete_forever</i></button></span>
    </div>

  </div>
  `,
  styleUrls: ['./app.component.css']

})
export class AppTableBody{
  constructor(){}
  @Input() id: number;
  @Input() nomeCampoA: string;
  @Input() nomeCampoB: string;
  @Input() nomeCampoC: string;
  @Input() nomeCampoD: string;
  @Input() nomeCampoE: string;
  @Input() nomeCampoF: string;
  @Input() nomeCampoG: string;
  @Input() widthA: number;
  @Input() widthB: number;
  @Input() widthC: number;
  @Input() widthD: number;
  @Input() widthE: number;
  @Input() widthF: number;
  @Input() widthG: number;
  @Input() widthEditButton: number;
  @Input() widthViewButton: number;
  @Input() widthExcludeButton: number;
  @Input() eventoEdit: string;
  @Input() eventoView: string;
  @Input() eventoExclude: string;
  @Output() handler: EventEmitter<any> = new EventEmitter();
  
  openEdit(){
    this.handler.emit(this.eventoEdit+''+this.id);
  }
  openView(){
    this.handler.emit(this.eventoView+''+this.id);
  }
  openExclude(){
    this.handler.emit(this.eventoExclude+''+this.id);
  }
}

@Component({
selector: 'app-bottomBar',
template: `
  <div id="bottomBar" style="height: 6%; position: absolute; bottom: 0px; width: 100%; background: #2d2c2c">
    <button class="mdl-button mdl-js-button mdl-js-ripple-effect" style="width: 45%; height: 100%; float: left" (click)="openBottomBar()">
      <i class="material-icons" style="color: white">add</i>
      <span class="descChar mdl-color-text--white" style="border-left:45%"> {{nomeBotao}} </span>
    </button>

    <div class="mdl-textfield mdl-js-textfield" style="width:55%; height: 100%; float:left">
      <input id="ipSearch" type="text" class="mdl-textfield__input mdl-shadow--8dp" style="width: 0%;height: 50%;margin-left: 39%;border-radius: 10px;">

      <button [id]="'btSearch'+id" class="mdl-button mdl-js-button mdl-button--icon" style="position: absolute;bottom: 0%;right: 5%;height: 100%;width: 50px;" (click)="openSearchInput()">
        <i class="material-icons" style="color: #ffffff; font-size: 30px">search</i>
      </button>
    </div>	

  </div>
`,
styleUrls: ['./app.component.css']

})
export class AppBottomBar{
  constructor(){}

  @Output() handler: EventEmitter<any> = new EventEmitter();
  @Output() handlerB: EventEmitter<any> = new EventEmitter();
  @Input() id: string;
  @Input() nomeBotao: string;
  @Input() evento: string;

openBottomBar(){
  this.handler.emit(this.evento);
}
openSearchInput(){
  this.handlerB.emit();
}

}

@Component({
  selector: 'text-dialog',
  template:`
  <div [id]="id" style="position: fixed;top: 0%;height: 100%;width: 100%;left: 0px;background: rgba(15,20,22,0.3); z-index: 2" *ngIf="divWidth && divHeight">
    <div [id]="'textDialog'+id" [style.width]="divWidth+'%'" [style.height]="divHeight+'%'" style="margin: 20% auto; position: relative; border-radius: 10px; background-color: #ffffff; text-align: center" class="mdl-shadow--8dp">
      <div id="tituloProduto" style="position: relative; width: 100%; height: 22%; text-align: center; background-color: #2d2c2c; border-top-left-radius: 10px; border-top-right-radius: 10px">
       <span style="position: relative; font-weight: bold; font-family: 'roboto', sans-serif; font-size: 25px; color: #ffffff; top: 8px">Atenção</span>
      </div>
      <span [style.left]="leftText+'%'" [style.top]="topText+'%'" [style.font-size]="fontSize+'px'" style="margin: 10% auto; display: table">{{text}}</span>
      <button *ngIf="icon" class="mdl-button mdl-js-button mdl-button--icon" [style.left]="leftButton+'%'" [style.height]="heightButton+'%'" [style.width]="widthButton+'%'" [style.bottom]="bottomButton+'%'" style="position: absolute; border-radius: 10px" (click)="close()"> <i class="material-icons">{{icon}}</i> </button>
    </div>
  </div>
  `,
  styleUrls: ['./app.component.css']
})
export class TextDialog{
  constructor(){}
  @Input() id : string;
  @Input() divWidth: number;
  @Input() divHeight: number;
  @Input() leftText: number;
  @Input() topText: number;
  @Input() fontSize: number;
  @Input() text : string;
  @Input() icon: string;
  @Input() leftButton: number;
  @Input() heightButton: number;
  @Input() widthButton: number;
  @Input() bottomButton: number;
  @Output() handler: EventEmitter<any> = new EventEmitter();

  close(){
    this.handler.emit();
  }
} 


@Component({
  selector: 'pattern-dialog',
  template: `
  <div [id]="id" style="position: fixed;top: 0px;left: 0px; width: 100%; height: 100%;background: rgba(15,20,22,0.3);" *ngIf="divWidth && divHeight">
    <div [style.width]="divWidth+'%'" [style.height]="divHeight+'%'" [style.left]="(100-divWidth)/2+'%'" [style.top]="(100-divHeight)/2+'%'" style="position: absolute; border-radius: 10px; background-color: #ffffff; display: table" class="mdl-shadow--8dp">
    
      <div id="tituloProduto" style="position: absolute; width: 100%; min-height: 30px; text-align: center; background-color: #2d2c2c; border-top-right-radius: 10px; border-top-left-radius: 10px">
        <span style="position: relative; font-weight: bold; font-family: 'roboto', sans-serif; font-size: 20px; color: #ffffff; top: 8px">{{titulo}}</span>
      </div>

      <span *ngIf="spanA" [style.left]="leftSpanA+'%'" [style.top]="topSpanA+'%'" [style.font-size]="sizeSpanA+'px'" style="position: absolute; font-weight: bold">{{spanA}}</span>
      <input [type]="typeA" [id]="tituloA+id" type="text" class="inputDialog" *ngIf="placeholderA" [style.width]="widthA+'%'" [style.left]="leftA+'%'" [style.top]="topA+'%'" style="outline: none; position: absolute; text-align:center; float: left" [placeholder]="placeholderA">
      <input [type]="typeA" [id]="tituloA+id" type="text" class="inputDialog" *ngIf="valueA" [style.width]="widthA+'%'" [style.left]="leftA+'%'" [style.top]="topA+'%'" style="outline: none; position: absolute; text-align:center; float: left" [value]="valueA">
      <input [type]="typeA" [id]="tituloA+id" type="text" class="inputDialog" *ngIf="valueA && disabled" [style.width]="widthA+'%'" [style.left]="leftA+'%'" [style.top]="topA+'%'" style="outline: none; position: absolute; text-align:center; float: left; background: initial" [value]="valueA" disabled>
      
      <span *ngIf="spanB" [style.left]="leftSpanB+'%'" [style.top]="topSpanB+'%'" [style.font-size]="sizeSpanB+'px'" style="position: absolute; font-weight: bold">{{spanB}}</span>
      <input [type]="typeB" [id]="tituloB+id" type="text" class="inputDialog" *ngIf="placeholderB" [style.width]="widthB+'%'" [style.left]="leftB+'%'" [style.top]="topB+'%'" style="outline: none; position: absolute; text-align:center; float: left" [placeholder]="placeholderB">
      <input [type]="typeB" [id]="tituloB+id" type="text" class="inputDialog" *ngIf="valueB" [style.width]="widthB+'%'" [style.left]="leftB+'%'" [style.top]="topB+'%'" style="outline: none; position: absolute; text-align:center; float: left" [value]="valueB">
      <input [type]="typeB" [id]="tituloB+id" type="text" class="inputDialog" *ngIf="valueB && disabledB" [style.width]="widthB+'%'" [style.left]="leftB+'%'" [style.top]="topB+'%'" style="outline: none; position: absolute; text-align:center; float: left; background: initial" [value]="valueB" disabled>
      
      <span *ngIf="spanC" [style.left]="leftSpanC+'%'" [style.top]="topSpanC+'%'" [style.font-size]="sizeSpanC+'px'" style="position: absolute; font-weight: bold">{{spanC}}</span>
      <input [type]="typeC" [id]="tituloC+id" type="text" class="inputDialog" *ngIf="placeholderC" [style.width]="widthC+'%'" [style.left]="leftC+'%'" [style.top]="topC+'%'" style="outline: none; position: absolute; text-align:center; float: left" [placeholder]="placeholderC">
      <input [type]="typeC" [id]="tituloC+id" type="text" class="inputDialog" *ngIf="valueC" [style.width]="widthC+'%'" [style.left]="leftC+'%'" [style.top]="topC+'%'" style="outline: none; position: absolute; text-align:center; float: left" [value]="valueC">
      <input [type]="typeC" [id]="tituloC+id" type="text" class="inputDialog" *ngIf="valueC && disabledC" [style.width]="widthC+'%'" [style.left]="leftC+'%'" [style.top]="topC+'%'" style="outline: none; position: absolute; text-align:center; float: left; background: initial" [value]="valueC" disabled>
      
      <span *ngIf="spanD" [style.left]="leftSpanD+'%'" [style.top]="topSpanD+'%'" [style.font-size]="sizeSpanD+'px'" style="position: absolute; font-weight: bold">{{spanD}}</span>
      <input [type]="typeD" [id]="tituloD+id" type="text" class="inputDialog" *ngIf="placeholderD" [style.width]="widthD+'%'" [style.left]="leftD+'%'" [style.top]="topD+'%'" style="outline: none; position: absolute; text-align:center; float: left" [placeholder]="placeholderD">
      <input [type]="typeD" [id]="tituloD+id" type="text" class="inputDialog" *ngIf="valueD" [style.width]="widthD+'%'" [style.left]="leftD+'%'" [style.top]="topD+'%'" style="outline: none; position: absolute; text-align:center; float: left" [value]="valueD">
      <input [type]="typeD" [id]="tituloD+id" type="text" class="inputDialog" *ngIf="valueD && disabledD" [style.width]="widthD+'%'" [style.left]="leftD+'%'" [style.top]="topD+'%'" style="outline: none; position: absolute; text-align:center; float: left; background: initial" [value]="valueD" disabled>
      
      <span *ngIf="spanE" [style.left]="leftSpanE+'%'" [style.top]="topSpanE+'%'" [style.font-size]="sizeSpanE+'px'" style="position: absolute; font-weight: bold">{{spanE}}</span>
      <input [type]="typeE" [id]="tituloE+id" type="text" class="inputDialog" *ngIf="placeholderE" [style.width]="widthE+'%'" [style.left]="leftE+'%'" [style.top]="topE+'%'" style="outline: none; position: absolute; text-align:center; float: left" [placeholder]="placeholderE">
      <input [type]="typeE" [id]="tituloE+id" type="text" class="inputDialog" *ngIf="valueE" [style.width]="widthE+'%'" [style.left]="leftE+'%'" [style.top]="topE+'%'" style="outline: none; position: absolute; text-align:center; float: left" [value]="valueE">
      <input [type]="typeE" [id]="tituloE+id" type="text" class="inputDialog" *ngIf="valueE && disabledE" [style.width]="widthE+'%'" [style.left]="leftE+'%'" [style.top]="topE+'%'" style="outline: none; position: absolute; text-align:center; float: left; background: initial" [value]="valueE" disabled>
      
      <span *ngIf="spanF" [style.left]="leftSpanF+'%'" [style.top]="topSpanF+'%'" [style.font-size]="sizeSpanF+'px'" style="position: absolute; font-weight: bold">{{spanF}}</span>
      <input [type]="typeF" [id]="tituloF+id" type="text" class="inputDialog" *ngIf="placeholderF" [style.width]="widthF+'%'" [style.left]="leftF+'%'" [style.top]="topF+'%'" style="outline: none; position: absolute; text-align:center; float: left" [placeholder]="placeholderF">
      <input [type]="typeF" [id]="tituloF+id" type="text" class="inputDialog" *ngIf="valueF" [style.width]="widthF+'%'" [style.left]="leftF+'%'" [style.top]="topF+'%'" style="outline: none; position: absolute; text-align:center; float: left" [value]="valueF">
      <input [type]="typeF" [id]="tituloF+id" type="text" class="inputDialog" *ngIf="valueF && disabledF" [style.width]="widthF+'%'" [style.left]="leftF+'%'" [style.top]="topF+'%'" style="outline: none; position: absolute; text-align:center; float: left; background: initial" [value]="valueF" disabled>
      
      <span *ngIf="spanG" [style.left]="leftSpanG+'%'" [style.top]="topSpanG+'%'" [style.font-size]="sizeSpanG+'px'" style="position: absolute; font-weight: bold">{{spanG}}</span>
      <input [type]="typeG" [id]="tituloG+id" type="text" class="inputDialog" *ngIf="placeholderG" [style.width]="widthG+'%'" [style.left]="leftG+'%'" [style.top]="topG+'%'" style="outline: none; position: absolute; text-align:center; float: left" [placeholder]="placeholderG">
      <input [type]="typeG" [id]="tituloG+id" type="text" class="inputDialog" *ngIf="valueG" [style.width]="widthG+'%'" [style.left]="leftG+'%'" [style.top]="topG+'%'" style="outline: none; position: absolute; text-align:center; float: left" [value]="valueG">
      <input [type]="typeG" [id]="tituloG+id" type="text" class="inputDialog" *ngIf="valueG && disabledG" [style.width]="widthG+'%'" [style.left]="leftG+'%'" [style.top]="topG+'%'" style="outline: none; position: absolute; text-align:center; float: left; background: initial" [value]="valueG" disabled>
      
      <span *ngIf="spanH" [style.left]="leftSpanH+'%'" [style.top]="topSpanH+'%'" [style.font-size]="sizeSpanH+'px'" style="position: absolute; font-weight: bold">{{spanH}}</span>
      <input [type]="typeH" [id]="tituloH+id" type="text" class="inputDialog" *ngIf="placeholderH" [style.width]="widthH+'%'" [style.left]="leftH+'%'" [style.top]="topH+'%'" style="outline: none; position: absolute; text-align:center; float: left" [placeholder]="placeholderH">
      <input [type]="typeH" [id]="tituloH+id" type="text" class="inputDialog" *ngIf="valueH" [style.width]="widthH+'%'" [style.left]="leftH+'%'" [style.top]="topH+'%'" style="outline: none; position: absolute; text-align:center; float: left" [value]="valueH">
      <input [type]="typeH" [id]="tituloH+id" type="text" class="inputDialog" *ngIf="valueH && disabledH" [style.width]="widthH+'%'" [style.left]="leftH+'%'" [style.top]="topH+'%'" style="outline: none; position: absolute; text-align:center; float: left; background: initial" [value]="valueH" disabled>
      
      <span *ngIf="spanI" [style.left]="leftSpanI+'%'" [style.top]="topSpanI+'%'" [style.font-size]="sizeSpanI+'px'" style="position: absolute; font-weight: bold">{{spanI}}</span>
      <input [type]="typeI" [id]="tituloI+id" type="text" class="inputDialog" *ngIf="placeholderI" [style.width]="widthI+'%'" [style.left]="leftI+'%'" [style.top]="topI+'%'" style="outline: none; position: absolute; text-align:center; float: left" [placeholder]="placeholderI">
      <input [type]="typeI" [id]="tituloI+id" type="text" class="inputDialog" *ngIf="valueI" [style.width]="widthI+'%'" [style.left]="leftI+'%'" [style.top]="topI+'%'" style="outline: none; position: absolute; text-align:center; float: left" [value]="valueI">
      <input [type]="typeI" [id]="tituloI+id" type="text" class="inputDialog" *ngIf="valueI && disabledI" [style.width]="widthI+'%'" [style.left]="leftI+'%'" [style.top]="topI+'%'" style="outline: none; position: absolute; text-align:center; float: left; background: initial" [value]="valueI" disabled>
      
      <select *ngIf="nameComboA" [name]="nameComboA"> <option *ngIf="valueComboAOptionA" [value]="valueComboAOptionA"><span>{{ComboAOptionA}}</span></option></select>

      <div *ngIf="nameComboB" style="position: absolute; font-weight: bold; top: 18%; width: 40%;right: 5%; text-align: center; font-size: 15px"> Unidade de Venda </div>
      <select *ngIf="nameComboB" name="nameComboB" id="unidadeInsumo" [value]="valueCombo" class='select' style='position: absolute;top: 30%;width: 40%;right: 5%;'> 
        <option  value="Un"><span> Unidade </span></option>
        <option  value="Kg"><span> kilograma </span></option>
        <option  value="Gr"><span> Gramas </span></option>
        <option  value="Lt"><span> Litro </span></option>
        <option  value="Ml"><span> Mililitro </span></option>
        <option  value="Hr/Energia"><span> Hora/Energia </span></option>
        <option  value="Cons/Agua"><span> Consumo/Água </span></option>
        <option  value="H/H"><span> Hora/Homem </span></option>
      </select>

      <button *ngIf="nomeBotaoA" class="mdl-button mdl-js-button mdl-js-ripple-effect" [style.left]="leftButtonA+'%'" [style.right]="rightButtonA+'%'" [style.height]="heightButtons+'%'" [style.width]="widthButtonA+'%'" [style.bottom]="bottomButtons+'%'" style="position: absolute; border-radius: 10px" (click)="closeCadastro()"><span> {{nomeBotaoA}} </span></button>
      <button *ngIf="nomeBotaoB" class="mdl-button mdl-js-button mdl-js-ripple-effect" [style.left]="leftButtonB+'%'" [style.right]="rightButtonB+'%'" [style.height]="heightButtons+'%'" [style.width]="widthButtonA+'%'" [style.bottom]="bottomButtons+'%'" style="position: absolute; border-radius: 10px" (click)="funcao()"><span> {{nomeBotaoB}} </span></button>
      <button *ngIf="nomeBotaoC" class="mdl-button mdl-js-button mdl-js-ripple-effect" [style.left]="leftButtonC+'%'" [style.right]="rightButtonC+'%'" [style.height]="heightButtons+'%'" [style.width]="widthButtonA+'%'" [style.bottom]="bottomButtons+'%'" style="position: absolute; border-radius: 10px"><span> {{nomeBotaoC}} </span></button>
      
    </div>
</div>

  `,
  styleUrls: ['./app.component.css']

})
export class PatternDialog{
  constructor(){}
  @Input() disabled: boolean;
  @Input() disabledB: boolean;
  @Input() disabledC: boolean;
  @Input() disabledD: boolean;
  @Input() disabledE: boolean;
  @Input() disabledF: boolean;
  @Input() disabledG: boolean;
  @Input() disabledH: boolean;
  @Input() disabledI: boolean;
  @Input() id: string;
  @Input() divWidth: number;
  @Input() divHeight: number;
  @Input() margin: number;
  @Input() titulo: string;
  @Input() typeA: string;
  @Input() typeB: string;
  @Input() typeC: string;
  @Input() typeD: string;
  @Input() typeE: string;
  @Input() typeF: string;
  @Input() typeG: string;
  @Input() typeH: string;
  @Input() typeI: string;
  @Input() spanA: string;
  @Input() spanB: string;
  @Input() spanC: string; 
  @Input() spanD: string;
  @Input() spanE: string;
  @Input() spanF: string;
  @Input() spanG: string;
  @Input() spanH: string;
  @Input() spanI: string;
  @Input() topSpanA: number;
  @Input() topSpanB: number;
  @Input() topSpanC: number;
  @Input() topSpanD: number;
  @Input() topSpanE: number;
  @Input() topSpanF: number;
  @Input() topSpanG: number;
  @Input() topSpanH: number;
  @Input() topSpanI: number;
  @Input() leftSpanA: number;
  @Input() leftSpanB: number;
  @Input() leftSpanC: number;
  @Input() leftSpanD: number;
  @Input() leftSpanE: number;
  @Input() leftSpanF: number;
  @Input() leftSpanG: number;
  @Input() leftSpanH: number;
  @Input() leftSpanI: number; 
  @Input() sizeSpanA: number;
  @Input() sizeSpanB: number;
  @Input() sizeSpanC: number;
  @Input() sizeSpanD: number;
  @Input() sizeSpanE: number;
  @Input() sizeSpanF: number;
  @Input() sizeSpanG: number;
  @Input() sizeSpanH: number;
  @Input() sizeSpanI: number;
  @Input() widthA: number;
  @Input() widthB: number;
  @Input() widthC: number;
  @Input() widthD: number;
  @Input() widthE: number;
  @Input() widthF: number;
  @Input() widthG: number;
  @Input() widthH: number;
  @Input() widthI: number;
  @Input() topA: number;
  @Input() topB: number;
  @Input() topC: number;
  @Input() topD: number;
  @Input() topE: number;
  @Input() topF: number;
  @Input() topG: number;
  @Input() topH: number;
  @Input() topI: number;
  @Input() leftA: number;
  @Input() leftB: number;
  @Input() leftC: number;
  @Input() leftD: number;
  @Input() leftE: number;
  @Input() leftF: number;
  @Input() leftG: number;
  @Input() leftH: number;
  @Input() leftI: number;
  @Input() placeholderA: string;
  @Input() placeholderB: string;
  @Input() placeholderC: string;
  @Input() placeholderD: string;
  @Input() placeholderE: string;
  @Input() placeholderF: string;
  @Input() placeholderG: string;
  @Input() placeholderH: string;
  @Input() placeholderI: string;
  @Input() tituloA: string;
  @Input() tituloB: string;
  @Input() tituloC: string;
  @Input() tituloD: string;
  @Input() tituloE: string;
  @Input() tituloF: string;
  @Input() tituloG: string;
  @Input() tituloH: string;
  @Input() tituloI: string;
  @Input() valueA: string;
  @Input() valueB: string;
  @Input() valueC: string;
  @Input() valueD: string;
  @Input() valueE: string;
  @Input() valueF: string;
  @Input() valueG: string;
  @Input() valueH: string;
  @Input() valueI: string;
  @Input() leftButtonA: number;
  @Input() leftButtonB: number;  
  @Input() leftButtonC: number;
  @Input() rightButtonA: number;
  @Input() rightButtonB: number; 
  @Input() rightButtonC: number;
  @Input() widthButtonA: number;
  @Input() widthButtons: number;
  @Input() heightButtons: number;
  @Input() bottomButtons: number;
  @Input() nameComboA: string;
  @Input() nameComboB: string;
  @Input() valueCombo: string;
  @Input() valueComboAOptionA: string;
  @Input() ComboAOptionA: string;
  @Input() nomeBotaoA: string;
  @Input() nomeBotaoB: string;
  @Input() funcaoBotaoB: string;
  @Input() nomeBotaoC: string;
  @Output() keyUp: EventEmitter<any> = new EventEmitter();
  @Output() handler: EventEmitter<any> = new EventEmitter();
  @Output() handlerB: EventEmitter<any> = new EventEmitter();

  closeCadastro(){
    this.handler.emit('closeDialog');
  }

  funcao(){
    this.handlerB.emit();
  }

  filtro(){
    this.keyUp.emit();
  }
}