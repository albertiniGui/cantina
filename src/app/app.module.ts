import { FormsModule } from '@angular/forms';
import { NgClass, NgStyle } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component, Directive, LOCALE_ID } from '@angular/core';

import { HttpModule } from '@angular/http';

import { AppTopBar } from './app.component';
import { TextDialog } from './app.component';
import { AppComponent } from './app.component';
import { AppTableHead } from './app.component';
import { AppTableBody } from './app.component';
import { AppBottomBar } from './app.component';
import { PatternDialog } from './app.component';
import { AppInfo } from './components/main/main.component';
import { LoginComponent } from './components/login/login.component';
import { CaixaComponent } from './components/caixa/caixa.component';
import { ComandaComponent } from './components/comanda/comanda.component';
import { ComprasComponent } from './components/compras/compras.component';
import { LoadingComponent } from './components/loading/loading.component';
import { ControleComponent } from './components/controle/controle.component';
import { MainComponent, AppUsuario, AppCredito } from './components/main/main.component';
import { CadastroComponent } from './components/cadastro/cadastro.component';
import { RelatorioComponent } from './components/relatorio/relatorio.component';

import { Properties } from './class/properties';

import { MainService } from './services/main.service';
import { LoginService } from './services/login.service';
import { CaixaService } from './services/caixa.service';
import { ComprasService } from './services/compras.service';
import { ComandaService } from './services/comanda.service';
import { CadastroService } from './services/cadastro.service';
import { ControleService } from './services/controle.service';
import { RelatorioService } from './services/relatorio.service';


export const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'loading', component: LoadingComponent },
  {
    path: 'main', component: MainComponent,
    children: [
      { path: 'caixa', component: CaixaComponent },
      { path: 'compras', component: ComprasComponent },
      { path: 'comanda', component: ComandaComponent },
      { path: 'controle', component: ControleComponent },
      { path: 'cadastrar', component: CadastroComponent },
      { path: 'relatorios', component: RelatorioComponent }
    ]
  }
];

@Directive({ selector: '[ngStyle]' })

@Directive({ selector: '[ngClass]' })

@NgModule({
  declarations: [
    AppInfo,
    AppTopBar,
    AppUsuario,
    TextDialog,
    AppCredito,
    AppComponent,
    MainComponent,
    AppTableHead,
    AppTableBody,
    AppBottomBar,
    PatternDialog,
    LoginComponent,
    CaixaComponent,
    ComandaComponent,
    ComprasComponent,
    LoadingComponent,
    CadastroComponent,
    ControleComponent,
    RelatorioComponent
  ],
  imports: [
    HttpModule,
    FormsModule,
    BrowserModule,
    CurrencyMaskModule,
    RouterModule.forRoot(
      appRoutes,
      { useHash: true, initialNavigation: false }, // <-- debugging purposes only
    )
  ],
  providers: [
    MainService,
    LoginService,
    CaixaService,
    ComandaService,
    ComprasService,
    CadastroService,
    ControleService,
    RelatorioService,
    { provide: LOCALE_ID, useValue: 'pt-BR' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}

