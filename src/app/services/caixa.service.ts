import { Injectable } from '@angular/core';
import { Properties } from '../class/properties';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class CaixaService {

    constructor(private http: Http) { }

    // POSTS
    insertCredito(data) {
        return this.http.post(`${Properties.URL}/insertCredito`, data)
    }

    pagarConta(data) {
        return this.http.post(`${Properties.URL}/pagarConta`, data)
    }

    updateCredito(data) {
        return this.http.post(`${Properties.URL}/updateCredito`, data)
    }

    atualizaSangria(data) {
        return this.http.post(`${Properties.URL}/atualizaSangria`, data)
    }

    checarSenha(data) {
        return this.http.post(`${Properties.URL}/checarSenha`, data);
    }

    // GETS
    comandasAbertas() {
        return this.http.get(`${Properties.URL}/comandasAbertas`);
    }

    checarCredito(data) {
        return this.http.get(`${Properties.URL}/checarCredito/${data}`);
    }

    verificarComandaAbertaCaixa(data) {
        return this.http.get(`${Properties.URL}/verificarComandaAbertaCaixa/${data}`);
    }

}