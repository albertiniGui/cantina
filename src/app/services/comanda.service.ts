import { Injectable } from '@angular/core';
import { Properties } from '../class/properties';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class ComandaService {

    constructor(private http: Http) { }

    // POSTS
    salvarComanda(data) {
        return this.http.post(`${Properties.URL}/salvarComanda`, data);
    }

    salvarComandaAberta(data) {
        return this.http.post(`${Properties.URL}/salvarComandaAberta`, data);
    }

    senhaSupervisor(data) {
        return this.http.post(`${Properties.URL}/senhaSupervisor`, data);
    }

    cancelarComanda(data) {
        return this.http.post(`${Properties.URL}/cancelarComanda`, data);
    }

    // GETS
    selectProdutos() {
        return this.http.get(`${Properties.URL}/selectProdutos`)
    }

    selectCartoesConvidados() {
        return this.http.get(`${Properties.URL}/selectCartoesConvidados`);
    }

    verificarExistenciaCartao(data) {
        return this.http.get(`${Properties.URL}/verificarExistenciaCartao/` + data);
    }

    verificarTipoCartao(data) {
        return this.http.get(`${Properties.URL}/verificarTipoCartao/` + data);
    }

    verificarComandaAberta(data) {
        return this.http.get(`${Properties.URL}/verificarComandaAberta/` + data);
    }

    selectPorBarras(data) {
        return this.http.get(`${Properties.URL}/selectPorBarras/` + data);
    }

}