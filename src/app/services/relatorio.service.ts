import { Injectable } from '@angular/core';
import { Properties } from '../class/properties';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class RelatorioService {

    constructor(private http: Http) { }

    // POSTS
    relatorioCompras(data) {
        return this.http.post(`${Properties.URL}/relatorioCompras`, data);
    }

    relatorioInsumos(data) {
        return this.http.post(`${Properties.URL}/relatorioInsumos`, data);
    }

    relatorioProducoes(data) {
        return this.http.post(`${Properties.URL}/relatorioProducoes`, data);
    }

    relatorioProdutos(data) {
        return this.http.post(`${Properties.URL}/relatorioProdutos`, data);
    }

    relatorioReceitas(data) {
        return this.http.post(`${Properties.URL}/relatorioReceitas`, data);
    }

    relatorioVendas(data) {
        return this.http.post(`${Properties.URL}/relatorioVendas`, data);
    }

    relatorioRegistros(data) {
        return this.http.post(`${Properties.URL}/relatorioRegistros`, data);
    }

    relatorioUsuarios(data) {
        return this.http.post(`${Properties.URL}/relatorioUsuarios`, data);
    }

    somarProdutos(data) {
        return this.http.post(`${Properties.URL}/somarProdutos`, data);
    }

    gerarPdf(data) {
        return this.http.post(`${Properties.URL}/gerarPdf`, data)
    }

    // GETS
    verificarPolo() {
        return this.http.get(`${Properties.URL}/verificarPolo`);
    }
}