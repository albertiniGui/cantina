import { Injectable } from '@angular/core';
import { Properties } from '../class/properties';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class MainService {

    constructor(private http: Http) { }

    // POSTS
    sangria(data) {
        return this.http.post(`${Properties.URL}/sangria`, data)
    }

    cadastrarUsuario(data) {
        return this.http.post(`${Properties.URL}/cadastrarUsuario`, data);
    }

    // GETS
    selectAll(tabela, coluna) {
        return this.http.get(`${Properties.URL}/selectAll?tabela=${tabela}&coluna=${coluna}`);
    }

    checarCreditoMain(tag) {
        return this.http.get(`${Properties.URL}/checarCreditoMain/${tag}`);
    }
}