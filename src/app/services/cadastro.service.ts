import { Injectable } from '@angular/core';
import { Properties } from '../class/properties';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class CadastroService {

    constructor(private http: Http) { }

    // POSTS
    cadastrarProduto(data) {
        return this.http.post(`${Properties.URL}/cadastrarProduto`, data)
    }

    editarProduto(data) {
        return this.http.post(`${Properties.URL}/editarProduto`, data)
    }

    cadastrarInsumo(data) {
        return this.http.post(`${Properties.URL}/cadastrarInsumo`, data)
    }

    editarInsumo(data) {
        return this.http.post(`${Properties.URL}/editarInsumo`, data)
    }

    cadastrarReceita(data) {
        return this.http.post(`${Properties.URL}/cadastrarReceita`, data)
    }

    editarReceita(data) {
        return this.http.post(`${Properties.URL}/editarReceita`, data)
    }

    cadastrarFreezer(data) {
        return this.http.post(`${Properties.URL}/cadastrarFreezer`, data)
    }

    editarFreezer(data) {
        return this.http.post(`${Properties.URL}/editarFreezer`, data)
    }

    saveImage(data){
        return this.http.post(`${Properties.URL}/saveImage`,data)
    }

    // GETS
    selectAll(tabela, coluna) {
        return this.http.get(`${Properties.URL}/selectAll?tabela=${tabela}&coluna=${coluna}`)
    }
}