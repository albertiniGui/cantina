import { Injectable } from '@angular/core';

@Injectable()
export class FilterService {

  constructor() { }


  filterData(model, array, nome){
    var result = [];
    var regexp = new RegExp((".*"+model.split("").join('.*')+".*"), "i");
    switch(nome){
      case 'produto':
        array.filter((r) =>{
          if(r.desc_produtoFinal.match(regexp)){
            result.push(r)
          }
        })
      break;
      case 'insumo':
        array.filter((r) =>{
          if(r.desc_insumo.match(regexp)){
            result.push(r)
          }
        })
      break;
      case 'receita':
        array.filter((r) =>{
          if(r.desc_receita.match(regexp)){
            result.push(r)
          }
        })
      break;
      case 'producao':
        array.filter((r) =>{
          if(r.desc_produtoFinal.match(regexp)){
            result.push(r)
          }
        })
      break;
      case 'freezer':
        array.filter((r) =>{
          if(r.id_freezer.match(regexp)){
            result.push(r)
          }
        })
      break;
      case 'compra':
        array.filter((r) =>{
          if(r.desc_compra.match(regexp)){
            result.push(r)
          }
        })
      break;
    }
    return result;
  }

}
