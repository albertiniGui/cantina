import { Injectable } from '@angular/core';
import { Properties } from '../class/properties';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class ComprasService {

    constructor(private http: Http) { }

    // POSTS
    cadastrarCompra(data) {
        return this.http.post(`${Properties.URL}/cadastrarcompra`, data);
    }

    editarCompra(data) {
        return this.http.post(`${Properties.URL}/editarCompra`, data);
    }

    // GETS
    selectAll(tabela, coluna) {
        return this.http.get(`${Properties.URL}/selectAll?tabela=${tabela}&coluna=${coluna}`);
    }

    selectCompras() {
        return this.http.get(`${Properties.URL}/selectCompras`);
    }
}