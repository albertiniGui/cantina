import { Injectable } from '@angular/core';
import { Properties } from '../class/properties';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class ControleService {

    constructor(private http: Http) { }

    // POSTS
    insertProducao(data) {
        return this.http.post(`${Properties.URL}/insertProducao`, data)
    }

    updateProducao(data) {
        return this.http.post(`${Properties.URL}/updateProducao`, data)
    }

    deleteProducao(data) {
        return this.http.post(`${Properties.URL}/deleteProducao`, data)
    }

    entradaEstoqueInsumo(data) {
        return this.http.post(`${Properties.URL}/entradaEstoqueInsumo`, data)
    }

    updateEntrada(data) {
        return this.http.post(`${Properties.URL}/updateEntrada`, data)
    }

    deleteInsumo(data) {
        return this.http.post(`${Properties.URL}/deleteInsumo`, data)
    }

    cadastrarRegistro(data) {
        return this.http.post(`${Properties.URL}/cadastrarRegistro`, data)
    }

    editarRegistro(data) {
        return this.http.post(`${Properties.URL}/editarRegistro`, data)
    }

    // GETS
    selectAll(tabela, coluna) {
        return this.http.get(`${Properties.URL}/selectAll?tabela=${tabela}&coluna=${coluna}`)
    }

    selectControleProduto() {
        return this.http.get(`${Properties.URL}/selectControleProduto`);
    }

    selectProdReceitas() {
        return this.http.get(`${Properties.URL}/selectProdReceitas`)
    }

    selectControleFreezer() {
        return this.http.get(`${Properties.URL}/selectControleFreezer`);
    }
}