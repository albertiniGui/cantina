import { Injectable } from '@angular/core';
import { Properties } from '../class/properties';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class LoginService {

    constructor(private http: Http) { }

    // POSTS
    logIn(data) {
        return this.http.post(`${Properties.URL}/login`, data);
    }

    // GETS
    selectCartoesConvidados() {
        return this.http.get(`${Properties.URL}/selectCartoesConvidados`);
    }
}